---
layout: page_toc
title: Criptografia
permalink: /criptografia/
parent: Serveis i processos
nav_order: 3
---
Resultats d'aprenentatge (compartit amb [Seguretat](/seguretat/)):

1.  Protegeix les aplicacions i les dades definint i aplicant criteris de seguretat en l’accés, emmagatzematge i transmissió de la informació.

## Conceptes

Principi de Kerckhoffs: "L'efectivitat del sistema no ha de dependre que el seu disseny romangui en secret.".

### Autenticació

L'**autenticació** és el procés de confirmar l'autenticitat reclamada per una entitat. Tenim bàsicament dos tipus:

*   Autenticació **d'autor**: l'autor és qui diu ser. Es pot aconseguir amb una **signatura digital**.
*   Autenticació **de dades**: les dades no han estat modificades. Es pot aconseguir amb un algorisme de **resum de missatge** (message digest).

L'autenticació no vol dir que tinguem xifratge.

### Xifrat

La **criptografia** (del grec "_kryptos_" - amagat, secret - i "_graphin_" - escriptura. Per tant seria "**escriptura oculta**") és l'estudi de formes de convertir informació des de la seva forma original cap a un codi incomprensible, de forma que sigui incomprensible pels que no coneguin aquesta tècnica.

En la terminologia de criptografia, trobem els següents elements:

*   La **informació original** que ha de protegir-se i que es denomina text en clar o **text pla**.
*   El **xifrat** és el procés de convertir el text pla en un text il·legible, anomenat **text xifrat** o **criptograma**.
*   Les **claus** són la base de la criptografia, i són cadenes de números amb propietats matemàtiques.

### Estats de les dades

En un sistema segur, quan una dada s'ha d'utilitzar (en ús) cal que sigui **en pla**. Però si està emmagatzemada o transmetent-se, cal mantenir-la secreta. Aquests són els estats:

*   **En repòs**: quan estan emmagatzemades digitalment en un dispositiu.
*   **En trànsit**: quan s'estan movent entre dispositius o punts de les xarxa.
*   **En ús**: quan una aplicació l'estan utilitzant, sense protecció.

Quan protegim les dades en repòs, preparem al nostre sistema per a l'eventualitat que es puguin llegir després d'un atac a la màquina on són. Quan protegim les dades en trànsit, ho fem perquè sabem que qualsevol pot escoltar el tràfic d'una xarxa.

Si s'exposen les nostres dades xifrades, i aconsegueixen accedir a les claus secretes associades (ja sigui al moment o més endavant), podran veure-les en pla. 

De vegades podem evitar haver-les de guardar. Per exemple, si les podem demanar cada cop (p. ex. clau mestra), o bé comparar-les amb resums (p. ex. contrasenyes).

### Tipus de xifrat

Existeixen dos grans grups d'algorismes de xifrat, en funció de si les claus són úniques o van en parella:

*   **Simètrics:** els algorismes que utilitzen una única clau privada per xifrar la informació i la mateixa per desxifrar-la.
*   **Asimètrics:** els que tenen dues claus, una pública i una altra privada, que permet xifrar amb una qualsevol i desxifrar amb l'altra. S'utilitzen principalment per a dos propòsits:
    *   **Xifrat amb clau pública**: un missatge xifrat amb clau pública només es pot desxifrar amb la clau privada.
    *   **Signatura digital**: un missatge signat amb la clau privada pot ser verificat per qualsevol amb la clau pública.

La clau simètrica té inconvenients. Primer, necessites una clau per cada parella origen/destí. Segon, necessites una forma segura de compartir-les. La clau asimètrica té l'avantatge que pots compartir la part pública de la clau, ja que sense la part privada no pots fer res, però els algorismes són més lents i tenen limitacions en la mida del missatge a xifrar. Per exemple, RSA té un missatge màxim de `floor(n/8)-11 bytes`.

El xifrat també pot ser de bloc o de stream:

*   **Bloc**: xifrat de blocs de mida fixa de dades. Poden ser asimètrics o simètrics.
*   **Stream**: xifrat d'un stream (corrent) de bits o bytes. Són simètrics.

Si no es diu el contrari, parlem només de xifrats en bloc.

**Xifrat simètric**: les dues parts comparteixen una clau privada

![](/images/xifratpriv.png)

**Xifrat asimètric**: només el propietari de les claus (el receptor) pot desxifrar les dades

![](/images/xifratpubl.png)

### Intercanvi de claus

Com ja hem vist, el xifratge simètric té el problema de compartir la clau entre les dues parts. I l'asimètric, no permet xifrar blocs gaire grans.

La solució és utilitzar els dos tipus de xifrat de forma combinada. 
*   La criptografia asimètrica ens permet compartir una clau privada de forma segura.
*   La criptografia simètrica ens permet xifrar missatges més llargs i més ràpidament.

L'intercanvi de clau més senzill es pot fer amb xifrat de clau pública RSA: una part xifra el secret compartit amb la clau pública de l'altra. El problema és que aquesta acció no és "secreta cap endavant": si algú obté la clau privada en el futur, i ha guardat la conversa secreta, podria desxifrar-la.

![](/images/rsaexchange.png)

No obstant, l'algorisme més comú per fer intercanvi de clau és el **Diffie-Hellman (DH)**, present en el preàmbul de la majoria de les comunicacions amb xifrat simètric.

![](/images/diffiehellman.png)

Per a dues parts A i B, el procés és el següent:
*   A i B generen dues parelles de claus pública/privada, Apub/Apri i Bpub/Bpri.
*   Les dues parts s'intercanvien les claus públiques Apub i Bpub.
*   En privat, cada part combina les claus públiques rebudes amb les privades (Apub+Bpri, Bpub+Apri). La característica essencial de DH és que aquesta combinació genera el mateix secret!

### Resums de missatge

Un "message digest" o hash és una seqüència de bytes produïda quan un conjunt de dades passen per un **motor**. No és sempre necessària una clau perquè aquest motor operi. Alguns coneguts: MD5, SHA-256. Les seves propietats són:

1.  És determinista (el mateix resultat per la mateixa entrada).
2.  És ràpida.
3.  La funció inversa no és viable.
4.  Un petit canvi d'entrada provoca un gran canvi de sortida.
5.  Dues entrades diferents no poden tenir el mateix hash.

Un resum ens permet protegir la **integritat** d'un missatge.

Veiem com funciona SHA-256 a Linux sobre un petit text "hello world!". Si ho proveu, veureu que el resultat és instantani (2). Com que té 256 bits, genera un resum de 32 bytes (en hex). Mireu com, canviar un caràcter, canvia totalment el hash (4).

```
$ echo 'hello world!' | openssl sha256
(stdin)= ecf701f727d9e2d77c4aa49ac6fbbcc997278aca010bddeeb961c10cf54d435a
$ echo 'hello,world!' | openssl sha256
(stdin)= 4c4b3456b6fb52e6422fc2d1b4b35da2afbb4f44d737bb5fc98be6db7962073f
```

Si busqueu el resum de "hello world!" o el de "123456" el trobareu a la xarxa. Dues conclusions: per a un algorisme i una entrada, tenim la mateixa sortida (1). No tenim la funció inversa (3), però hi ha taules de resums per a texts que fan la correspondència, i que s'utilitzen per esbrinar credencials.

Si volem protegir la integritat i l'**autenticitat**, podem utilitzar els MAC (message authentication code). Bàsicament, es tracta de resums segurs xifrats amb una clau privada que cal compartir entre les dues parts per tal de verificar la comunicació.

![](/images/hmac.png)

També tenim les **Key Derivation Functions (KDF)**, una hash que permet derivar un o més secrets a partir d'un altre més petit, com un password. Les KDF permeten estendre claus (key stretching) en altres més llargues.

En el següent diagrama es poden veure dos usos de les KDF: 
*   Per a guardar un hash d'una contrasenya, i poder comprovar si s'introdueix correctament.
*   Per a generar claus d'algorismes simètrics a partir d'una contrasenya.

![](/images/kdf.png)

### Signatures digitals

La **signatura digital** és un mecanisme de xifrat per autentificar informació digital. El mecanisme utilitzat és la **criptografia de clau pública.** Per això aquest tipus de signatura també rep el nom de signatura digital de clau pública. S'utilitzen per a garantir tres aspectes: **autenticitat**, **integritat** i **no repudi**.

Aquest és el procés per a **obtenir** una signatura digital:

1.  Es calcula un resum de missatge per a les dades d'entrada.
2.  El resum es xifra amb la clau privada.

Aquest és el procés per a **verificar** una signatura digital:

1.  Es calcula un resum de missatge per a les dades d'entrada.
2.  El resum de la signatura digital es desxifra amb la clau pública.
3.  Es comparen els dos resums. Si són iguals, la signatura és correcta.

**Signatura digital**: el propietari de les claus (l'emissor) envia la prova de les dades originals

![](/images/signatura.png)

### Certificats

Si se signa un document digital utilitzant una clau privada, el receptor ha de tenir la clau pública per verificar la signatura. El problema és que una clau no indica a qui pertany. Els certificats resolen aquest problema: una entitat ben coneguda (Certificate Authority: CA) **verifica la propietat de la clau pública** que se t'ha enviat.

Un certificat conté:

1.  El **nom de l'entitat** per qui s'ha emès el certificat.
2.  La **clau pública** d'aquesta entitat.
3.  La **signatura digital** que verifica la informació en el certificat, feta amb la clau privada de l'emissor.

Un certificat podria contenir aquesta informació:

```
"L'autoritat de certificació 2276172 certifica que la clau pública de John Doe és 217126371812".
```

Problema: l'emissor del certificat té una clau pública en què hem de confiar. I poden estar encadenades. La darrera de la cadena està autosignada per ella mateixa. Llavors, hem d'acceptar certes CA com a **confiables**, i habitualment Java (igual que els navegadors) tenen una llista de CA confiables.

El certificat de servidor TLS/SSL és el més comú. El client utilitza **l'algorisme de validació del camí de certificació**:

1.  El subject (CN) del certificat coincideix amb el domini al qual es connecta.
2.  El certificat l'ha signat un CA confiable.

**Com s'estableix la comunicació segura entre el navegador i un servidor HTTPS?**

1.  Quan el navegador es connecta al servidor es descarrega el seu certificat, que conté la seva **clau pública**.
2.  El navegador posseeix les claus públiques de les CA confiables, i comprova si la signatura del certificat s'ha fet per una CA confiable.
3.  El navegador comprova que el domini que apareix al certificat és el mateix amb el qual es comunica amb el servidor.
4.  El navegador genera una clau simètrica, la xifra amb la clau pública del servidor i li envia.
5.  S'acaba el xifrat asimètric i comença el simètric amb la clau compartida.

## Criptografia a Java (JCA)

Els motors criptogràfics de Java proporcionen mecanismes per signatures digitals, resums de missatges, etc. Aquests motors estan implementats per proveïdors de seguretat (`java.security.Provider`), que es poden visualitzar mitjançant `java.security.Security.getProviders()`. Cada motor (`java.security.Provider.Service`) té un tipus i un algorisme.

### Claus

Podem generar claus de tipus simètric (`KeyGenerator`) o asimètric (`KeyPairGenerator`).

```java
KeyGenerator keyGen = KeyGenerator.getInstance(algorithm);
keyGen.init(size);
SecretKey secretKey = keyGen.generateKey();
```

Algorismes simètrics típics són [DES](https://en.wikipedia.org/wiki/Data_Encryption_Standard) (56 bits) o [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) (128, 192, 256 bits).

```java
KeyPairGenerator kpg = KeyPairGenerator.getInstance(algorithm);
kpg.initialize(size);
KeyPair kp = kpg.generateKeyPair();

PublicKey publicKey = kp.getPublic();
PrivateKey privateKey = kp.getPrivate();
```

Cal indicar l'algorisme. El més habitual és [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) (1024, 2048 bits).

Tant SecretKey, com PublicKey i PrivateKey, són subclasses de `java.security.Key`. Totes elles tenen un mètode getEncoded(): la clau en format binari. 

### Xifrat

Per a poder xifrar, necessitem un objecte [`javax.crypto.Cipher`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/javax/crypto/Cipher.html):

```java
Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
```

El format del paràmetre (transformation) és _algorithm/mode/padding_. Mode i padding són opcionals: si no s'indiquen, s'utilitza el **mode** i **padding** per defecte. Què són?

*   **Padding**: és una tècnica que consisteix a afegir dades de farciment al començament, mig o fi d'un missatge abans de ser xifrat. Això es fa perquè els algorismes estan dissenyats per tenir dades d'entrada d'una mida concreta.
*   **Mode**: defineix com es fa la correspondència entre els blocs d'entrada (en pla) i els de sortida (xifrats). El més senzill és el mode ECB: un bloc d'entrada va a un de sortida. Altres modes, més segurs, fan aleatòria aquesta correspondència.

Després, hem d'inicialitzar l'objecte utilitzant el mode (`Cipher.ENCRYPT_MODE` o `Cipher.DECRYPT_MODE`) i la clau de xifrat:

```java
cipher.init(Cipher.ENCRYPT_MODE, key); // xifrat
cipher.init(Cipher.DECRYPT_MODE, key); // desxifrat
```

Finalment, realitzem el xifrat o desxifrat:

```java
byte[] bytesOriginal = textOriginal.getBytes("UTF-8"); // necessito bytes com a entrada
byte[] bytesXifrat = cipher.doFinal(bytesOriginal);
```

El desxifrat podria ser:

```java
byte[] bytesDesxifrat = cipher.doFinal(bytesXifrat);
// alternativament, si el contingut a desxifrar és una part de l'array:
byte[] bytesDesxifrat = cipher.doFinal(bytesXifrat, inici, longitud);
```

Alguns [modes de xifrat en bloc](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation) utilitzen el que s'anomena **vector d'inicialització (IV)**. Es tracta d'un paràmetre aleatori per a l'algorisme de xifrat que fa més difícil relacionar els blocs en funció de les seves entrades. Normalment no cal que sigui secret, només que no es repeteixi amb la mateixa clau.

Per utilitzar aquests modes (p.ex. CBC), cal afegir un nou paràmetre quan s'inicialitza el Cipher. La mida de l'IV és habitualment la mateixa del bloc. Per AES és de 16 bytes (256 bits).

```java
byte[] iv;
// inicialitzar iv amb un generador aleatori com SecureRandom
IvParameterSpec parameterSpec = new IvParameterSpec(iv);
cipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec); // xifrat
cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec); // desxifrat
```

### Xifrat de streams

No té sentit xifrar grans quantitats de dades amb mètodes de bloc. Per a aquestes situacions, podem utilitzar el xifrat de streams. Aquest xifrat és sempre **simètric**.

A Java, tenim les classes [CipherInputStream](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/javax/crypto/CipherInputStream.html) i [CipherOutputStream](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/javax/crypto/CipherOutputStream.html).

CipherInputStream i CipherOutputStream admeten un Cipher simètric de bloc, com per exemple AES/ECB/PKCS5Padding, o bé els modes de tipus feedback CFB8 or OFB8, (8 = blocs de 8 bits), com per exemple AES/CFB8/NoPadding. Els modes feedback necessiten vectors d'inicialització (IV).

Per exemple, si volem obrir un arxiu i xifrar-lo o desxifrar-lo, podem fer-ho així:

```java
FileInputStream in = new FileInputStream(inputFilename);
FileOutputStream fileOut = new FileOutputStream(outputFilename);
CipherOutputStream out = new CipherOutputStream(fileOut, cipher);
```

Llavors, caldria copiar el stream in a out.

L'objecte cipher ha d'inicialitzar-se amb el mode que calgui, ENCRYPT\_MODE o DECRYPT\_MODE.

CipherInputStream es pot utilitzar de forma anàloga. En aquest cas, el stream de sortida podria ser un FileOutputStream:

```java
FileInputStream fileIn = new FileInputStream(inputFilename);
CipherInputStream in = new CipherInputStream(fileIn, cipher);
FileOutputStream fileOut = new FileOutputStream(outputFilename);
```

### Dades binàries en text

Les claus i la informació xifrada està en format binari. Si cal intercanviar-ho utilitzant un canal de text, es poden convertir utilitzant Base64. A Java, tenim [`java.util.Base64`](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Base64.html).

```java
byte[] binary1 = ...;
String string = Base64.getEncoder().encodeToString(binary1);
byte[] binary2 = Base64.getDecoder().decode(string);
// binary1 i binary2 són iguals
```

## Resums, signatures i certificats

### Resums de missatges

Els resums s'implementen utilitzant la classe `java.security.MessageDigest`, que permet generar un resum de dades. El resum es pot fer segur utilitzant `javax.crypto.Mac`. També es pot realitzar l'operació amb Streams gràcies a `java.security.DigestInputStream i java.security.DigestOutputStream`.

Alguns algorismes típics de resums: MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512.

```java
MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
byte[] resum = messageDigest.digest(text.getBytes());
```

### Resums segurs

Un Message Authentication Code (`javax.crypto.Mac`) és un resum xifrat amb una clau privada compartida. Només es pot verificar si tens aquesta clau. Podem generar-lo així:

```java
Mac mac = Mac.getInstance(algorithm);
mac.init(key); // la clau privada
byte[] macBytes = mac.doFinal(text.getBytes());
```

Un algorisme podria ser HmacSHA256 (HMAC: Hash-based MAC).

### Key Derivation Functions

Amb una KDF, podem generar una clau més llarga que un passowrd, per exemple.
Aquí tenim un exemple utilitzant l'algorisme PBKDF2WithHmacSHA256.

```java
SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
KeySpec ks = new PBEKeySpec(password, salt, iterationCount, keyLength);
SecretKey rawSecret = f.generateSecret(ks); // secret genèric
SecretKey aesSecret = new SecretKeySpec(s.getEncoded(), "AES"); // secret AES
```

Els paràmetres del KeySpec són:
*   password: un char[] amb la contrasenya.
*   salt: una salt per a randomitzar el hash.
*   iterationCount: nombre d'iteracions per a generar el hash.
*   keyLength: longitud de la clau a generar.

### Signatures digitals

Una signatura digital equival a fer un resum i xifrar-lo amb una clau privada. El receptor podria desxifrar-lo amb la pública, i comparar-lo amb un resum que faci de les dades (en pla) rebudes.

Els algorismes són variats, per exemple, SHA256withRSA indica que el resum es fa amb SHA256 i el xifratge amb RSA. Per tant, les claus utilitzades han de ser RSA. Per a signar una entrada (array de bytes):

```java
Signature sign = Signature.getInstance(algorithm);
sign.initSign(privateKey);        
sign.update(input);
byte[] signatura = sign.sign();
```

Per verificar-la:

```java
Signature sign = Signature.getInstance(algorithm);
sign.initVerify(publicKey);
sign.update(input);
boolean correcte = sign.verify(signatura);
```

### Certificats

Els certificats (`java.security.cert.Certificate`) més habituals són de tipus X.509, i indiquen una vinculació d'una identitat a una clau pública, garantida per una altra entitat autoritzada. Inclouen:

*   data d'inici i fi
*   versió (3 actualment)
*   número de sèrie (únic per proveïdor)
*   el DN (distinguished name) de la CA emissora
*   el DN del subjecte del certificat

Els DN (Distinguished Names) conté una sèrie de camps (CN, OU, O, L, S, C) que identifiquen tant l'emissor com el subjecte.

Habitualment, els trobem dins dels magatzems. Per gestionar-los podem utilitzar l'eina `keytool` del JRE o bé programàticament amb la classe `java.security.KeyStore` i els mètodes `getKey(alias)` (clau privada) i `getCertificate(alias)` (certificat on hi la clau pública).

Són necessaris als portals web amb seguretat habilitada HTTPS.

## Comunicació SSL amb sòcols

Per tal que un socket es pugui comunicar amb el protocol segur SSL, cal crear objectes una mica diferents.

Al servidor:

```java
ServerSocket serverSocket = new ServerSocket(PORT);
```

Es converteix en:

```java
SSLServerSocketFactory factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();        
SSLServerSocket serverSocket = (SSLServerSocket) factory.createServerSocket(PORT);
```

Opcionalment, si volem autenticar al client (ho veurem més endavant):

```java
serverSocket.setNeedClientAuth(true);
```

Al client:

```java
Socket clientSocket = new Socket(HOST, PORT);
```

Es converteix en:

```java
SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
SSLSocket clientSocket = (SSLSocket) factory.createSocket(HOST, PORT);    
```

A més, cal configurar les claus privada i pública correctament. I això depèn de si volem o no autoritzar la clau pública dels clients al servidor.

Tenim dos tipus de magatzems necessaris:

*   Els **keystores** serveixen per identificar-nos
*   Els **truststores** serveixen per autoritzar altres parts

La configuració es realitza mitjançant propietats de sistema a Java, amb aquesta instrucció:

*   `System.setProperty("nomDeLaPropietat", "valor");`

### Sense autorització del client

Cal generar dos magatzems:

*   serverKeystore.jks: clau pública/privada del servidor
*   clientTruststore.jks: clau pública del servidor

```
keytool -genkey -alias srvAlias -keyalg RSA -keystore serverKeystore.jks -keysize 2048
keytool -export -keystore serverKeystore.jks -alias srvAlias -file server.crt
keytool -importcert -file server.crt -keystore clientTruststore.jks -alias srvAlias\
```

Configuració al servidor per a identificar-se:

```
javax.net.ssl.keyStore=serverKeystore.jks
javax.net.ssl.keyStorePassword=yourpassword
```

Configuració al client per a acceptar al servidor (només si el servidor no està autoritzat per una CA Authority):

```
javax.net.ssl.trustStore=clientTrustore.jks
javax.net.ssl.trustStorePassword=yourpassword
```

### Amb autorització del client

Als les configuracions anteriors, cal afegir:

*   clientKeystore.jks: clau pública/privada del client
*   serverTruststore.jks: clau pública del client

```
keytool -genkey -keyalg RSA -alias cltAlias -keystore clientKeystore.jks -keysize 2048
keytool -export -keystore clientKeystore.jks -alias cltAlias -file cliente.crt
keytool -importcert -file cliente.crt -keystore serverTruststore.jks -alias cltAlias
```

Configuració al servidor per a acceptar el client:

```
javax.net.ssl.trustStore=serverTruststore.jks
javax.net.ssl.trustStorePassword=yourpassword
```

Configuració al client per a identificar-se:

```
javax.net.ssl.keyStore=clientKeystore.jks
javax.net.ssl.keyStorePassword=yourpassword
```

## Gestió de claus

### Generació i destrucció

Consells sobre generació:
*   Per a xifrat simètric, utilitzar AES amb almenys 128 bits, millor 256. 
*   Utilitzar modes autenticats si és possible: GCM, CCM. Si no, CTR o CBC. ECB hauria d'evitar-se.
*   Per a xifrat asimètric, utilitzar criptografia de corba el·líptica (ECC) com Curve25519. Si no, utilitzar RSA d'almenys 2048 bits.
*   Utilitzar generadors aleatoris segurs. Millor CSPRNG que PRNG. A Java, preferir sempre SecureRandom sobre Random.

És important fer que les claus tinguin una durada limitada (rotar-les): desxifrar i rexifrar.
Cal fer rotació de les claus si:
*   Si se sap (o se sospita) que la clau anterior ha estat compromesa.
*   Quan passi un cert temps predeterminat.
*   Quan s'hagi xifrat una certa quantitat de dades.
*   Si ha canviat la seguretat associada a un algorisme (nous atacs).

### Emmagatzematge

Les aplicacions gestionen diferents tipus de secrets: credencials, claus de xifratge, claus privades de certificats, claus d'API, dades sensibles, etc. 

Els secrets s'han de xifrar en repòs i en trànsit. Les claus per xifrar secrets s'anomenen **Data Encryption Keys (DEK)**. Aquestes claus també s'han de protegir, i el que es fa és xifrar-les utilitzant el que es diu **Key Encryption Key (KEY)** o **"Master Key"**. Aquest esquema permet modificar la KEK mantenint les DEK, i per tant, sense requerir el rexifratge de les dades.

Consells amb les claus:
*   Si és possible, no emmagatzemar mai la KEK. Per exemple, demanar-la interactivament quan calgui.
*   Si és factible, utilitzar un HSM (Hardware Security Module).
*   Millor no mantenir-les en memòria i en pla.
*   Mai guardar-les al codi ni al git.
*   Les DEK i les KEK s'han d'emmagatzemar en llocs diferents. Per exemple, la base de dades i el sistema d'arxius, o en màquines diferents.
*   Si van a un fitxer, protegir els arxius amb permisos restrictius.
*   Fer key stretching des d'una contrasenya per generar la KEK. Per exemple, amb la funció de derivació de claus PBKDF2.

### KeyStores

Un magatzem de claus, o **keystore**, pot tenir un alias, i pot contenir:

*   **Claus**: pot ser una clau (asimètrica o simètrica). Si es tracta d'una asimètrica, pot contenir una cadena de certificats.
*   **Certificats**: una clau pública, habitualment l'arrel de CAs de confiança.

Les keystores poden tenir diferents formats (JKS, JCEKS, PKCS12, PKCS11, DKS). Els més utilitzats són:

*   JKS (Java Key Store): format propietari de Java. Històricament, el més utilitzat. Extensió jks.
*   PKCS#12: format standard. Format recomanat. Extensió p12 o pfx.

Java té una eina anomenada **keytool** per gestionar magatzems de claus. A continuació es mostren algunes comandes habituals.

Comenda per llistar els continguts d'una keystore:

```shell
$ keytool -list -v -keystore keystore.jks
```

Comanda per generar un keystore amb una parella de claus:

```shell
$ keytool -genkey -alias mydomain -keyalg RSA -keystore keystore.jks -keysize 2048
```

Comanda per exportar un certificat d'una keystore:

```shell
$ keytool -export -alias mydomain -file mydomain.crt -keystore keystore.jks
```

Comanda per importar un certificat a una keystore:

```shell
$ keytool -importcert -file mydomain.crt -keystore keystore.jks -alias mydomain
```

## Formats de certificats

**PEM - Privacy Enhanced Mail**

PEM és el format més comú en què les autoritats certificadores emeten certificats. Aquests són més utilitzats pels usuaris d’Unix / Linux. Estan codificats en base-64.

La part pública del certificat estarà limitada per "—–BEGIN PUBLIC KEY—–" i "—–END PUBLIC KEY—–", mentre que la part privada del certificat és dins de "—–BEGIN RSA PRIVATE KEY––" i "—–END RSA PRIVATE KEY—–".

El format PEM pot contenir qualsevol certificat client / servidor, certificat intermedi, CA arrel i la clau privada.

*   Són fitxers ASCII codificats per Base64.
*   Tenen extensions com .pem, .crt, .key (.cert de vegades).
*   Els servidors Apache i similars utilitzen certificats de format PEM.

**DER - Distinguished Encoding Rule**

DER és un certificat de format binari. Es poden codificar tota mena de certificats i claus privades.

Aquest format admet l’emmagatzematge d'un únic certificat i no inclou la clau privada per a la CA arrel/intermèdia.

*   Són fitxers de format binari.
*   Tenen extensions .cer i .der.

DER s’utilitza normalment en la plataforma Java.

**PKCS#7**

Aquest format només conté certificat o cadena de certificats, però no emmagatzema la clau privada.

CA s’utilitza habitualment per proporcionar cadenes de certificats als usuaris, i solen tenir extensió .p7b o .p7s.

**PFX - Personal Information Exchange**

PFX és un format per emmagatzemar un certificat de servidor o qualsevol certificat intermedi juntament amb la clau privada en un fitxer xifrat. PFX segueix l'estàndard de xifratge de clau pública (PKCS). El terme PFX s'utilitza indistintament amb PKCS#12.

Tenen extensió .pfx o .p12.

## Referències

*   [Java Security Standard Algorithm Names, JDK 11](https://docs.oracle.com/en/java/javase/11/docs/specs/security/standard-names.html)
*   [Cryptographic Storage Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cryptographic_Storage_Cheat_Sheet.html)
*   [Security Developer’s Guide, JDK 11 Providers Documentation](https://docs.oracle.com/en/java/javase/11/security/oracle-providers.html)
*   [Block cipher mode of operation](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation)
*   [Java Criptography (Jenkov)](http://tutorials.jenkov.com/java-cryptography/index.html)
*   [Cryptographic hash function (Wikipedia)](https://en.wikipedia.org/wiki/Cryptographic_hash_function)
*   [Public-key cryptography (Wikipedia)](https://en.wikipedia.org/wiki/Public-key_cryptography)
*   [Java Security 2nd Edition (examples)](https://resources.oreilly.com/examples/9780596001575)
*   [Practical Cryptography for Developers](https://cryptobook.nakov.com/)
*   [Establishing a TLS Connection](https://www.acunetix.com/blog/articles/establishing-tls-ssl-connection-part-5/)
*   [Generating and Verifying Signatures (The Java Tutorials)](https://docs.oracle.com/javase/tutorial/security/apisign/index.html)
*   [A Deep Dive on End-to-End Encryption: How Do Public Key Encryption Systems Work?](https://ssd.eff.org/en/module/deep-dive-end-end-encryption-how-do-public-key-encryption-systems-work)
*   [Java Keytool - Commands and Tutorials](https://www.misterpki.com/java-keytool/)
*   [Java KeyStores - the gory details](https://neilmadden.blog/2017/11/17/java-keystores-the-gory-details/)
*   [Trusted Timestamping (INCIBE)](https://www.incibe-cert.es/blog/trusted-timestamping)
*   [Trusted Timestamping (Wikipedia)](https://en.wikipedia.org/wiki/Trusted_timestamping)
*   [CyberChef](https://gchq.github.io/CyberChef/)
*   [Capture The Flag 101](https://ctf101.org/)
*   [SideKEK](https://github.com/OWASP/SideKEK)
