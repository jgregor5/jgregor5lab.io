---
layout: page_toc
title: Pla d'empresa
permalink: /eie_plaempresa/
parent: Empresa
nav_order: 8
---

Aquest és un guió per a confeccionar un pla d'empresa basat en quatre checkpoints.

## Primer checkpoint

Aquest checkpoint conté dos punts: el **pla estratègic** i el **canvas**.

### 1 Pla estratègic

#### 1.1 Dades personals de l'equip promotor

*   Per a totes les persones implicades en el projecte:
    *   Nom i cognoms
    *   DNI
    *   Data de naixement
    *   Adreça
    *   Telèfon
*   Formació i experiència laboral relacionada amb les diferents tasques que es desenvoluparan en la nova empresa
    *   Destacar els elements que estiguin més relacionats amb l’activitat empresarial: formació, experiència relacionades en atenció al públic, experiència en àmbit comercial, en gestió econòmica i administrativa, etc.
    *   Destacar si tens experiències prèvies en negocis similars o en el cas d’existir algun buit en relació amb algun aspecte comentar com se superaran (Punts forts i punts febles de les capacitats dels emprenedors front l’activitat)
    *   Aportar el currículum vitae de cadascun dels promotors a l’annex, com a carta de presentació del vostre perfil i de la vostra capacitat tècnica.

*Aquesta informació es pot copiar de FOL.*

#### 1.2 Motivacions i origen de la idea

*   **Explicar els motius que es tenen per crear l’empresa.**
*   **Explicar l’evolució de la idea**: com va sorgir la idea de crear l’empresa? Com ha anat canviant?
*   Quines persones participen en el projecte i quin és el grau d’implicació de cadascuna.
*   Projecta l'empresa cap al futur: quina és la vostra **visió**?
*   Explica els **valors** que voleu que es reconeguin en la vostra activitat.
*   Quins criteris socials aplicareu a la vostra empresa? Veure annex: llista de criteris socials.

#### 1.3 Descripció de l'activitat

*   Quina és la **missió** del negoci?
*   Tipus d'activitat que es durà a terme. Quin producte o servei es vendrà?
*   Expliqueu la vostra **proposta de valor**. Elements diferencials de la vostra idea. Creativitat i originalitat.
*   Petita planificació de l'activitat. Quan comença? On es fa físicament?

*Entre l'apartat 1.2 i el 1.3, escriu almenys una pàgina.*

### 2 Canvas

Inclou el canvas del model de negoci desenvolupat.

## Segon checkpoint

### 1 Pla de màrqueting

#### 1.1 Anàlisi de mercat

*   **Descripció del sector de l'activitat i projecció**: descriure el sector econòmic on es desenvoluparà l’activitat empresarial de l’empresa i la previsió futura d’aquest sector de manera global.
*   **Concretar el mercat escollit i tendències**: Definir i descriure el mercat concret al qual es dirigirà l’activitat de l’empresa (zona geogràfica, sectors, etc.), i les tendències o comportament que es preveu en aquest àmbit local.

*Fer 1 a 1,5 pàgines.*

#### 1.2 Segmentació de Clients

*   **Determinar el perfil comú dels nostres clients potencials.**
*   En el cas que existeixin diferents tipus de clients, cal descriure’ls indicant quins criteris s’han utilitzat per diferenciar-los i per què s’han triat aquests criteris. Explicar si són empreses, particulars, altres entitats, administracions, etc.
*   Dimensionar el mercat potencial: quants consumidors o clients componen aquests mercats.

Incloure una taula amb una fila per cada tipus de client, amb les següents columnes:

*   **Perfil demogràfic**: gènere, edat, estat civil, mida familiar, ocupació, nivell d'estudis, nivell de renda.
*   **Perfil psicogràfic**: estil de vida, personalitat, classe social.
*   **Comportament**: benefici buscat, freqüència de compra, nivell de lleialtat, nivell d'ús, lloc i moment d'ús, categories d'usuaris.

*Fer 1 pàgina.*

#### 1.3 Anàlisi de la competència

*   **Característiques** de la competència (directa / indirecta): preus, localització, productes/serveis, mida, tipus de client, qualitat, directa o indirecta.
    *   És **directa** o **indirecta**: ofereixen el mateix que la teva empresa o un de similar, però que poden ser considerats com a competència.
*   **Posicionament** de la competència: quina estratègia competitiva utilitza? Què fa i com per atraure clients.
*   **Punts forts i febles** de la competència respecte de l'estàndard del mercat

Pots fer un quadre d'anàlisi de la competència directa. Per cada empresa competidora, i per la teva, afegeix aquestes columnes:

*   Nom de l'empresa
*   Producte o servei
*   Nivell de preus
*   Localització
*   Punts febles respecte al mercat
*   Punts forts respecte al mercat

*Fer 1 pàgina.*

#### 1.4 Posicionament i avantatge competitiu

Aspectes diferenciadors del nostre producte/servei respecte de la competència directa. T'has de posar en el punt de vista del client final i explicar:

*   Què ens diferencia?
*   Què influirà en les decisions de compra del possible client?
*   Quina necessitat es cobreix?
*   Amb quina freqüència compra?
*   Si l'usuari o beneficiari és diferent de qui fa la compra, indicar qui decideix la compra i quines necessitats es cobreixen.

*Fer mitja pàgina.*

#### 1.5 Màrqueting-Mix del projecte

El màrqueting-mix es fa en quatre punts:

1.  **Producte/servei**: característiques i elements diferenciadors respecte de l'oferta actual (qualitat, servei postvenda, presentació, segmentació, etc.).
2.  **Política de preus**:
    *   Criteris per determinar-los: segons la competència, segons els costos, etc.
    *   Llista de preus i previsió de vendes mensual
    *   Forma de cobrament
3.  **Promoció i publicitat**
    *   Com es donarà a conèixer l'empresa i el seu producte/servei? Mitjans, promocions especials, destí de les accions.
    *   Cost de la publicitat i/o les accions de promoció. Cost previst, impacte previst en clients i ingressos.
4.  **Distribució i comercialització**
    *   Com arribarà el producte/servei al client? Canals majoristes, detallistes, punts de venda directa, etc.
    *   Com s'organitzarà la tasca comercial amb relació als canals.

*Fer 2 pàgines.*

### 2 Taula financera

Fer una taula esborrany anotant les despeses (fixes i variables) i els ingressos que es produeixen al teu negoci quan ja es troba en un punt d'equilibri financer. Pots assumir que això passa després d'un cert temps de funcionament, com per exemple un any.

### 3 Canvas (actualitzat)

Inclou l'actualització del canvas del model de negoci amb relació als aspectes desenvolupats en aquest segon checkpoint.

## Tercer checkpoint

### 1 Pla de màrqueting en línia

#### 1.1 Objectius

Expliqueu els vostres objectius de màrqueting. Han de ser (SMART): eSpecífic, Mesurable (analítica web), Assolible, orientat a Resultats, basat en un Temps delimitat.

#### 1.2 Estratègia inbound

*   Fes un estudi de les **paraules clau** del teu negoci:
    *   Indica les paraules clau escollides.
    *   Justificar-les.
    *   Explica on juga la teva competència.
*   Defineix i crea el teu **contingut SEO**.
    *   Fes un diagrama amb la jerarquia, el títol i les paraules clau per a cada pàgina.
    *   Fes un esbós del text de la pàgina principal, on hi ha d’haver les paraules clau.
*   Escriu el teu **calendari editorial**: un full de càlcul o taula amb les publicacions que preveus fer els tres primers mesos, aproximadament.

#### 1.3 Estratègia outbound

Defineix una estratègia de màrqueting outbound per al teu negoci. Indica la teva inversió a cercadors, xarxes socials o altres mitjans.

#### 1.4 Mesurament

Defineix almenys **cinc indicadors KPI en relació als teus objectius** i explica com faràs el seu mesurament.

*Total: 3 pàgines (1/2, 1 i 1/2, 1/2 i 1/2).*

### 2 Pla operacional

L’objectiu és detallar **com es fabricarà el producte** o quin és el **procés de prestació del** servei. Cal que descriguis totes les **depeses** que faràs.

*   **Recursos infraestructurals**: descripció del local, ubicació, entorn, què cal fer per adequar-lo i les despeses necessàries de lloguer.
*   **Recursos materials**: maquinària, eines, mobiliari, matèries primeres.
*   **Recursos humans**: funcions i tasques necessàries, descriure els perfils professionals, dedicació horària, procediment de selecció i costos salarials.
*   **Proveïdors**:
    *   Llista de proveïdors i forma de pagament
    *   Contractacions externes
    *   Serveis i subministraments: despeses de gestor, subministraments, comunicacions, manteniment, reparacions, transport, dietes, assegurança de responsabilitat civil, vehicles.

*De 1 ½ a 2 pàgines.*

### 3 DAFO

L'anàlisi DAFO ens ajuda a dissenyar la nostra estratègia mitjançant l'avaluació de quatre aspectes, classificables en interns/externs i positius/negatius:

*   Amb origen intern, tenim fortaleses (positives) i debilitats (negatives).
*   Amb origen extern, tenim oportunitats (positives) i amenaces (negatives).

**Fortaleses**: visuals i mostrats o ocults .. Què et fa diferent? Què destaca?

*   De què ets bo de forma natural?
*   Quines habilitats heu treballat per desenvolupar?
*   Quins són els vostres talents o els regals naturals?
*   Què tan forta és la vostra xarxa de connexions?
*   Què veuen les altres persones com a punts forts?
*   Quins valors i ètica us distingeixen dels companys?

**Debilitats**: coses a millorar. Obstacles a la vostra vida / carrera professional.

*   Hàbits i trets negatius de treball.
*   Educació / formació que necessita millorar.
*   Què consideren els altres com a debilitats?
*   On es pot millorar?
*   Què tens por de fer o probablement evites? Què t’espanta? Falta confiança per fer-ho?

**Oportunitats**: factors externs per aprofitar per dur a terme els vostres somnis.

*   Estat de l’economia.
*   Noves tecnologies o temes a tractar.
*   Demanda de l'habilitat o el tret que posseeix.
*   Hi ha la necessitat que ningú compleixi la seva carrera professional / lloc de treball / indústria?
*   Esteu en una indústria en creixement?
*   Podeu assistir a xarxa, classes o conferències?
*   Algun dels seus punts forts us ofereix una oportunitat?
*   Alguna de les seves debilitats es convertiria en una oportunitat si es supera?

**Amenaces**: factors externs que poden fer mal o disminuir les possibilitats d’assolir els vostres objectius.

*   Alguna de les seves debilitats inhibeixen la capacitat de créixer / augmentar / millorar (company / lfe)?
*   La vostra indústria contracta o canvia / canvia d’orientació?
*   Hi ha una forta competència per als llocs de treball que més s’adapten?
*   Quin és el perill extern més gran dels vostres objectius?
*   Hi ha noves normes professionals que no pugui complir?
*   Hi ha nous requisits tecnològics / educatius o de certificació que impedeixin el vostre progrés?

Relaciona els punts forts amb les oportunitats per saber on centrar-se i prendre mesures.

Compondre amb debilitats i amenaces et mostra on treballar i millorar.

Cerqueu qualsevol manera de convertir les vostres debilitats o amenaces en oportunitats o punts forts.

*1 pàgina.*

### 4 Canvas (actualitzat)

### 5 Taula financera (actualitzada)

Al checkpoint anterior vas descriure les **despeses**. Descriu ara la **previsió de vendes** anuals dels tres primers exercicis. Quina seria la previsió de vendes mensual i diària per al primer exercici? (fes la divisió)

## Quart checkpoint

### 1 Pla jurídic

Prendre com a referència el pla d'empresa Vapor Llonch. 

*Fer 2 pàgines.*

#### 1.1 Forma jurídica

Indicar la forma jurídica escollida i motius de l'elecció.

#### 1.2 Tràmits

Elaborar una llista dels tràmits i els costos pertinents.

#### 1.3 Obligacions fiscals

Descriure les obligacions pròpies de la forma jurídica escollida.

#### 1.4 Obligacions comptables

Descriure les obligacions laborals que tindrà l'empresa, així com el règim de la seguretat social al que s'acolliran els socis i la contractació prevista, i així mateix, calcular el cost laboral.

#### 1.5 Seguretat social

Altres aspectes a destacar en relació amb l'activitat: prevenció de riscos laborals, qualitat, medi ambient, etc.

### 2 Pla econòmic financer

Prendre com a referència el pla econòmic i financer autònom (punt 3). 

*Fer 2 pàgines i 1/2.*

#### 2.1 Pla d'inversió inicial

Detalla al màxim l'explicació de la inversió per a dur a terme el projecte, a més d'omplir el requadre de la plantilla.

#### 2.2 Pla de finançament

Detallar al màxim l’explicació del pla de finançament (% recursos propis, aportació dels socis, nombre de socis, endeutament dels socis, condicions del préstec, etc.). Omple el requadre de la plantilla.

#### 2.3 Simulació del préstec

Utilitza un simulador per omplir el préstec de la plantilla. A internet trobaràs diferents simuladors.

#### 2.4 Sistema de cobrament als clients i sistema de pagament als proveïdors

Detallar el sistema de cobrament dels clients i pagament als proveïdors (contat o diferit en el temps).

#### 2.5 Explicació del càlcul dels ingressos i la previsió d'ingressos d'explotació

Molt important, a més de quantificar els ingressos, és detallar com s’ha realitzat el càlcul dels ingressos. Utilitza la taula de la plantilla.

#### 2.6 Previsió de despeses d'explotació

Quantifica les despeses d'explotació i calcula el resultat d'explotació. Utilitza la taula de la plantilla.

#### 2.7 Tipus d'IVA repercutit i suportat de l'activitat

Explicació del tipus d’IVA suportat i repercutit, així com del sistema tributari de l’IVA (estimació directa, indirecta, etc.).

## Lliurament final

### 1 Els quatre checkpoints revisats

Cal incloure tots els comentaris fets pel professor durant els quatre checkpoints, i totes les actualitzacions que calguin en funció del desenvolupament del pla.

### 2 Conclusions

La conclusió és l'apartat on fareu una valoració del procés de planificació d'aquesta primera idea de negoci que vàreu plantejar a l'inici. També es poden remarcar quins entrebancs heu trobat durant l'elaboració del pla i com els heu donat solució.

És aquí on senyalareu els canvis que ha experimentat la idea inicial i quins són els punts remarcables del pla. Podeu fer una projecció de futur del vostre model de negoci i de quins són els objectius que voldríeu assolir. Cal finalment agrair atots aquells que han fet possible que la vostra tasca arribés a bon port.

### 3 Maquetació

Tapa, contratapa, índex i paginació.

