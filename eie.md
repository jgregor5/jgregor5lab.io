---
layout: page
title: Empresa
permalink: /eie/
has_children: true
nav_order: 6
nav_exclude: false
---
Resultats d'aprenentatge: 

1.  Reconeix les capacitats associades a la iniciativa emprenedora, analitzant els requeriments derivats dels llocs de treball i de les activitats empresarials.
2.  Defineix l’oportunitat de creació d’una microempresa, valorant l’impacte sobre l’entorn d’actuació i incorporant valors ètics.
3.  Realitza activitats per a la constitució i posada en marxa d’una microempresa de desenvolupament d’aplicacions multiplataforma, seleccionant la forma jurídica i identificant-ne les obligacions legals associades.
4.  Realitza activitats de gestió administrativa i financera d’una microempresa de desenvolupament d’aplicacions multiplataforma, identificant-ne les obligacions comptables i fiscals principals i coneixent-ne la documentació.
