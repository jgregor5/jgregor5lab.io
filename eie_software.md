---
layout: page_toc
title: Sector de la informàtica
permalink: /eie_software/
parent: Empresa
nav_order: 3
---
## Software a la economia

El software és omnipresent:

-   Substituint negocis tradicionals, com llibreries, publicitaris, música, telecomunicacions, selecció de personal, serveis financers, etc.
-   Menjant-se la cadena de valor de diferents negocis, tot i no substituir-los, com a la fabricació de cotxes, la carrera espacial, la logística i la distribució, etc.

El progrés humà podria veure's com a quatre revolucions industrials:

1.  El vapor, l'aigua i la producció mecànica.
2.  La divisió del treball, l'electricitat i la producció massiva.   
3.  L'electrònica, la informàtica i la producció automatitzada.
4.  L'anàlisi de dades, els dispositius mòbils, la intel·ligència artificial, el machine learning, la robòtica i la genòmica.

No sempre podem fer investigació des de l'empresa. Però l'ús de les [tecnologies de propòsit general](https://en.wikipedia.org/wiki/General-purpose_technology) ens dona un marc d'innovació que podem integrar en el nostre negoci.

La tecnologia ha transformat els negocis i també la societat, produint **canvis disruptius** a cadascuna d'aquestes revolucions que han afectat les persones i les seves feines. 

## Estratègia de negoci

La innovació permet a l'empresa entrar al mercat i adaptar-se als canvis. L'empresa software ho pot fer de diverses maneres:

-   Fent la creativitat un hàbit (cultura corporativa).
-   Tenir un [cicle de desenvolupament (SDLC)](https://en.wikipedia.org/wiki/Systems_development_life_cycle) àgil.
-   Tenir un espai de treball (físic o virtual) funcional, flexible i mòbil. Tenim eines de gestió de codi i DevOps al nostre abast.

Què necessita el món empresarial del software?

-   Hi ha negocis completament software:
    -   Creació, adaptació, ampliació i manteniment d'aplicacions off-the-shelf i a mida.
    -   Sharing economy.
    -   Software as a Service.
-   Tenim la tasca de la digitalització d'empreses:
    -   Presència a internet (web, xarxes).
    -   Estratègia de comunicació i màrqueting.
    -   Venda online.
    -   Digitalització dels processos de gestió (CRM, ERP, treball col·laboratiu, teletreball).
-   Tenim feina a les activitats de la cadena de valor:
    -   Serveis dins de les empreses, especialment en automatització, qualitat, anàlisi de dades i business intelligence.
    -   Interacció entre empreses (B2B), com adaptació de protocols, API Rest.

La nostra estratègia go-to-market hauria d'incloure:

-   La visibilitat d'un equip innovador i ben format.
-   El nostre portfolio de solucions i creativitat.
-   Solucions sensibles a l'èxit del client a l'estructura de costos (per ús).
-   Bona comunicació escrita en les propostes tècniques, justificades i amb detall.
-   Codi font lliurat al client amb la promesa que podrà ser assumit per qualsevol altre proveidor.

## Deute tècnic

El deute tècnic és el resultat de decisions tècniques dolentes o subòptimes que finalment generen problemes per a mantenir i expandir una solució. Com a integrants de l'economia del software, tenim una responsabilitat en reduir aquest deute.

La raó del deute tècnic pot ser:

-   El resultat d'una decisió basada en paràmetres de negoci i no sostenible.
-   El resultat de fer un mal disseny i la seva implementació del codi.

En ambdós casos, el resultat és el mateix: un codi que no s'entén, que no és robust, que està mal documentat, que provoca infelicitat als programadors i que molt difícilment es pot modificar o estendre.

Què podem fer per reduir el deute?

-   Tenir clara l'estratègia tècnica inicial. Fer una bona avaluació de les possibilitats, i fer-ho amb un objectiu ben definit.
-   Tenir un responsable d'arquitectura.
-   Dissenyar amb la seguretat com a requisit.
-   Pensar com es podrien substituir els components o llibreries de la nostra solució.
-   Mantenir la refactorització a petita escala dins del procés, millorant el codi.
-   Tenir testos de regressió automatitzats.
-   Millorar la cobertura dels tests.
-   Reescriure codi, si cal. És un moviment perillós, però de vegades necessari.
-   Fer una bona documentació.
-   Tenir bons canals de comunicació per a gestionar problemes.

## Model de negoci

El nostre model de negoci software ha de descriure el tipus de negoci dins del context del mercat, a qui va dirigit i com s'aconseguiran els ingressos necessaris.

El model de negoci descriu com fa una organització per a crear, lliurar i capturar valor en un context econòmic, social o cultural.

Si utilitzem el canvas com a eina per a definir el model de negoci, podem trobar els següents aspectes.

### Segments de clients

Podem caracteritzar els client segons una sèrie de paràmetres.

Segons l'audiència:
-   B2B (venda a altres empreses)
-   B2C (venda a usuari final)
-   C2C (marketplace).

Segons l'àmbit del producte:
-   Off-the-shelf ("caixa"): generalista, solució global. Pot permetre customització o implementar personalització.
-   A mida: fet a mida, per a un problema concret del client.
-   Híbrid: producte off-the-shelf que permet desenvolupaments a mida gràcies a un API o similar. Pot ser obert o tancat.

Segons la plataforma destí:
-   Per a una o diverses plataformes: Android, iOS, etc.
-   Plataforma agnòstica. Habitualment basada en web.

Segons la interacció dels usuaris:
-   One-to-many (clients)
-   Many-to-many (usuaris productors i usuaris consumidors).

Segons el model de llicències:
-   Propietari
-   Open-source

### Proposta de valor

Explicar la [proposta de valor](https://en.wikipedia.org/wiki/Value_proposition): una afirmació que identifica els beneficis clars, mesurables i demostrables que els clients obtenen en comprar cadascun dels productes o serveis. Caldria explicar-ho per cada segment.

En software significa explicar almenys les funcionalitats, el rendiment, l'arquitectura i el model de suport.

### Canals

Fases:
1.  Coneixement: web, màrqueting online.
2.  Avaluació: freemium, trial.
3.  Compra: contracte, e-commerce, store.
4.  Lliurament: on-premise vs off-premise (cloud) vs híbrid.
5.  Suport: issue/bug tracker, CRM.

Segons el lliurament:
-   On-premise: el software s'instal.la i funciona a les instal.lacions del client.
-   Cloud-based: el software funciona al núvol o a un proveïdor de allotjament (SaaS).
-   Híbrid: barreja els dos anteriors. Hi ha instal.lació, però també es compta amb el núvol per al seu funcionament.

### Relació amb els clients

Segons el model de negoci. Podem utilitzar eines CRM integrades en el nostre ERP, comunitats a les xarxes, auto-servei.

### Ingressos

Podem tenir un o més fluxos:
-   Aplicacions de pagament. Els clients paguen per instal·lar un producte.
-   Publicitat a l'aplicació. L'aplicació és gratuïta, però veneu llocs d'aplicacions per a publicitat.
-   Compres des de l'aplicació. L'aplicació és gratuïta, però guanyes venent productes o serveis mitjançant una aplicació.
-   Subscripcions. Els usuaris paguen anualment o mensualment una quota de subscripció.
-   Model d'ingressos de programari basat en l'ús. Els clients paguen només pel que fan servir.
-   Desenvolupaments a mida. Els clients paguen per desenvolupar funcionalitats a mida.
-   Càrrecs per suport, serveis empresarials i consultoria.

### Activitats clau

-   Programació.
-   Suport i consultoria.
-   Anàlisi, venda o accés a dades.
-   Optimització interna i innovació.
-   Formació contínua.

### Recursos clau

-   El nostre codi, que pot ser propietari o open source, i llibreries de tercers.
-   Les nostres dades (com a actius).
-   Programadors que donen valor a l'empresa.
-   Ordinadors.
-   Local físic vs teletreball.
-   Subscripcions a serveis o comptes de desenvolupament.

### Costos

Revisar els costos dels recursos clau.

### Socis clau

-   Dependència d'empreses tecnològiques.
-   Store amb les seves normes i tecnologies.
-   APIs de tercers.

## Impacte social

Els codis deontològics que hi ha a les referències d'aquest document ens donen una visió general de l'impacte de la professió de desenvolupament de software. La responsabilitat podria veure's en tres àrees: negoci, tecnologia i societat.

### Negoci

La pregunta a fer-se és: segueixo l'estratègia correcta? Aquesta estratègia ha de ser compatible a la de les altres dues àrees esmentades. 

Hem d'assegurar-nos de complir tota la llei i normativa en relació a la societat. En particular:

-   El [reglament general de protecció de dades europeu (RGPD)](https://gdprinfo.eu/es).
-   La llei orgànica de protecció de dades personals i garantia dels drets digitals, que compatibilitza la legislació espanyola amb el RGPD.
-   La [llei de serveis de la societat de la informació i del comerç electrònic](https://lssi.mineco.gob.es/).

### Tecnologia

Ens hem de preguntar: utilitzo l'eina correcta per la feina? És quelcom nou o reinventem la roda? Estic generant **deute tècnic**? Aquestes decisions impactaran al mercat del desenvolupament software, vist com un col.lectiu que interactua i acaba reprenent o col.laborant en solucions tècniques.

### Societat

Ens preguntarem: millorem les vides de les persones? Pot utilitzar-se de forma nociva? Exclou persones? És ètic? És segur? A qui pertanyen les dades?

En aquesta àrea ens podem preguntar:

-   Si utilitzem les eines i solucions més responsables socialment, com per exemple, el codi obert.
-   Si respectem el [dret a la privatitat](https://en.wikipedia.org/wiki/Right_to_privacy) en el tractament de les dades de les nostres solucions.
-   Si seguim les [línies universals de l'IA](https://thepublicvoice.org/ai-universal-guidelines/).

## Impacte ambiental

**Computació verda**: ús eficient dels recursos informàtics, minimitzant l'impacte ambiental. Aproximacions:

1.  Longevitat dels productes, ja que el procés de fabricació és la part més significativa de l'ús de recursos naturals.
2.  Disseny eficient dels data centers.
3.  Eficiència del software (algorismes, assignació de recursos, virtualització, servidors de terminals).
4.  Gestió de energia amb l'ús de components no utilitzats, reduir voltatges, parar màquines, fonts d'energia més eficients, etc.
5.  Reciclatge d'equips per ser reutilitzats.
6.  Cloud computing (virtualització) versus edge computing (més a prop).
7.  Teletreball, reduint el transport.

En particular, **programació verda**:

1.  Minimitzar l'emissió de CO2.
2.  Dissenyar les aplicacions eficientment per reduir el consum d'energia.
3.  Consumir electricitat amb la intensitat de CO2 mínima (mix de fonts).
4.  Construir aplicacions que siguin eficients amb el hardware, estenent la seva vida.
5.  Maximitzar l'eficiència energètica del hardware, reduïnt el nombre de servidors amb la major ràtio d'utilització.
6.  Reduit la mida i la distància recorreguda de les dades per la xarxa.
7.  Construir aplicacions conscients del CO2, que permetin gestionar la demanda i moure-la a regions o moments de menys intensitat.
8.  Per poder optimitzar, mesurar el CO2, l'energia, el cost, l'ús de xarxa i el rendiment.

## Referències

*   [Why software is eating the world](https://a16z.com/2011/08/20/why-software-is-eating-the-world/)
*   [Social Responsibility Impacts Software Development Processes](https://www.itprotoday.com/devops-and-software-development/social-responsibility-impacts-software-development-processes)
*   [How to recognize exclusin in AI](https://medium.com/microsoft-design/how-to-recognize-exclusion-in-ai-ec2d6d89f850)
*   [Principles of Green Software Engineering](https://principles.green/)
*   [Green Coding: Sustainability in Software](https://whossavingtheplanet.com/read/innovation/green-coding-sustainability-in-software/2020-10-04)
*   [Codi Deontològic](https://enginyeriainformatica.cat/about/oglecodi-deontologic/)
*   [Código Ético y Deontológico de la Ingeniería Informática](https://ccii.es/CodigoDeontologico)
*   [Programming ethics](https://en.wikipedia.org/wiki/Programming_ethics)