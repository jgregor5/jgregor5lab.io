---
layout: page_toc
title: Sòcols i serveis
permalink: /socols_serveis/
parent: Serveis i processos
nav_order: 2
---
Resultats d'aprenentatge:

1.  Programa mecanismes de comunicació en xarxa emprant sòcols i analitzant l’escenari d’execució.
1.  Desenvolupa aplicacions que ofereixen serveis en xarxa, utilitzant llibreries de classes i aplicant criteris d’eficiència i disponibilitat.

## Protocols

Els protocols d'Internet tenen més d'un model. El [model OSI](https://en.wikipedia.org/wiki/OSI_model) ens dona una organització per capes:

1.  **Física**: transmissió i recepció de bits sobre el mitjà físic.
2.  **Enllaç**: transmissió fiable de trames entre dos nodes. PPP.
3.  **Xarxa**: transmissió de paquets sobre una xarxa multi-node, amb adreçament, enrutament i control de tràfic. IP.
4.  **Transport**: transmissió de segments de dades entre punts d'una xarxa. TCP, UDP.
5.  **Sessió**: gestió de sessions.
6.  **Presentació**: traducció dels protocols cap a una aplicació. MIME, SSL.
7.  **Aplicació**: APIs d'alt nivell. HTTP, Websockets.

![](/images/networkstack.png)

Com a programadors, podem involucrar-nos a diferents nivells del model:

*   Construint protocols basats en TCP, UDP mitjançant Java Sockets (nivell 4 i mig).
*   Accedint a aplicacions web HTTP (nivell 7), implementades als ports 80 o 443 (segur).

Els protocols de nivell 4 i mig estàndards es poden veure a [aquesta llista](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers).

### Classes d'adreces IP

Les adreces IPv4 tenen rangs assignats a diferents propòsits:
*   Classe A: de 1.x.x.x a 127.x.x.x (subnet mask 255.0.0.0). El rang 127.x.x.x està reservat per al loopback.
*   Classe B: de 128.0.x.x a 191.255.x.x (subnet mask 255.255.0.0).
*   Classe C: de 192.0.0.x a 223.255.255.x (subnet mask 255.255.255.0).
*   Classe D: de 224.0.0.0 a 239.255.255.255. Reservades per a multicasting.
*   Classe E: de 240.0.0.0 a 255.255.255.254. Reservades per a recerca.

D'aquestes, es consideren IP privades:
*   10.0.0.0 a 10.255.255.255
*   172.16.0.0 a 172.31.255.255
*   192.168.0.0 a 192.168.255.255

### Tipus d'enrutaments

**Unicast**: adreces de classe A, B i C. Transport TCP i UDP.

![](/images/unicast.png)

**Broadcast**: Adreça del host amb tot 1's. Transport UDP. No travessen els enrutadors, i les reben totes les màquines.

![](/images/broadcast.png)

**Multicast**: adreces de classe D. Transport UDP.  Una màquina ha d'escoltar per rebre la comunicació. Disponible a xarxes locals.

![](/images/multicast.png)

### Eines HTTP

Tenim tres eines per depurar protocols HTTP: netcat, curl i l'inspector dels navegadors.

Netcat permet connectar-se a un port i fer una conversa, utilitzant les canonades. Si s'indica -u utilitza UDP, si no, TCP. Per exemple, per a accedir al servei echo de la nostra màquina:

```shell
$ nc localhost 7
```

CURL permet obtenir la resposta d'una URL a la xarxa.

```shell
$ curl -I http://maripili.es
(GET, veure headers)
HTTP/1.1 200 OK
Date: Fri, 05 Apr 2019 05:03:23 GMT
Server: Apache
X-Logged-In: False
P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"
Cache-Control: no-cache
Pragma: no-cache
Set-Cookie: 4af180c8954a0d5a1965b5b1b23ccbc5=pc1jg8f5kqigd71iec5a5lm7k5; path=/
X-Powered-By: PleskLin
Content-Type: text/html; charset=utf-8

$ curl http://maripili.es
(GET, contingut de la pàgina web)

$ curl -v http://maripili.es
(GET, headers i contingut)

$ curl -d "key1=val1&key2=val2" http://maripili.es/contacto/
(POST)
```

![](/images/inspect.png)

### Introducció a HTTP

HTTP és un protocol de nivell aplicació per a sistemes col·laboratius i distribuïts. És el component principal de la web, gràcies a l'ús de documents d'hipertext. HTTP/1.1, la versió actual, està implementat mitjançant TCP al transport. La versió 2 ja està estandaritzada, i la 3 funcionarà sobre UDP.

La versió segura d'HTTP es diu HTTPS, o també HTTP sobre TLS, el protocol criptogràfic per a la transmissió segura.

#### Sessió

Una sessió és una seqüència de peticions/respostes. Comença mitjançant l'establiment d'una connexió TCP a un port d'un servidor (habitualment 80). El servidor contesta habitualment amb un codi, del tipus "HTTP/1.1 200 OK", i amb un cos, que normalment conté el recurs demanat.

HTTP es un protocol **sense estat**, tot i que algunes aplicacions utilitzen mecanismes per emmagatzemar informació. Per exemple, les **cookies**.

#### Missatges

Una petició conté, habitualment:

*   Una línia de petició, amb un mètode. Exemple: GET /images/logo.png HTTP/1.1
*   Camps de la capçalera de la petició. Exemple: Accept-Language: ca
*   Una línia buida.
*   Un cos opcional. Exemple: per fer un POST.

Mètodes de petició:

*   **GET**: el mètode habitual per obtenir un recurs. No té cos.
*   **POST**: el mètode utilitzat per enviar un cos al servidor. S'utilitza als formularis.
*   PUT, DELETE, TRACE, OPTIONS, CONNECT, PATCH son altres mètodes utilitzats.

Una resposta conté:

*   Una línia d'estat. Exemple: HTTP/1.1 200 OK.
*   Camps de la capçalera de la resposta. Exemple: Content-Type: text/html
*   Una línia buida.
*   Un cos opcional. Exemple: per a un GET, el contingut requerit.

Els [codis d'estat](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) poden ser del tipus:

*   Informació (1XX).
*   Èxit (2XX). Exemple: 200 OK.
*   Redirecció (3XX). Exemple: 301 Moved Permanently.
*   Error de client (4XX). Exemple: 404 Not Found.
*   Error de servidor (5XX). Exemple: 500 Internal Server Error.

Podem utilitzar el programa telnet per conectar-nos a un servidor web HTTP i enviar una comanda GET.

```shell
$ telnet maripili.es 80
Trying 217.160.0.165...
Connected to maripili.es.
Escape character is '^]'.
GET / HTTP/1.0
Host: maripili.es

```

Això provoca la resposta del servidor:

```shell
HTTP/1.1 200 OK
Content-Type: text/html; charset=UTF-8
Connection: close
Date: Wed, 16 Feb 2022 08:12:31 GMT
Server: Apache

<!DOCTYPE html>
<html lang="es">
...
</html>
Connection closed by foreign host.
```

### [URL](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/URL.html) i [HttpURLConnection](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/HttpURLConnection.html)

La classe URL fa referència a un recurs a la WWW. Un recurs genèric pot tenir la següent forma.

![](/images/url.png)

Veiem un exemple per al protocol HTTP:

[`https://www.example.com/test?key1=value1&key2=value2`](https://www.example.com/test?key1=value1&key2=value2)

En aquest cas, tenim que:

*   l'esquema és https
*   el host és [www.example.com](http://www.example.com/)
*   el port és 80, però no s'indica, ja que és el valor per defecte al protocol HTTP
*   el camí (path) és test
*   la query és key1=value1&key2=value2

A Java es pot construir una URL amb:

```java
URL url = new URL(String spec)
```

Un cop fet això, podem accedir a cada part de l'URL amb els mètodes getHost(), getPath(), getPort(), getProtocol(), getQuery(), etc.

Els dos mètodes més importants per interactuar amb l'URL són:

*   `URLConnection openConnection()`: retorna una connexió al recurs remot.
*   `InputStream openStream()`: retorna un InputStream per a llegir el recurs remot.

La classe `URLConnection` és abstracta, i si hem accedir a un recurs HTTP llavors l'objecte serà una instància de `HttpURLConnection`.

#### openStream

Per llegir una pàgina web que es trobi a l'URL d'una cadena anomenada urlText, podem fer:

```java
URL url = new URL(urlText);
BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
```

Per llegir un arxiu:

```java
BufferedInputStream in = new BufferedInputStream(new URL(urlText).openStream());
```

#### openConnection

Amb openConnection podem accedir als mètodes del protocol HTTP i els codis d'estat que es retornen o el tipus de contingut.

Aquest és un mètode GET:

```java
URL url = new URL(urlText);
HttpURLConnection httpConn = ((HttpURLConnection) url.openConnection());
httpConn.setRequestMethod("GET"); // opcional: GET és el mètode per defecte
int responseCode = httpConn.getResponseCode();
String contentType = httpConn.getContentType();
BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
// falta llegir in: resposta del servidor
```

Aquest és un mètode POST:

```java
URL url = new URL(urlText);
HttpURLConnection httpConn = ((HttpURLConnection) url.openConnection());
httpConn.setRequestMethod("POST");
httpConn.setDoOutput(true);
OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
out.write("propietat1=valor1&propietat2=valor2"); // valors dels paràmetres del POST
out.close();
int responseCode = httpConn.getResponseCode();
String contentType = httpConn.getContentType();
BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
// falta llegir in: resposta del servidor
```

## Sòcols

Un **sòcol** és un enllaç de doble sentit que permet comunicar dos programaris que són a la xarxa.

Aquests dos programaris fan dues funcions: la del **client** i la del **servidor**. El servidor proveeix algun servei des d'un lloc conegut (adreça IP + port), i el client accedeix a aquest servei. Aquest servei ha d'implementar un protocol ben definit, sigui un estàndard o un de dissenyat a mida.

Els [números de ports](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml) són:

*   El rang 0 a 1023 són els ports coneguts (well-known) o de sistema. En Linux, cal ser administrador per tenir un servei en aquests ports.
*   El rang 1024-49151 són els ports registrats, assignats per IANA.
*   El rang 49152–65535 són ports dinàmics o privats, o de vida curta.

Com que els dos programaris treballen en l'àmbit del protocol, al codi dels dos programaris **no hi ha dependències** mútues. Però és habitual que qui implementa el protocol proveeixi d'una **llibreria de client** per poder accedir al servei. Això permet reduir el codi que un client ha d'escriure, i assegura que utilitzarà correctament el protocol. A Java, la llibreria de client es materialitza mitjançant un arxiu jar i una documentació d'ús.

#### TCP i UDP

Es poden utilitzar els protocols TCP o UDP. TCP està orientat a connexió, i UDP no. Això vol dir que TCP requereix un pas previ de connexió entre el client i el servidor per tal de comunicar-se. Un cop establerta la connexió, TCP garanteix que les dades arribin a l’altre extrem o indicarà que s’ha produït un error.

En general, els paquets que han de passar en l'ordre correcte, sense pèrdues, utilitzen **TCP**, mentre que els serveis en temps real on els paquets posteriors són més importants que els paquets més antics utilitzen **UDP**. Per exemple, la transferència d’arxius requereix una precisió màxima, de manera que normalment es fa mitjançant TCP, i la conferència d’àudio es fa freqüentment a través d’UDP, en què pot ser que no es notin les interrupcions momentànies.

**TCP** necessita uns paquets de control per a **establir la connexió** en tres fases: SYN, SYN + ACK i ACK. Cada paquet enviat es contesta amb un ACK. I finalment, es produeix una **desconnexió** des de les dues bandes amb FIN + ACK i ACK.

**UDP**, en canvi, només transmet els paquets de petició / resposta, sense cap control sobre la transmissió.

### Protocol exemple: ECHO

A continuació es pot veure la visualització del protocol ECHO amb Wireshark, tant per a la implementació TCP com la UDP.

#### Captura TCP (petició i resposta)

![](/images/echotcp1.png)
![](/images/echotcp2.png)

#### Captura UDP (petició i resposta)

![](/images/echoudp1.png)
![](/images/echoudp2.png)

### Protocol exemple: SMTP

SMTP és un protocol que funciona al port 25, sobre TCP. El client envia comandes, i el servidor respon amb un codi d'estat.

A continuació, veiem una conversa (C: client / S: servidor). Tota aquesta conversa es manté sobre una **connexió oberta**.

```
C: <client connects to service port 25>
C: HELO snark.thyrsus.com                 la máquina que envia s'identifica
S: 250 OK Hello snark, glad to meet you   el receptor accepta
C: MAIL FROM: <esr@thyrsus.com>           identificació de l'usuari que envia
S: 250 <esr@thyrsus.com>... Sender ok     el receptor accepta
C: RCPT TO: cor@cpmy.com                  identificació del destí
S: 250 root... Recipient ok               el receptor accepta
C: DATA
S: 354 Enter mail, end with "." on a line by itself
C: Scratch called.  He wants to share
C: a room with us at Balticon.
C: .                                      final de l'enviament multi-línia
S: 250 WAA01865 Message accepted for delivery
C: QUIT                                   l'emissor s'acomiada
S: 221 cpmy.com closing connection        el receptor es desconnecta
C: <client hangs up>
```

### Comunicació TCP

Java té dues classes del paquet java.net que ho permeten:

*   `Socket`: implementació del sòcol client, que permeten comunicar dos programaris a la xarxa.
*   `ServerSocket`: implementació del sòcol servidor, que permet escoltar peticions rebudes des de la xarxa.

El funcionament es reflecteix en les següents dues imatges: primer el client demana una connexió, i després el servidor l'accepta, i s'estableix.

![](/images/socketconn1.gif)
![](/images/socketconn2.gif)

#### Servidor a majúscules

Aquest servidor escolta línies de text i retorna la versió en majúscules.

La comunicació comença amb la petició de connexió del client, i l'acceptació del servidor. Aquestes dues accions creen un sòcol compartit del tipus Socket, sobre el qual, tan el client com el servidor, poden utilitzar els mètodes:

*   `getInputStream()`
*   `getOutputStream()`

El client pot enviar cadenes de text, que el servidor convertirà a majúscules.

El protocol que ens hem inventat estableix que la comunicació s'acaba quan el client envia una línia buida. a la qual el servidor contesta amb un comiat.

![](/images/amajuscules.png)

Aquest podria ser un codi del **servidor** que implementa el protocol descrit utilitzant **TCP**.

```java
ServerSocket serverSocket = new ServerSocket(PORT);
Socket clientSocket = serverSocket.accept();
PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

out.println("hola!");        

String text;
while ((text = in.readLine()).length() > 0)        
        out.println(text.toUpperCase());

out.println("adeu!");
clientSocket.close();
serverSocket.close();
```

Pots provar-ho mitjançant la comanda netcat (nc).

Com seria el protocol d'aquest servei? En pseudocodi:

1.  Quan et connectes al servidor, envia una línia amb una salutació.
2.  Per cada línia que envies, et retorna la mateixa línia en majúscules.
3.  Quan envies una línia en blanc, et contesta amb el comiat, i es desconnecta.

A continuació, es pot veure un **client** que accedeix a aquest servei, implementant aquest protocol.

```java
Socket clientSocket = new Socket(HOST, PORT);
PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); 

String salutacio = in.readLine();
System.out.println("salutacio: " + salutacio);

for (String text: new String[]{"u", "dos", "tres"}) {
    out.println(text);
    String resposta = in.readLine();
    System.out.println(text + " => " + resposta);
}

out.println();

String comiat = in.readLine();
System.out.println("comiat: " + comiat);

in.close();
out.close();
clientSocket.close();
```

### Comunicació UDP

Amb UDP no hi ha connexió: simplement s'envien paquets (Datagrames) amb destinació un servidor UDP. Si volem respondre, cal conèixer l'adreça i port destí, que pot obtenir-se del paquet rebut.

Un **servidor** es pot crear amb:

```java
DatagramSocket socket = new DatagramSocket(PORT)
```

El **client** funciona exactament igual, però el socol es crea amb:

```java
DatagramSocket socket = new DatagramSocket();
```

Per rebre un paquet de mida màxima MIDA:

```java
byte[] buf = new byte[MIDA];
DatagramPacket paquet = new DatagramPacket(buf, MIDA);
socket.receive(paquet);
// dades a paquet.getData() i origen a paquet.getAddress() i paquet.getPort()
```

Per enviar un paquet al servidor:

```java
InetAddress address = InetAddress.getByName(HOST);
paquet = new DatagramPacket(buf, MIDA, address, PORT);
socket.send(paquet);
```

Per enviar un paquet de resposta a un client, hem d'utilitzar el port que hi ha al paquet que ens ha enviat prèviament:

```java
InetAddress address = paquetRebut.getAddress();
int port = paquetRebut.getPort();
DatagramPacket paquetResposta = new DatagramPacket(buf, buf.length, address, port);
socket.send(paquet);
```

#### Concurrència

Com podem fer que els servidors acceptin **peticions concurrents** de diversos clients?

Ho vam veure a la UF "Processos i fils". Caldria atendre cada petició a un fil diferent. Per exemple, per al cas de TCP:

```java
Executor executor = Executors.newFixedThreadPool(NFILS);
ServerSocket serverSocket = new ServerSocket(PORT);
while (true) {
    final Socket clientSocket = serverSocket.accept();
    Runnable tasca = new Runnable() {
        public void run() {
            atendrePeticio(clientSocket);
        }
    };
    executor.execute(tasca);
}
```

D'aquesta manera, no fem esperar nous clients quan atenem un.

### Comunicació asíncrona

Basada en les llibreries NIO. Veure [aquest post](https://www.baeldung.com/java-nio-2-async-channels).

## Serveis

### Arquitectures

Una aplicació pot veure's com quatre components: les dades, la lògica d'accés a dades, la lògica de l'aplicació i la presentació. Aquests components es poden distribuir de moltes formes:

*   **basades en servidor**: el servidor fa pràcticament tota la feina. Els clients són molt lleugers.
*   **basades en client**: el client fa pràcticament tota la feina. El servidor només guarda les dades.
*   **peer-to-peer**: les màquines fan de client i servidor i comparteixen la feina, que fan integralment.
*   **client/servidor**: l'arquitectura dominant. La lògica de l'aplicació i d'accés a dades pot estar distribuïda entre client i servidor. Poden tenir múltiples capes: 2, 3, N. Permet integrar aplicacions de diferents proveïdors utilitzant protocols estàndard. Aquesta és l'arquitectura dominant.

L'arquitectura client/servidor està centrada habitualment en les dades: la lògica de negoci s'interposa entre aquestes dades i la interfície d'usuari, habitualment web. Un exemple de patró és el MVC (model/vista/controlador).

Si atenem al criteri d'on es genera l'HTML d'una aplicació web podem tenir:

*   El **tradicional**: l'HTML es genera al servidor.
*   El **SPA** (Single-Page Application): l'HTML es genera al client, i amb el servidor s'intercanvien dades (JSON, habitualment). Al servidor implementem APIs basades en HTTP, que poden compartir-se amb diferents tipus de clients com navegadors o aplicacions per mòbil.

Quan les funcionalitats creixen i s'afegeixen a una solució, tenim el risc de convertir la nostra aplicació en el que s'anomena [aplicació monolítica](https://en.wikipedia.org/wiki/Monolithic_application). Algunes solucions arquitecturals proposen solucions:

*   **Arquitectura de microserveis**: proposa serveis completament independents que proporcionen funcionalitats autocontingudes. Tots ells es comuniquen amb protocols lleugers (poden ser heterogenis) basats en REST/HTTP gràcies a un contracte ben establert (API). Basats en l'idea del bucle d'esdeveniments.
*   **Arquitectura orientada a serveis (SOA)**: una solució similar a l'anterior, però els serveis es comuniquen utilitzant un middleware més complex anomenat [bus de serveis d'empresa (ESB)](https://en.wikipedia.org/wiki/Enterprise_service_bus). Basats en coordinació de múltiples serveis al bus.

### APIs

El món està cada cop més interconnectat mitjançant APIs que proporcionen serveis. Aquests poden ser **públics**, per tal d'**afegir valor** al negoci d'una empresa.

Les APIs es diuen que són gestionades quan tenen un cicle de vida ben definit:

CREADA ➡ PUBLICADA ➡ OBSOLETA ➡ RETIRADA

Només es publiquen un cop estan ben documentades, amb les seves regles de qualitat d'ús, com la limitació d'ús. La forma estàndard i oberta de descriure APIs és mitjançant [OpenAPI](https://www.openapis.org/).

**Exemple**: [API Twitter](https://developer.twitter.com/en/docs/api-reference-index)

### HTTP

El protocol HTTP s'implementa a sobre de TCP, habitualment al port 80. Això ens permet implementar un servidor HTTP utilitzant sòcols.

Per escriure el servidor, hem de ser capaços de llegir una petició HTTP i de respondre.

#### Petició

```
request = Request-Line
            *(<message-header>)
            CRLF
            [<message-body>]
```

La **Request-Line** té el format:

```
Request-Line = Method URI HTTP-Version
```

Els mètodes més habituals són **GET/POST**. La versió, HTTP/1.1. La URI és només la part del camí (path) absolut.

Els **headers** més habituals són:

*   Host (obligatori): especifica el nom de domini del servidor.
*   Accept: informa al servidor sobre els tipus de dades que es poden rebre.

El **message-body** està buit per al mètode **GET**, i conté els camps d'un formulari per al mètode **POST**.

Quan és POST, s'envia el header Content-Type amb els valors:

*   application/x-www-form-urlencoded: valors codificats en tuples clau-valor separades per &, amb un = entre clau i valor. Els valors no alfanumèrics s'han de codificar en [codi percent](https://en.wikipedia.org/wiki/Percent-encoding). En Java es pot fer amb `URLEncoder.encode(query, "UTF-8")`.
*   multipart/form-data: transmisió de dades binàries, per exemple, un arxiu.
*   text/plain: format text.

#### Resposta

```
response = Status-Line
            *(<message-header>)
            CRLF
            [<message-body>]
```

El **Status-Line** té el format:

```
HTTP-Version Status-Code Reason-Phrase
```

Els codis d'estat ja els vam veure. La Reason-Phrase és un text llegible que explica el codi.

Els **headers** més habituals són:

*   Content-Type: el [tipus MIME](https://www.google.com/url?q=https%3A%2F%2Fdeveloper.mozilla.org%2Fen-US%2Fdocs%2FWeb%2FHTTP%2FBasics_of_HTTP%2FMIME_types&sa=D&sntz=1&usg=AFQjCNGVCt9pl95JSukGN4p70HRw3rJ0Ww) (media) retornat. Pot incloure el charset. Exemple: `text/html; charset=UTF-8`.
*   Content-Length: el nombre de bytes del contingut retornat.
*   Date: la data del contingut retornat.
*   Server: el nom del servidor.

El **message-body** té el contingut del recurs que s'obté.

#### Cookies

Les cookies són un mecanisme que permet emmagatzemar parelles clau/valor al navegador des d'un servidor HTTP. Es pot utilitzar per diferents propòsits, per exemple, per identificar una sessió d'un usuari, o bé per seleccionar una preferència de visualització, com pot ser l'idioma.

Hi ha dues capçaleres associades a aquest mecanisme:

*   **Set-Cookie**: capçalera que s'escriu des de la resposta del servidor per a assignar una cookie.
*   **Cookie**: capçalera que es llegeix des de la petició del navegador amb els valors de les cookies que hi ha emmagatzemades.

Per a esborrar una cookie, només cal enviar la cookie buida amb una data al camp "expires" antiga:

*   Set-Cookie: nomgaleta=; expires=Thu, 01-Jan-1970 00:00:00 GMT;

**Exemple GET/POST d'un formulari HTML:** la URI /form mostra un formulari, que s'omple i processa la URI /submit.

![](/images/getpost.png)

**Exemple de cookie:** la URI /page1 emmagatzema una cookie, que després està disponible a altres pàgines.

![](/images/cookie.png)

**Exemple de redirecció:** la URI /page1 es redirecciona a /page2.

![](/images/redirect.png)

## APIs sobre HTTP

Veurem dos tipus de protocols sobre HTTP: un stateless i un altre stateful.

### RESTful API

REST (Representional State Transfer) és un **estil d'arquitectura** per a sistemes distribuïts. Permet establir comunicació d'aplicacions amb serveis proporcionats a la web. Per tal que una interfície es pugui anomenar RESTful, ha de cumplir una sèrie de principis:

1.  Ha d'implementar un esquema **client/servidor**. Això permet desenvolupar-los de forma independent, i reemplaçar-los.
2.  Ha de ser **stateless** (sense estat en el servidor). Per tant, l'estat s'ha de conservar al client. Això millora l'escalabilitat, la disponibilitat i el rendiment de l'aplicació.
3.  S'ha de donar informació al client (de forma implícita o explícita) de si el contingut és **cacheable**. Així, es pot millorar l'escalabilitat i rendiment.
4.  Ha de tenir una **interfície uniforme**. Bàsicament, un recurs ha de associar-se amb una **URI** que permeti accedir a les seves dades.
5.  Ha de dissenyar-se com a un **sistema per capes**. El client no pot saber específicament l'arquitectura del servei o on es troben les dades, per exemple.
6.  Opcionalment, el client pot **demanar codi** al servidor, per simplificar la seva implementació (poc habitual).

Encara que no és obligatori, un servei RESTful sovint utilitza **HTTP** com a protocol. En aquest cas, els cos de les peticions i les respostes solen tenir el format **XML** o bé **JSON**.

Si ens fixem en les operacions CRUD habituals, hi ha una convenció de [com utilitzar els mètodes HTTP](https://restfulapi.net/http-methods/) utilitzant els codis d'estat 200, 201, 204, 400, 404:

*   GET: llegir (idempotent).
*   POST: crear (no cacheable).
*   PUT: actualitzar/reemplaçar.
*   DELETE: esborrar.
*   PATCH: modificació parcial.

Com que el protocol és sense estat (stateless), la autenticació/autorització ha de produir-se per cada petició. Les [pràctiques recomanades](https://restfulapi.net/security-essentials/) inclouen utilitzar canals segurs, i no exposar mai dades a la URL. També es recomana l'ús d'[Oauth](https://en.wikipedia.org/wiki/OAuth).

L'ús de **tokens**, o paraules d'accés, és habitual als sistemes d'autenticació. El funcionament amb token és el següent:

*   L'usuari o aplicació client accedeix al servei d'autenticació.
*   Si és correcta, el servidor genera un token que envia al client.
*   L'usuari accedeix als recursos amb el seu token.

### Streaming API

Un protocol de tipus streaming és justament una inversió del RESTful. No es tracta d'una conversació. Es tracta d'obrir una connexió entre un client i l'API, on el client va rebent els nou resultats quan es produeixen, en temps real.

La seva naturalesa és stateful, ja que l'API envia els resultats en funció del perfil del client i/o de les regles de filtratge que hagi establert.

És habitual utilitzar el format JSON. En aquest cas, s'utilitza el format text i es poden delimitar els missatges amb fi de línia.

Un exemple és el de [Twitter](https://developer.twitter.com/en/docs/tutorials/consuming-streaming-data).

## Referències

*   [OSI Model](https://en.wikipedia.org/wiki/OSI_model)
*   [HTTP (Mozilla)](https://developer.mozilla.org/en-US/docs/Web/HTTP)
*   [HttpURLConnection](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/net/HttpURLConnection.html)
*   [reqbin.com](https://reqbin.com/)
*   [A guide to Java sockets](https://www.baeldung.com/a-guide-to-java-sockets)
*   [Do a Simple HTTP Request in Java](https://www.baeldung.com/java-http-request)
*   [Core Java Networking (eugenp)](https://github.com/eugenp/tutorials/tree/master/core-java-modules/core-java-networking)
*   [Read an InputStream using the Java Server Socket](https://www.baeldung.com/java-inputstream-server-socket)
*   [REST vs Websockets (baeldung)](https://www.baeldung.com/rest-vs-websockets)
*   [A Guide to the Java API for WebSocket (baeldung)](https://www.baeldung.com/java-websockets)
*   [All About Sockets (The Java Tutorials)](https://docs.oracle.com/javase/tutorial/networking/sockets/)
*   [SSL Handshake Failures (baeldung)](https://www.baeldung.com/java-ssl-handshake-failures)
*   [HTTP: The Protocol Every Web Developer Must Know (part 1)](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177)
*   [HTTP: The Protocol Every Web Developer Must Know (part 2)](https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-2--net-31155)
*   [REST API Tutorial](https://restfulapi.net/)
*   [Blocking I/O and non-blocking I/O](https://medium.com/coderscorner/tale-of-client-server-and-socket-a6ef54a74763)
*   [A Guide to NIO2 Asynchronous Socket Channel](https://www.baeldung.com/java-nio2-async-socket-channel)
*   [Java NIO Tutorial (Jenkov)](http://tutorials.jenkov.com/java-nio/index.html)
*   [How Single-Page Applications Work](https://blog.pshrmn.com/how-single-page-applications-work/)
*   [How Single-Page Web Applications actually work?](http://progressivecoder.com/how-single-page-web-applications-work/)
*   [What Is a Single Page Application and Why Do People Like Them so Much?](https://www.bloomreach.com/en/blog/2018/07/what-is-a-single-page-application.html)
*   [Guía práctica para la publicación de Datos Abiertos usando APIs](https://datos.gob.es/es/documentacion/guia-practica-para-la-publicacion-de-datos-abiertos-usando-apis)
*   [Local-first software](https://www.inkandswitch.com/local-first.html)