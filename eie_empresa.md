---
layout: page_toc
title: Economia i empresa
permalink: /eie_empresa/
parent: Empresa
nav_order: 1
---
## Sistema econòmic

El sistema econòmic és l'organització d'una societat per a gestionar els recursos de què disposa. En funció de la regulació del mercat, en tenim dos tipus:

-   El **capitalisme**, que es basa en la propietat privada. La finalitat és satisfer les necessitats humanes amb un mercat desregulat que funciona gràcies a la competència i la cerca del benefici econòmic.
-   El **socialisme**, que es basa en la propietat col·lectiva. La finalitat és aconseguir una societat justa i solidària amb el repartiment de la riquesa mitjançant la planificació i mediació al mercat.

|Aspectes|Capitalisme|Socialisme|
|:---|:---|:---|
|Orígen|Segle XIII|Segle XIX|
|Propietat dels medis de producció|Privada|Social|
|Mecanisme d'assignació|Mercat|Estat|
|Principals factors de producció|Capital|Treball|
|Classes socials|Segons el poder econòmic|No hi ha classes|
|Llibertat de decisió|Existeix libertat|Libertat limitada|
|Treball|Dret|Deure|
|Distribució de la riquesa|Sistema meritocràtic|Sistema igualitari|
|Defensa d'interessos|Individual|Col·lectiva|
|Objectiu|Maximització del benefici econòmic|Maximizació del benestar social|
|Marc institucional|Descentralizació|Centralizació|

El sistema més habitual al món és l'**economia mixta**, que barreja els dos: tenim un sector privat i un de públic que regula i corregeix el primer en la senda del progrés social. Una conseqüència d'aquest model és l'**estat del benestar**, on l'estat utilitza part del seu pressupost per a assegurar que tots els ciutadans arriben a un mínim de recursos per a viure dignament.

La regulació del mercat per part de l'estat estableix un marc que pretén protegir l'economia de les seves imperfeccions. Per exemple:

-   Quin ha de ser el nivell dels salaris per poder viure dignament.
-   Com es resol la falta de competència (monopolis).
-   Com es proporcionen els serveis essencials.
-   Establir mecanismes justos per a la redistribució de la riquesa.
-   Evitar l'enginyeria fiscal per a l'evasió i elusió fiscal.

La doctrina política del **neoliberalisme** és un corrent amb molta força que vol defensar el capitalisme reduint al màxim la intervenció de l'estat. És el causant de la reducció de l'estat del benestar i les privatitzacions de béns públics dels darrers anys al nostre país.

## PIB i creixement

El principi del sistema capitalista és que necessitem créixer perquè funcioni l'economia. El concepte s'anomena **creixement econòmic**. La tendència és de creixement, però el comportament és cíclic, i ho fa en els anomenats **cicles econòmics**. Cada cicle té una sèrie de fases: recuperació, expansió, auge, recessió i depressió.

L'indicador més utilitzat per a valorar la situació de l'economia és el **PIB** (producte interior brut): el valor monetari de tots els béns i serveis finals produïts per una regió durant un any. Es diu que si creix l'economia, llavors ho fan els béns i serveis, i tots ens beneficiem.

Els crítics amb la teoria del creixement econòmic basada en el PIB diuen que:

-   Créixer no genera necessàriament cohesió social. 
-   Provoca agressions al medi, moltes irreversibles. 
-   Esgota els recursos, que no estaran disponibles a les properes generacions.
-   Facilita el mode de vida esclau, on serem més feliços quan més treballem, més guanyem i més consumim.

A més, els pilars del sistema econòmic capitalista es basen en principis poc ètics:

-   La primacia de la publicitat, que ens obliga a comprar el que no necessitem.
-   El crèdit, que ens permet obtenir recursos pel que no necessitem.
-   La caducitat dels productes (obsolescència programada).

Els defensors del creixement afirmen que el mateix sistema, amb el mecanisme de la competència, s'encarregarà de resoldre els problemes que genera. Per exemple, amb solucionisme tecnològic. 

També hi ha alternatives de responsabilitat social, que es refereixen a **creixement sostenible** per expressar que és possible créixer però fer-ho en favor el medi i el benestar de la societat. Aquestes plantegen substituir el PIB com a indicador, però encara no hi ha un consens:

-   Índex de desenvolupament humà (IDH).
-   Felicitat interna bruta (FIB).
-   Índex de benestar canadenc (IBC).
-   Renda nacional bruta modificada (RNB*).
-   Índex de progrés real (IPR).

La teoria del **decreixement** és un moviment polític, econòmic i social favorable a la disminució regular controlada de la producció econòmica, amb l'objectiu d'establir una nova relació entre l’ésser humà i la natura. 

Els principis d'actuació d'aquesta teoria segons Serge Latouche són:

-   Reavaluar els valors individualistes i consumistes i substituir-los per ideals de cooperació.
-   Reconceptualitzar l'estil de vida actual.
-   Reestructurar els sistemes de producció i les relacions socials en funció de la nova escala de valors.
-   Relocalitzar: es pretén reduir l'impacte generat pel transport intercontinental de mercaderies i se simplifica la gestió local de la producció.
-   Redistribuir la riquesa.
-   Reduir el consum, simplificar l'estil de vida dels ciutadans. El decreixement aposta per una tornada al petit i al que és simple, a aquelles eines i tècniques adaptades a les necessitats d'ús, fàcils d'entendre, intercanviables i modificables.
-   Reutilitzar i reciclar: allargar el temps de vida dels productes per evitar el malbaratament. Evitar el disseny de productes obsolescents.

## Emprenedoria

### Autoocupació

L'**autoocupació** permet a una persona obtenir ingressos mitjançant una activitat que crea per a un mateix, en lloc de treballar per una altra persona o organització (el treball per compte d'altri). Bàsicament tenim dues opcions:

-   Ser treballador independent. Associat als conceptes de **treballador autònom** o **freelance**.
-   Crear una **empresa capitalista**, és a dir, una organització de dues o més persones que busca un benefici econòmic mitjançant el desenvolupament d'una activitat. Una empresa (societat) pertany als socis capitalistes. Quan hi ha beneficis, es poden reinvertir en l'empresa o bé repartir dividends entre els socis capitalistes.

L'**emprenedoria** és la iniciativa d'un **emprenedor** de portar a la pràctica una **idea de negoci**, és a dir, crear una empresa i dur a terme la producció del bé o prestació de servei. El coneixement, relacions i capacitats de l'equip són essencials a l'hora de definir una bona idea. Es parla d'**intraprenedor** en referència als emprenedors que exerceixen com a tals en empreses de les quals no són titulars. Finalment, l'**empresari** (pot ser o no el mateix l'emprenedor) és qui després dirigeix i gestiona l'empresa. 

Un emprenedor haurà de prendre decisions, assumir riscos, ser creatiu, constant, tenir confiança en si mateix, sentit pràctic, saber organitzar-se i tenir facilitat per a les relacions personals. Pot tenir diferents motivacions, com econòmiques, socials o tecnològiques.

### Visió, missió i valors

Una eina per definir l'estratègia per a un emprenedor és la de la visió, la missió i els valors. Tot parteix de fer explícita la seva **visió**:

-   Què volem ser i on volem arribar?
-   Com ens enfrontarem al canvi?
-   Al mercat només podem accedir amb un **avantatge competitiu**. Quin és i com el podrem mantenir?

Aquesta visió ha de ser clara, concisa, memorable i ha de definir un futur desitjable.

Per altra banda, la **missió** és la raó d'existir de l'organització, i respon a les preguntes: què fem, per a qui, quines necessitats satisfem, i com ens diferenciem.

Finalment, els **valors** ens indiquen quines són les nostres formes particulars per seguir el camí que ens porta a la visió. Proporcionen un criteri en el moment de prendre decisions incertes, que en alguns casos podrien ser una solució fàcil però que portarien a trair els nostres valors.

### Estratègia de negoci

Una eina per a determinar l'avantatge competitiu d'una empresa és la **cadena de valor**, que permet examinar i dividir l'empresa en les seves activitats estratègiques més rellevants i entendre els costos, les fonts i la seva diferenciació. 

Tenim dos tipus d'activitats a la cadena de valor:

-   Les **primàries** o competències distintives (core business), enfocades a l'elaboració del producte o servei i la seva venda: logística interna i externa, operació (producció), màrqueting i vendes, servei post-venda.
-   Les de **suport**: infraestructura, recursos humans, recerca i desenvolupament (R+D) i compres.

En el context de la **globalització**, quan una empresa pot desenvolupar activitats en altres llocs geogràfics, se'n parla de **cadena global de valor**.

Els avantatges competitius d'una activitat econòmica poden classificar-se en tres:

-   Lideratge en costos, amb qualitats similars.
-   Diferenciació d'un producte o servei de certa complexitat segons la combinació de la seva qualitat i les seves característiques.
-   [Segmentació de mercat](https://en.wikipedia.org/wiki/Market_segmentation) segons variables geogràfiques, personals, psicogràfiques o de comportament.

Finalment, cal comunicar-ho mitjançant una estratègia **go-to-market**: que la nostra marca arribi als potencials clients i puguin distinguir els nostres productes o serveis de la resta mitjançant el seu possicionament.

El **model de negoci** d'una empresa descriu el tipus de negoci dins del context del mercat, a qui va dirigit, com es vendrà i com s'aconseguiran els ingressos. Per exemple, alguns tipus de models són la fabricació, la distribució, el retail, l'e-commerce, la publicitat, etc.

Una eina molt pràctica per a definir el model de negoci és el [Canvas](https://en.wikipedia.org/wiki/Business_Model_Canvas).

Un cop tenim clar el nostre model de negoci, podem concretar-lo i descriure'l al nostre **pla d'empresa**. És un document descriptiu amb els passos i els números concrets en que es basa la nostra iniciativa, i que ens pot servir per presentar la nostra idea davant dels possibles inversors.

## Innovació

L'estratègia més adient per a ser competitiu és la **innovació**. Innovar és introduir alguna cosa nova o diferent. Fer-ho en un mercat global i dinàmic comporta risc: podem tenir una gran idea, però si no crea valor, no tindrà impacte.

La innovació ens permet:

-   Diferenciar-nos, l'avantatge competitiu essencial.
-   Millorar la productivitat, i per tant els costos.
-   Tenir una certa cultura d'empresa d'innovació contínua. 

Les innovacions poden estar dirigides cap a:

-   El producte o servei: quines són les característiques.
-   El procés: com es gestiona la producció.
-   El màrqueting o disseny: quin és el mètode de comercialització i com es concreta al preu, la promoció i la distribució.
-   La tecnologia: innovació derivada de la recerca i desenvolupament tecnològic.
-   L'organització: quines persones i com estructuren l'empresa.

Quines serien les característiques d'un equip innovador?

-   Divers: com ho són les habilitats i responsabilitats necessàries.
-   Inquiet: el procés innovador es continu, cal actualitzar-se i estar atent.
-   Amb una bona formació en relació a l'àmbit o sector de l'empresa.

Finalment, l'equip humà ha de tenir un pla. El **pla d'empresa** és un document formal que principalment defineix els objectius del negoci, el mètodes per a aconseguir-los i la seva planificació.

## Recursos per a l'emprenedoria

### Sabadell

-   El [Vapor Llonch](https://www.vaporllonch.cat/) fa formació, programes d'ocupació, orientació professional, borsa de treball i creació i consolidació d'empreses. També té un [observatori de l'economia local](https://www.vaporllonch.cat/observatori-economia-local).
-   L'[oficina d'atenció a l'empresa i l'autònom/a de Sabadell](https://www.sabadellempresa.cat/) es un PAE, que permet crear una societat. També té un centre de promoció empresarial amb oficines i coworking.
-   La [Cambra de Sabadell](https://www.cambrasabadell.org) disposa de diferents serveis amb cost associat. Tenen un [programa d'acceleració de startups innovadores](https://www.cambrasabadell.org/ca/cambraccelera/).

### Catalunya

-   [ACCIÓ](https://www.accio.gencat.cat) és l'agencia per la competitivitat de l'empresa de la Generalitat. Està orientada a millorar la competitivitat de l'empresa, oferint una sèrie de [serveis](http://www.accio.gencat.cat/ca/serveis/) per a l'emprenedoria. Té una secció específica per al [sector TIC](http://www.accio.gencat.cat/ca/sectors/tic/).
-   El [Canal Empresa](https://canalempresa.gencat.cat) de la Generalitat té una sèrie de [serveis](https://canalempresa.gencat.cat/ca/02_serveis_per_temes/) sobre finançament, innovació, sostenibilitat, etc. També informa sobre la [constitució i tràmits](https://canalempresa.gencat.cat/ca/01_que_voleu_fer/02_comencar_un_negoci/crear-empresa-constitucio-tramits/) per a crear una empresa.
-   [Catalunya Emprèn](http://catempren.gencat.cat).
-   [Xarxa Emprèn](http://xarxaempren.gencat.cat).
-   Hi ha un portal d'[economia social](https://treball.gencat.cat/ca/ambits/economia_social/) al departament de treball de la Generalitat. Des d'aquest, es pot gestionar la [creació d'una cooperativa o societat laboral](https://treball.gencat.cat/ca/ambits/economia_social/vols_crear_una_cooperativa_o_una/).

### Estat

El [CIRCE](http://www.ipyme.org/es-ES/CreacionTelematica/Paginas/CIRCE.aspx) és el Centro de Información y Red de Creación de Empresas, un sistema que permet fer els tràmits de creació de societats en línia. Per a crear una empresa de tipus Societat Limitada:

-   Anar a un [Punt d'Atenció a l'Emprenedor (PAE)](https://paeelectronico.es/es-es/CanalPAE/Paginas/QueEsPAE.aspx), on se li assessorarà en tot el relacionat amb la definició del seu projecte empresarial i se li permetrà iniciar els tràmits de constitució de l'empresa.
-   Iniciar els tràmits omplint el DUE a través del portal CIRCE. Per a això és necessari disposar d'un certificat electrònic.

### Finançament

Alguns tipus de finançament que es poden considerar:

-   Ajuts i subvencions
-   Premis, concursos i beques
-   Capitalització de l'atur
-   Microcrèdits
-   Fundraising o mecenatge
-   Inversors privats (business angels)
-   Crowdfunding o micromecenatge
-   Banca ètica i coooperatives de crèdit
-   Incubadores i acceleradores

Alguns enllaços interessants:

-   [ACCIÓ, cercador d'ajuts i serveis per a l'empresa](http://www.accio.gencat.cat/ca/serveis/cercador-ajuts-empresa/).
-   [ACCIÓ, servei d'assessorament financer per a startups](https://www.accio.gencat.cat/ca/serveis/financament/servei-dassessorament-financer/).
-   [Startup Capital, ajut per a startups tecnologiques](https://www.accio.gencat.cat/ca/serveis/innovacio/startups-i-innovacio-disruptiva/startup-capital/).
-   [Business angels per a emprenedors](http://www.bancat.com/emprenedors/).
-   [Finançament i gestió econòmica del Canal Empresa](https://canalempresa.gencat.cat/ca/02_serveis_per_temes/financament/).
-   [Ajuts i subvencions de l'ajuntament de Sabadell](http://www.sabadellempresa.cat/suport-empresarial/ajuts-i-subvencions).

## Referències

*   [Ventaja competitiva](https://economipedia.com/definiciones/ventaja-competitiva.html)
*   [Importance of innovation](https://www.viima.com/blog/importance-of-innovation)
*   [Competitive advantage](https://en.wikipedia.org/wiki/Competitive_advantage)
*   [Cómo funciona la máquina económica](https://youtu.be/npoNbXXS4oQ)
