---
layout: page_toc
title: Màrqueting
permalink: /eie_marketing/
parent: Empresa
nav_order: 6
---

## Màrqueting digital

El màrqueting digital (o en línia) consisteix a contactar possibles clients aprofitant l'internet i tots els seus canals, entre d'altres:

*   Cercadors
    
*   Xarxes socials
    
*   E-mail
    
*   Portals web
    

A més d'utilitzar-los, podem obtenir **feedback** (automatitzat) per a modelar l'**estratègia** que hem dissenyat per a aconseguir l'**objectiu** que hem definit.

### Procés

El procés de l'acció de màrqueting digital podria ser el següent:

**Objectius ⇨ Estratègia ⇨ Mesurament ⇨ Optimització**

1.  Establir **objectius** que volem aconseguir. Caldrà definir els compradors i analitzar la competència.
    
2.  Establir la nostra **estratègia**, definint els **indicadors** que determinaran si els objectius s'han complert**.**
    
3.  **Executar l'estratègia i mesurar** les taxes de **conversió** (accions previstes de la nostra estratègia que s'han complert) utilitzant les eines adients.
    
4.  Optimitzar els resultats, especialment si els resultats no han estat bons, replantejant la nostra estratègia.
    

### Objectius

El màrqueting digital requereix un **pla** amb objectius ben definits. Aquests objectius han de tenir unes característiques, anomenades SMART:

*   e**s**pecífic: els objectius estan clarament definits i exposats, de manera que tot l’equip entén l’objectiu i per què és important
    
*   **m**esurable (analítica web)
    
*   **a**ssolible,
    
*   orientat a **r**esultats
    
*   basat en un **t**emps delimitat
    

Alguns **possibles objectius de màrqueting**: promocionar nous productes o serveis, créixer en presència digital, generar leads, dirigir-se a nous clients, retenir clients existents, construir coneixement de marca, desenvolupar lleialtat de marca, incrementar vendes i/o beneficis, expandir-se a un nou mercat, fer créixer el teu share, convertir-se en un referent d'autoritat al sector.

## Tipus de màrqueting

Podem fer una primera classificació d'estratègies de màrqueting:

*   La **outbound**, o màrqueting tradicional, on l'objectiu és vendre en un sol sentit, sense comunicació des dels usuaris.
    
*   La inbound, basada en **màrqueting de continguts**, on l'objectiu és que l'usuari et trobi a tu en lloc d'anar a buscar-lo, en crear un canal. Això s'aconsegueix amb creació de continguts i el mesurament d'objectius.
    

L'estratègia outbound és la tradicional, però també té una part digital: el pagament als cercadors i anuncis als mitjans digitals. Aquesta estratègia es considera avui en dia superada si s'utilitza de forma aïllada, tot i que pot ser un complement a la inbound.

![](https://lh4.googleusercontent.com/TP7aUQU_I0E3btfajFDWFQ4a5U1W7IToKOVZ3LSgHG3Fk2XmXfM37HE2BaxAEf0nSK1-g3JgD79kfx_HeUMjAvPZlMKUbFVyZTyJNmHUwSvgaHFg=w1280)

## El màrqueting inbound

Ens cal arribar al nou consumidor. L'estratègia consisteix a formar una relació de valor en el temps que **condueixi de forma natural** a la compra, satisfent els seus desitjos i resolent els seus problemes.

El cicle de vida del màrqueting inbound seria:

*   **Atreure** els desconeguts perquè siguin visitants.
    
*   **Convertir** els visitants en oportunitats de venda.
    
*   **Tancar** les oportunitats i fer clients.
    
*   **Delectar** als clients i convertir-los en promotors.
    

Una forma més general de veure el cicle de vida de les compres és el **funnel**.

### Funnel

El **funnel** o **embut de conversió** defineix els diferents passos que ha de donar un usuari o visitant per a convertir-se en client.

Una **conversió** es produeix quan el nostre client potencial **executa una acció clau** que hem definit en la nostra estratègia de màrqueting. Això pot convertir un visitant en un lead, o en general, fer **que el client potencial progressi** dins del funnel.

Les **fases** del funnel són:

*   **TOFU** (top of the funnel): és l'etapa de descobriment de la marca. L'usuari busca contingut educacional que l'ajudi a identificar, definir i comprendre el seu problema i estableix els requeriments de la solució.
    
*   **MOFU** (middle): l'usuari ja coneix el seu problema i entra en una fase de consideració en la qual busca possibles solucions. Detecta les empreses que poden oferir-li un producte / servei d'acord amb les seves necessitats i requeriments.
    
*   **BOFU** (bottom): l'usuari analitza les possibles opcions i selecciona l'empresa que millor solucioni el seu problema. És a dir, pren una decisió.
    

Cada fase té diferents **recursos** per fer progressar els nostres clients potencials.

*   TOFU: **Consciència**. Hem de **detectar aquests problemes o necessitats**, encara que no tinguem la solució. En aquesta etapa els formats més utilitzats són guies, eBooks, blog post o white papers. Tècniques: SEO, anuncis, xarxes, vídeos, linkbuilding, tràfic directe (orgànic). Volem generar **leads**.
    
*   MOFU: **Consideració**. Hem de **proporcionar contingut valuós** que ajudi en aquesta decisió. Vídeos, podcast, comparacions de productes, webinars o guies d'expert són els formats més efectius en aquesta fase. Tècniques: drip màrqueting, missatges dirigits al llarg del temps, en format correu o altres.
    
*   BOFU: **Conversió**. Hem d'**oferir una solució** que porti al client potencial a una futura venda. Els millors formats per a aquesta fase són casos d'estudi, demo de producte o documentació del producte o servei. Tècniques: landing pages.
    

### Buyer persona

Per poder definir la nostra estratègia, s'utilitza el **buyer persona**: una representació semi-ficticia del client ideal, amb dades demogràfiques, patrons de comportament, motivacions, objectius i reptes. Aquesta construcció es basa en **dades**, no en suposicions.

Com podem fer-ho?

*   Recollint dades demogràfiques en enquestes en línia.
    
*   Analitzant el tràfic dels teus portals web, que ens ofereix informació demogràfica, **paraules clau**, etc.
    
*   Utilitzant informes i estudis oficials i d'empreses especialitzades.
    

Aquesta informació ajudarà a establir **patrons** i **tendències** que definiran l'estratègia de màrqueting i vendes.

### Leads

Un cop tenim la nostra **buyer persona**, podem establir estratègies per a conduir als clients potencials cap a la compra. L'estratègia inbound requereix aconseguir leads. Un **lead** és un usuari que ha lliurat les seves dades a una empresa i que, com a conseqüència, passa a ser un registre de la **base de dades** amb el qual l'organització pot interactuar.

Es poden aconseguir leads amb diferents mètodes promocionals: un portal informatiu, **landing pages** (regals), un blog, xerrades, esdeveniments i fins i tot anuncis (màrqueting tradicional outbound).

Un cop tenim leads, els hem d'organitzar en una base de dades que ens permeti fer un seguiment dels clients potencials. Els leads poden acostar-se fins a ser clients. Això es pot veure reflectit al **funnel**.

### Cercadors i paraules clau

Els cercadors han de ser amics de la nostra estratègia. Hem de trobar les **paraules clau** (o **keywords**) per al servei o producte que oferim, ja que aquestes seran les que utilitzin els nostres potencials clients.

L'objectiu és que el **tràfic arribi al teu portal web de forma orgànica**, és a dir, natural, mitjançant el posicionament dins dels motors de cerques (Google, principalment).

[En aquest enllaç](https://www.google.com/search/howsearchworks/), Google explica com funciona el seu cercador. Aquesta pàgina explica que, a l'hora de fer la cerca, es tenen en compte coses com:

*   paraules de la consulta
    
*   La rellevància i usabilitat de les pàgines
    
*   El nivell de coneixements de les fonts
    
*   la teva ubicació i configuració
    

Les paraules clau han d'incloure aspectes com:

*   El nostre producte o servei
    
*   El nostre aventatge competitiu
    
*   El nostre públic objectiu
    

Segons la intenció de l'usuari, els keywords poden ser:

*   Informatius: l'usuari busca informació.
    
*   Transaccionals: l'usuari té intenció de convertir.
    
*   Navegacionals: l´usuari vol anar a cert lloc.
    

Segons el volum de cerques, els keywords poden anar dels més genèrics als més concrets:

*   Head (genèriques), amb molta competència i poca conversió.
    
*   Middle Tail: al mig
    
*   Long Tail: cerques més específiques, amb poca competència i més conversió. Les més interessants per començar.
    

Per fer un estudi senzill, podem començar per Google Autosuggest, Google Trends o Google Ads (Keyword Planner).

### Estratègies i tècniques

*   SEO: es vol millorar l'autoritat i rellevància d'un portal mitjançant bon contingut amb paraules clau, HTML ben estructurat, contingut ben estructurat, bon temps de càrrega, bona experiència UX, bones URLs. També factors externs: qui ens enllaça i amb quina autoritat.
    
*   SEM: anuncis, utilitzant keywords (paraules clau) (Google Ads), Facebook Ads, vídeos, etc.
    
*   Màrqueting de continguts (**orgànic**): creació i distribució de contingut rellevant per a atreure un públic objectiu definit. Pot ser web, vídeo o xarxes socials. Per exemple: infografia, fotografia, posts de cites, vídeos, gràfics de dades, captures de pantalla, instruccions pas a pas, call to action (CTA), preguntes / questionaris, memes, gifs animats, e-books.
    
*   Landing pages: conversió de visitants a leads.
    
*   Remàrqueting: detecció de visitants que tornen (Google Ads).
    
*   Disseny responsive (accés multiplataforma als nostres continguts).
    
*   Email màrqueting.
    
*   Cerques locals: Google My Business, gestió d'opinions.
    
*   Lead scoring: es tracta de valorar numèricament la proximitat de l'usuari al client ideal. Això ajuda al procés de personalització de la comunicació.
    
*   Automatització de màrqueting: permet generar fluxes de treball per gestionar leads (Hotspot). També relacionat amb el lead nurturing: automatització de les interaccions amb l'usuari.
    
*   Màrqueting d'influencer.
    
*   Tests A/B.
    
## SEO

Tenim dos tipus de SEO, o sigui, aspectes que poden **afavorir la visibilitat orgànica** del nostre portal web dins dels cercadors:

*   **Off-page** SEO: coses que no depenen directament del nostre control, especialment, backlinks que milloren el CTR (Click-Through Rate).
    
*   **On-page** SEO: inclou optimització de paraules clau, temps de càrrega, experiència d'usuari, optimització del codi i format de les URL.
    

També tenim una altra classificació, en funció de la valoració ètica de les tècniques que utilitzem:

*   **Black Hat** SEO: tècniques poc ètiques, o que contradiuen les directrius dels cercadors. És una estratègia de curt termini, arriscada i que no aporta valor. I més important: si s'utilitza, **els cercadors poden penalitzar** el portal, i fer-lo perdre rellevància.
    
*   **White Hat** SEO: tècniques ètiques, alineades amb les directrius dels cercadors.
    

### Contingut

**El contingut** és el **més rellevant per aconseguir posicionar-se**, molt més que les paraules clau, les etiquetes, o els backlinks. I molt més que l'aspecte.

La **qualitat** del contingut depèn de diferents factors, pot ser: utilitat, educatiu, entretingut, rellevant, informatiu, original i autoritatiu (respectat). No ha de ser: promocional, publicitari, per a l'empresa, spam, fet només de paraules clau, copiat.

La qualitat determina el teu rànquing, ja que determina **quines paraules clau són rellevants**, i si **atreu** o no **tràfic d'altres portals**, incrementant la teva autoritat al cercador. Alguns consells:

*   El contingut s'ha de dirigir **al teu client**, i ho ha de fer molt llegible i entretingut. Però també als cercadors: millor utilitza **paraules** que imatges, vídeos o animacions.
    
*   **No repeteixis** les paraules clau de forma no natural, els cercadors ho detecten.
    
*   Els **primers paràgrafs** són molt importants. Escriu com si fos un diari: primer el més important.
    
*   No escriguis texts **massa llargs**, però tampoc **massa curts**, es diu que 1000 paraules per pàgina és una bona mida.
    

### Paraules clau

Són **paraules que s'inclouen en una cerca d'un usuari**. Per tant, si volem que els cercadors ens trobin, han d'aparèixer al nostre portal. Hem d'**investigar** quines són les millors per al nostre benefici, i això **es fa amb eines** (habitualment de pagament), mai de forma intuïtiva.

Un cop les tenim, hem de decidir **com les utilitzem**:

*   Al text de ancoratge (text visible de l'enllaç).
    
*   En certes etiquetes HTML: `<title>, <h1>, <h2>, <h3>, <meta name="keywords">` (menys important avui en dia).
    
*   En l'atribut ALT de les imatges `<img>`.
    
*   En el text del portal web, **de format natural**, utilitzant la **densitat correcta**.
    
*   Utilitzant-les a diferents seccions del portal, en funció de quina secció hi som, i amb cua més o menys llarga (head / tail).
    
*   Envolta-les d'etiquetes d'èmfasi: `<em>, <strong>, <b>`, les aranyes ho detecten.
    

### Etiquetes HTML

Són menys importants que el contingut (text), però poden ser significants:

*   Títol `<title>`: es veu a la pestanya navegador, però especialment important és que apareix **a la capçalera d'una entrada de la SERP**.
    
    *   El títol no hauria de ser de més de 64 caràcters, i entre 3 i 10 paraules.
        
    *   Ha d'incloure el nom de la pàgina i paraules clau. Un format possible és "_Títol: algunes paraules clau_".
        
*   Capçaleres `<h1>...<h6>`: les aranyes les llegeixen per entendre com s'organitza jerarquicament el portal.
    
*   A les imatges `<img>`, utilitza l'atribut ALT i TITLE per descriure la imatge.
    
*   Si pot ser, utilitzar sempre etiquetes **amb contingut semàntic** (capçaleres), que no aquelles que només serveixen per organitzar visualment (p. ex. `<div>`).
    
*   `<meta name="description">` inclou el resum de la teva pàgina.
    
*   `<meta name="keywords">` inclou les paraules clau, tot i que els cercadors ja no solen utilitzar-ho.
    

### Disseny i organització

Segueix alguns principis senzills:

*   La **jerarquia** no hauria de tenir **massa nivells**, facilita la feina als visitants i als cercadors.
    
*   La informació s'ha d'estructurar de head a tail, amb la **informació més important a les pàgines més dalt** de la jerarquia.
    
*   És important treballar amb les **paraules clau per pàgines**.
    
*   Les **pàgines importants** s'han de poder arribar des de la navegació de la **pàgina principal**.
    
*   **No duplicar** contingut: millor, tenir **diferents camins** per arribar al mateix contingut.
    
*   Les URL han de ser no massa llargues, i incloure paraules clau.
    
*   Testeja la navegabilitat interna de l'estructura.
    

### Enllaços d'entrada

La **quantitat** i especialment la **qualitat** (millor rànquing) dels **enllaços d'entrada** (backlinks) afecta el teu rànquing. A més, si una pàgina té pocs enllaços de sortida, i està relacionat amb els continguts del teu portal, els seus enllaços compten més.

Alguns consells (White Hat):

*   El més important: **crea contingut que altres portals vulguin enllaçar**.
    
*   Si vols que t'enllacin, **fes córrer la paraula**: emails, xarxes socials, notes de premsa.
    
*   Fes **peticions** personals a portals de qualitat (no automatitzades) **perquè t'enllacin** (webmasters, bloggers, altres contactes).
    
*   Negocia amb altres portals de qualitat l'**intercanvi d'enllaços**, però evita que sigui automatitzat.
    
*   Utilitza **related**:domain per veure on es parla d'un domini.
    

Comprar enllaços és considerat Black Hat.

### Altres tècniques

Altres tècniques a considerar:

*   **Afegir el teu portal** als cercadors perquè sigui indexat. A Google, mitjançant la [Search Console](https://search.google.com/search-console/).
    
*   Crear **sitemaps** per ajudar als cercadors a entendre l'estructura del teu portal. Veure [sitemaps.org](https://www.sitemaps.org).
    
*   Utilitzar **rich snippets** (fragments enriquits) són segments de codi HTML amb contingut que expressa la seva funció dins de la web. Pots mirar-te la pàgina [schema.org](https://schema.org).
    
*   Integrar **xarxes socials:**
    
    *   T'ajuda a **dirigir tràfic** cap a la teva web.
        
    *   Algunes xarxes socials **també s'indexen**. Per exemple, twitter, linkedin, pinterest, facebook.
        
    *   Proporcionen **backlinks**, especialment importants si es tracta d'**influencers**.
        
    *   Si vols rebre atenció de les xarxes, el teu **contingut** ha de ser **fresc**.
        
    *   **Participa** en les xarxes socials en què la teva activitat pugui beneficiar-se.
        
    *   Afegeix **botons de compartir** a les teves pàgines.
        

## Calendari editorial

Un calendari editorial és una **previsió temporal de publicacions** que és la forma de gestionar i controlar les publicacions al llarg de diversos mitjans per promocionar la teva marca.

La primera cosa que cal és decidir la **periodicitat de cada tipus de publicació** que es pot fer. Un cop decidida, el calendari es pot gestionar amb un senzill **full de càlcul**, que tingui per una banda les **dates** del calendari i per una altra els **camps** de cada publicació.

Per cada publicació, aquests són els camps recomanats:

*   Creador: qui escriu. Pots tenir també convidats externs.
    
*   Categoria: mitjà o temàtica de la publicació.
    
*   Estat: pots definir els teus propis. Una proposta seria: ajornat, estudi de paraules clau, començat, preparat, publicat.
    
*   Objectiu: quin és el propòsit de la publicació, dins del teu pla de màrqueting?
    
*   Títol: el títol que tindrà un cop publicat.
    
*   Paraules clau: quines paraules clau vols utilitzar.
    
*   Altres: inclou qualsevol altra informació útil per a tu o el creador.

## Mesurament

El mesurament del resultat de les nostres estratègies quantificables se sol fer amb KPI (Key Performance Indicators):

*   Et permeten **mesurar** el rendiment d'un procés.
    
*   Representen un **valor** relacionat amb un **objectiu** que s'ha fixat anteriorment (recorda: SMART).
    
*   Normalment, s'expressen com un **percentatge de consecució** d'aquest objectiu.
    

A continuació veurem alguns exemples de KPIs en funció de certs objectius.

*   **Augment de vendes**: unitats o ingressos associats, especialment al llarg de campanyes o iniciatives.
    
*   **Millora de beneficis**: marges aconseguits després de treure despeses.
    
*   **Share del mercat**: hauràs de comprovar els ingressos que generes en relació als que genera el teu mercat.
    
*   **Generació de leads**: nombre, increment, cost dels leads i taxa de conversió de les visites a la web.
    
*   **Obtenció de nous clients**: especialment al començament, comptar el nombre, l'increment, el cost per client (quan ens ha costat obtenir-lo) i el percentatge de leads que es converteixen en clients.
    
*   **Lifetime value** d'un client: quan es vol mantenir el negoci, compta els clients que retornen (nombre i percentatge) i quan gasten al llarg de la seva vida.
    
*   **La despesa individual**: quan es gasta en cada compra, per cada perfil (si es té).
    
*   **Les taxes de conversió**: quan hi ha campanyes (landing pages, email links, proves gratuites, etc.), mesura la conversió, o sigui, quin percentatge que gent fa l'acció desitjada quan se li presenta.
    
*   **Mètriques web**: nombre de visites, visitants únics, pàgines per visita, taxa de rebot (abandonament després de visitar una pàgina), temps mitjà a la web.
    
*   **Involucració a les xarxes social**: nombre i increment de seguidors, nombre de comentaris, comparticions, generació de leads, opt-ins, tràfic cap a la web.
    
*   **Rendiment SEO**: rellevància de la web (segons diferents eines SEO), backlinks.
    
## Estratègia outbound

Una estratègia **outbound** pot ser un complement de la teva **inbound**. Especialment al començament del teu negoci, ja que l'estratègia inbound és més de llarg termini.

A més, pot ser una estratègia personal per ajudar a convertir visitants que han arribat via inbound.

Aquestes són algunes claus:

*   **Trucades** i **emails** en fred. És important **personalitzar** la comunicació, especialment a les trucades. Cal fer primer una **recerca**, intentant identificar possibles objectius, potser amb eines de generació de leads.
    
*   **Correu convencional**. Pots obtenir leads de públic offline. Es pot automatitzar amb eines, i fer seguiment online. Es pot combinar amb el món digital, com estratègia complementària.
    
*   **Anuncis** a les xarxes i als cercadors. És complementari de l'estratègia de continguts. És important dirigir bé els anuncis cap a segments concrets. Especialment, si podem fer-ho per a nous lectors fora del canal inbound.
    
*   **Fires**, **conferències** i **networking**. Permeten obtenir nous leads cara a cara. Podem anar a esdeveniments existents o organitzar els nostres.
    

## Eines

Hotspot és una de les eines de referència, però en tenim moltes:

*   Un **sistema de gestió de continguts** (CMS), com ara Wordpress o Squarespace.
    
*   Una eina de màrqueting per **correu electrònic** / generació de leads, com Mailchimp, Convertkit o Aweber.
    
*   Un creador de **land pages**, com ara Leadpages. Això és opcional perquè les pàgines de destinació es poden fer amb Wordpress i Squarespace.
    
*   Una eina d'**automatització de màrqueting**, com ara Autopilot. Això és opcional perquè l’automatització de màrqueting sovint ja està integrada en les eines de màrqueting per correu electrònic o en les eines de generació de leads.
    
*   Un **calendari editorial**: és el document que ens dirà què hem de publicar, quan ho hem de fer, on el publicarem i qui serà la persona que l'ha de fer.
    

## Referències

*   [¿Cómo crear un plan de Marketing Online paso a paso? (vídeo 19 min)](https://youtu.be/-X_2Gb_sA7o?t=352)    
*   [Cómo hacer un plan de márqueting digital paso a paso](https://carlosguerraterol.com/como-hacer-un-plan-de-marketing-digital/)    
*   [Como hacer un calendario editorial](https://rubenmanez.com/como-hacer-un-calendario-editorial-para-tu-blog-incluye-plantilla/)    
*   [40 de fiebre](https://www.40defiebre.com/)    
*   [InboundCycle](https://www.inboundcycle.com/academia)    
*   [Google Trends](https://trends.google.com/)    
*   [How me make money (Google)](https://howwemakemoney.withgoogle.com/)    
*   [¿Qué es el Inbound Marketing? Metodología y caso práctico](https://blog.impulse.pe/que-es-el-inbound-marketing/)    
*   [Inbound Marketing: ¿Qué es y por qué usarlo hoy?](https://blog.impulse.pe/inbound-marketing-que-es-por-que-usarlo-hoy)    
*   [Cómo hacer una auditoría de Marketing Digital](https://www.marketingandweb.es/marketing/como-hacer-una-auditoria-de-marketing-digital/)    
*   [Inbound Marketing: Guía práctica para crear tu estrategia a medida](https://maldon.es/blog/guia-inbound-marketing/)    
*   [Marketing Automation en Hubspot](https://www.inboundcycle.com/blog-de-inbound-marketing/marketing-automation-hubspot)    
*   [Tutorial de búsqueda de keywords](https://nosinmiscookies.com/keyword-research-tutorial/)    
*   [The Content Marketer's Guide to Keyword Research](https://www.singlegrain.com/blog-posts/content-marketing/the-content-marketers-guide-to-keyword-research/)    
*   [The Ultimate Sales Funnel Guide for Small Business](https://stablewp.com/the-ultimate-sales-funnel-guide-for-small-businesses-blueprint/)    
*   [Marketing KPIs to mesure](https://vtldesign.com/digital-marketing/16-marketing-kpis-to-measure/)    
*   [How to Define and Measure Marketing Objectives](https://blog.alexa.com/marketing-objectives/)    
*   [What is Google Ads](https://www.wordstream.com/articles/what-is-google-adwords)    
*   [Guía SEO para principiantes](https://www.lifestylealcuadrado.com/guia-seo-principiantes/)    
*   [¿Quieres comprar un perro?](https://minuevomejoramigo.com/)    
*   [Here are the outbound marketing tactics that still work in 2019](https://databox.com/outbound-marketing-tactics)    
*   [Energía en edificios de oficinas](https://www.enectiva.cz/es/blog/2015/06/ideas-energia-edificio-de-oficinas/)