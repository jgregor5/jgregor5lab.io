---
layout: page_toc
title: Estructura legal i pla econòmic 
permalink: /eie_legaleconomic/
parent: Empresa
nav_order: 7
---

## Estructura Legal

### 1\. Definició Estructura Legal

L'estructura legal d'una empresa es conforma per diferents aspectes com són **la forma jurídica** que aquesta adoptarà (individual, societària o col·lectiva), **els tràmits** que es duran a terme per constituir-la i posar-la en marxa, i finalment **les obligacions fiscals** que tindrà arran de la forma jurídica escollida.

És convenient que pensis quins són els avantatges i desavantatges de les diferents formes jurídiques i quina implicació tindran en el teu model de negoci perquè la presa de decisió sigui la més adient per la teva empresa.

### 2\. Forma jurídica

Abans d’iniciar qualsevol tràmit d’inici d’activitat, cal estudiar atentament la **fórmula més convenient** per crear l’empresa, a fi i efecte de determinar quina estructura s’adapta millor a les característiques pròpies del projecte que es vol desenvolupar.

Les diferents **formes jurídiques** són els tipus d'empresa que **l'administració** preveu que poden constituir-se segons el **nombre de socis**, el **tipus de responsabilitat** i el **capital social aportat**. De fet, la forma jurídica escollida per a l’activitat econòmica de l’empresa en determinarà en gran manera el sistema d’organització.

A l’hora de prendre una decisió és important conèixer el ventall de formes jurídiques que la llei recull, els seus requisits, els avantatges i els inconvenients de cada tipologia. Entre els aspectes que cal valorar abans d’optar per una forma jurídica podem assenyalar:

*   La **complexitat de la constitució i la gestió**. Certes formes requereixen més burocràcia, o certes activitats requereixen certa forma. També els costos poden variar.
*   El **nombre de socis**. Podem tenir formes amb una sola persona o amb diverses.
*   Les **necessitats econòmiques** del projecte. Les formes poden requerir capital mínim.
*   Els **aspectes fiscals**. Bàsicament, tenim dos grups: les formes subjectes a **l'Impost sobre la Renda de les Persones Físiques** (IRPF) i les sotmeses a **l'impost de societats** (IS).
*   La **responsabilitat patrimonial** dels promotors. Variable segons la forma. Pot ser:
    *   Subsidiària: una persona assumeix la responsabilitat.
    *   Solidària: es pot exigir el deute a qualsevol soci.
    *   Mancomunada: cada soci respon segons l'aportació al capital social.
*   **Llibertat d'acció** de l'emprenedor. Quan hi ha formes amb diversos socis, és possible que la decisió sigui en funció del capital aportat, o bé pot ser un vot per persona.
*   **Imatge**. Les formes unipersonals donen una imatge menys sòlida.
*   **Accés a ajuts públics**. Algunes formes tenen ajuts, tot i que no hauria de ser un criteri important.

#### 2.1 Formes jurídiques individuals

Les formes jurídiques individuals realitzen la seva activitat amb el nom de l'empresari.

*   Empresari individual: autònom.
*   Comunitat de béns.
*   Societat civil particular.

Les formes individuals estan gravades amb **l'IRPF**, un impost progressiu.

Les rendes gravades amb l'IRPF són:

*   Rendiments del treball (sou).
*   Rendiments del capital mobiliari (interessos de comptes, dividends).
*   Rendiments per activitats econòmiques (empresaris). Per fer el càlcul de beneficis, tenim tres possibles règims:
    *   Estimació directa normal: per a grans empresaris (facturació > 600K).
    *   Estimació directa simplificada: petits empresaris (facturació < 600K).
    *   Estimació objectiva: per a activitats concretes, rendiments < 150K. Càlcul fix per cada mòdul segons treballadors i mida del local.
*   Guanys i pèrdues patrimonials (transmissió de béns, premis).

#### 2.2 Societats

Tenim quatre tipus principals:

*   Societat de responsabilitat limitada
*   Societat anònima
*   Societat laboral
*   Societat cooperativa

Les rendes estan gravades amb l'**IS**. Altres impostos:

*   Impost sobre activitats econòmiques (IAE). Només aplicable a empreses amb facturació superior a 1M.
*   Impost sobre el valor afegit (IVA) és aplicable a béns i serveis. Pot tenir dos règims:
    *   General: tenim el suportat i el repercutit, i la diferència s'ajusta amb l'agència tributària. Diferents tipus: general, reduit i superreduit (21, 10 i 4%).
    *   Especial: per a empreses detallistes, especialment agricultura, ramaderia i pesca.
*   Impost de béns immobles (IBI)
*   Impost sobre vehicles de tracció mecànica
*   Impost sobre construccions, instal·lacions i obres.
*   Impost sobre transmissions patrimonials i actes jurídics documentats.

### 3\. Constitució

La **constitució** dota de personalitat jurídica a l'empresa, sent susceptible de drets i obligacions. A continuació, es mostren els tràmits necessaris segons la forma jurídica:

*   **Empresari individual:**
    *   DNI del promotor
*   **Comunitat de béns i societat civil:**
    *   DNI dels promotors
    *   Contracte públic o privat de constitució
    *   Sol·licitud del número d'identificació fiscal (NIF)
    *   Impost sobre transmissions patrimonials
*   **Societats mercantils:**
    *   Sol·licitud de certificació negativa del nom o raó social
    *   Sol·licitud de qualificació per societats laborals i cooperatives
    *   Justificació d'aportacions dinerades o no dinerades
    *   Escriptura pública de constitució
    *   Sol·licitud del número d'identificació fiscal (NIF)
    *   Liquidació de l'impost de transmissions patrimonials i actes jurídics documentats
    *   Inscripció en el registre corresponent

### 4\. Posada en marxa

La posada en marxa és posterior a la constitució, i cal per tal de tenir activitat. Són, principalment:

1.  Alta en el cens d'empresaris i professionals. Es fa mitjançant el model 036 o el 037 (simplificat), i pot incloure el règim especial de l'IVA escollit.
2.  Alta en l'impost sobre activitats econòmiques. No implica pagament si no se supera 1M d'euros de facturació, i llavors es gestiona al 036.
3.  Diligencia i legalització dels llibres obligatoris.
    *   Empresaris individuals
        *   Estimació directa normal
            *   Activitat industrial, comercial o de servei: llibre diari, llibre d'inventari i comptes anuals
            *   Activitat no mercantil: llibre de vendes i ingressos, llibre d'ingressos i despeses i llibre registre de béns d'inversió
        *   Estimació directa simplificada: factures numerades per data i agrupades per trimestres, llibre de registre de béns d'inversió, llibre de registre de vendes i ingressos, llibre de registre de compres i despeses
        *   Estimació objectiva: justificants de les operacions.
    *   Societats: llibre diari, llibre d'inventaris i comptes anuals, llibre d'actes, llibre d'accions nominatives (SAs) i llibre de registre de socis (SLs)
    *   Cooperatives: llibre de registre de socis, llibre de registre d'aportacions al capital, llibre d'actes, llibre d'inventari, llibre diari
4.  Inscripció de l'empresa en la Seguretat Social, sempre que calgui contractar personal (TA.6)
5.  Alta de l'empresari en el RETA
6.  Afiliació i alta dels treballadors (TA.1)
7.  Compra de locals: cal comprovar la qualificació urbanística de terrenys o locals, i demanar les llicéncies municipals corresponents. Si hi ha una compra, cal formalitzar-la mitjançant un contracte de compravenda (amb escriptura pública al Registre de la Propietat). També, cal donar-se d'alta a l'IBI.
8.  Arrendament de locals: contractes (verbals o escrits), regulats per la llei d'arrendaments urbans.
9.  Sol·licitud de llicència d'obres d'acord a la normativa urbanística municipal. Objecte: nova planta, reformes d'edificacions o obres menors.
10.  Sol·licitud de llicència d'apertura
11.  Comunicació d'apertura de centre de treball

### 5\. Tramitació

Aquests són els organismes i els tràmits que gestionen:

*   Ajuntaments: llicència d'obres, apertura i altres tributs
*   Delegació o administració d'hisenda: alta IAE, alta cens d'etiquetes i opcions IVA, alta impost sobre béns immobles, alta estimació objectiva o directa, legalització de llibres obligatoris, obtenció del NIF
*   Tresoreria General de la Seguretat Social: inscripció al règim d'autònoms, inscripció de l'empresa, afiliació i alta de treballadors
*   Direcció provincial de Treball i Seguretat Social: comunicació d'apertura del centre de treball, segellar el calendari laboral
*   Oficines de Treball: registre dels contractes formalitzats amb treballadors, comunicació de contractacions que no requereixen contracte
*   Direcció Provincial d'Indústria: inscripció en el registre de la propietat industrial

La finestreta única empresarial (VUE) és una iniciativa que permet la creació d'empreses de forma més fàcil:

*   Informa i orienta a l'emprenedor
*   Facilita la tramitació a tots els organismes des d'un sol lloc físic

El procés telemàtic es pot fer dirigint-se als punts d'atenció a l'emprenedor (PUE). Tot es gestiona mitjançant un únic document, el document únic electrònic (DUE).

### 6\. Altres aspectes

*   Tingueu present les normes de seguretat i higiene en el treball i la prevenció de riscos laborals.
*   S’ha de preveure la possibilitat de protegir el vostre producte, nom o marca d’alguna manera.
*   Si el local no és de la vostra propietat, haureu de formalitzar un contracte de lloguer.
*   Si heu escollit el sistema de franquícia, haureu de signar el contracte que us lligarà amb el franquiciador.

## Pla Econòmic i Financer

### 1\. Introducció

El pla econòmic i financer esdevé la síntesi dels aspectes econòmics del pla d’empresa, les magnituds bàsiques del qual s’obtenen a partir dels diversos apartats del pla. Per tant, per ser coherent és necessari que les dades coincideixin amb les obtingudes en cada un d’aquests apartats. Així, per exemple, de les previsions de vendes i dels preus definits se’n desprendran els ingressos previsibles de l’empresa.

Una **despesa** és una compra d'un bé o servei. Per exemple: matèries primeres, salaris, lloguers, manteniment i neteja, publicitat, consum, assegurances, constitució de l'empresa, etc.

Una **inversió** és el conjunt de béns que l'empresa adquireix, com màquines i eines, per obtenir el producte o servei, i que mai es vendran.

El **pla financer**, sempre que es compleixin les circumstàncies previstes en el pla, evidenciarà la viabilitat o no del projecte de negoci.

En aquest pla fóra també interessant, si es tenen els coneixements adients, realitzar un estudi de la viabilitat de la inversió del projecte a través del càlcul del valor actual net (VAN), de la taxa interna de rendibilitat (TIR) i el criteri del pay-back o termini de devolució. També resulta interessant el càlcul d’algunes ràtios com la de rendibilitat, de solvència, d’endeutament, de liquiditat i d’altres.

### 2\. Pla d’inversions i de necessitats inicials

Representa el càlcul de la inversió inicial, així com de la forma de titularitat dels actius de l’empresa. Inclourà el desemborsament necessari per finançar tant l’immobilitzat material (local, maquinària, mobiliari i utillatge, equips informàtics i d’altres) com l’immaterial (despeses de constitució, de primer establiment, drets de traspàs i d’altres) i les existències necessàries per cobrir l’estoc inicial. També cal incloure un estoc de tresoreria per fer front a pagaments inicials, i això permetrà alleugerir les tensions de tresoreria durant els primers mesos d’activitat.

### 3\. Pla de finançament

El finançament de la inversió inicial i de les altres necessitats podrà fer-se mitjançant finançament extern o bé mitjançant finançament propi. Aquest finançament haurà de tenir en compte el finançament del fons de maniobra necessari per al desenvolupament de l’activitat normal de l’empresa. Es tracta que el total de recursos sigui igual al total de les necessitats estimades. Opcions:

*   Aportacions pròpies i de socis temporals.
*   Lloguer de béns i equips (en lloc de comprar-ne). Renting i leasing.
*   Préstecs.
*   Ajuts públics i subvencions.

Després, podem comptar amb **l'autofinançament** associat a les **reserves**, o bé pel **manteniment**, com són amortitzacions i provisions. El dia a dia també el podem gestionar amb eines com el confirming, els comptes de crèdit, descobert bancari, descomptes comercials, factoring, canvis de termini a proveïdors i descomptes de pagament immediat.

### 4\. Pla de tresoreria

Permet observar la **liquiditat de l’empresa** i, per tant, **preveure les necessitats** de tresoreria que puguin sorgir, mitjançant l’especificació de les entrades i les sortides. Es fa una **anotació mes a mes** de les entrades i les sortides. També cal detallar les hipòtesis considerades per al seu càlcul, considerant la possible estacionalitat de l’activitat. Això permetrà preveure a temps les mesures adients per solucionar els desequilibris de caixa que se’n puguin derivar.

Per exemple, per a les entrades podríem especificar: aportacions dels socis, préstecs, vendes, interessos dels comptes.

Per les sortides: devolució de préstecs, lloguers, compra de maquinària, assegurances, publicitat, compra de mercaderies, salaris, seguretat social, impostos, subministraments.

Per cada mes, mostrem la diferència entre entrades i sortides, per saber **quants diners tindrem al compte bancari**.

### 5\. Compte de resultats

**Mostra els beneficis o pèrdues** esperats per l’empresa com a diferència entre ingressos i despeses. És important especificar tant les diferents partides que determinen el compte de resultats com la seva tendència i evolució, destacant com poden afectar els canvis al resultat global de l’empresa.

És interessant fer totes aquestes previsions considerant diferents escenaris (negatiu, positiu, òptim) per poder estar preparats davant de les diferents necessitats que requerirà cadascun d’aquests casos.

#### 5.1 Amortitzacions

Una amortització és una part d'una despesa més gran, respecte a un element que ens ha de durar un cert temps. Això es fa amb les inversions, per exemple, una maquinària: si ens dura deu anys, l'amortització és la desena part del cost que comptabilitzem anualment.

#### 5.2 Estructura del compte de resultats

A una empresa tenim els comptes d'explotació, que es relacionen amb la seva activitat econòmica habitual, i els resultats financers, relacionats amb l'activitat financera. Trobem els següents apartats:

*   **Ingressos d'explotació**: vendes.
*   **Despeses d'explotació**: lloguers, amortitzacions, assegurances, publicitat, compra de mercaderies, salaris, seguretat social, impostos, subministraments.
*   **Ingressos financers**: interessos bancaris.
*   **Despeses financeres**: interessos del préstec.

Això ens permet calcular el resultat d'explotació (o benefici abans d'interessos o impostos) i el resultat financer. Si els sumem, tenim el **resultat abans d'impostos**. Si li descomptem els impostos, tenim el **resultat de l'exercici** (o benefici net).

Si els resultats són negatius, hem de fixar-nos si els resultats d'explotació són positius o negatius. Si són positius, el problema és que encara tenim resultats financers negatius, que es considera normal especialment si estem començant.

#### 5.3 Regles per l'elaboració

Aquestes serien els principis a seguir per a l'elaboració:

1.  És despesa tot allò que l'empresa compra i consumeix.
2.  Per a compres i vendes, apliquem el criteri de meritació: es considera despesa quan s'ha generat, encara que no s'hagi fet el pagament, i els ingressos quan es meritin, encara que no s'hagin percebut encara.
3.  Tots aquells diners que entren a la nostra empresa i hem de retornar no es consideraran ingressos, i viceversa: els que surten i han de tornar-nos no es consideren despesa.

El Pla General Comptable, a més d'aquests principis, identifica uns altres: empresa en funcionament, uniformitat, no compensació i importància relativa.

### 6\. El balanç

El balanç és la **representació comptable del patrimoni** de l'empresa.

El patrimoni d'una empresa es compon de:

*   El conjunt de **béns**: tot allò que pertany a l'empresa, com maquinària, locals, vehicles, mobiliari, patents, etc.
*   El conjunt de **drets**: tot allò que se li deu a l'empresa, com factures dels seus clients, lletres de canvi per pagar, etc.
*   El conjunt d'**obligacions**: tot allò que l'empresa deu, com els préstecs demanats al banc, lletres de canvi que es deuen a proveedors, etc.

En tot balanç es compleix sempre que **actiu** = **passiu** + **patrimoni net**.

#### 6.1 Actius

Són els béns que posseeix l'empresa. Tenim la següent classificació:

*   Actiu no corrent (de llarg termini)
    *   Immobilitzat intangible: aplicacions informàtiques, drets de traspàs, despeses d'investigació.
    *   Immobilitzat material: construccions, eines, maquinària.
    *   Inversions immobiliàries: terrenys o edificis llogats o en venda.
    *   Immobilitzat financer: accions a altres empreses, bons, fiances.
*   Actiu corrent (curt termini: un any com a molt)
    *   Existències: mercaderies, materies primeres, recanvis.
    *   Crèdits pendents de cobrament: clients, deudors.
    *   Efectiu: caixa, banc.

#### 6.2 Passius

Són tot allò que l'empresa deu. Pot ser:

*   Passiu no corrent: deutes que cal retornar a llarg termini. Crèdits.
*   Passiu corrent: deutes a retornar a curt termini (menys d'un any): pagament a proveïdors.

#### 6.3 Patrimoni net

És la riquesa de l'empresa: capital, reserves, pèrdues i guanys.

### 7\. Balanç final previsional

Al final de l'exercici, hem d'analitzar el balanç.

El passiu ens diu com financem l'empresa, i l'actiu en què invertim o gastem aquests diners. Al començament de l'exercici són iguals, i la seva diferència al final de l'exercici ens diu si hem tingut beneficis o pèrdues.

### 8\. Impostos

#### 8.1. Impostos

Els principals impostos que un emprenedor pot trobar-se són:

\* Impostos **directes**: impost sobre la Renda de les persones Físiques (IRPF), Impost sobre Societats (IS)

\* Impostos **Indirectes** (Impost sobre el Valor Afegit (IVA)

#### 8.2. Impost sobre Societats

L'Impost sobre societats és un impost:

*   Directe. Es calcula en funció de la renda obtinguda per la societat.
*   Personal. S'aplica a totes les rendes obtingudes pel contribuent.
*   Proporcional. S'aplica un tipus de gravamen fix amb independència de la renda.
*   Periòdic.

El període impositiu és anual, coincidint amb l'exercici econòmic de cada societat. Si la societat no fixa un període diferent, per defecte, l'any fiscal va de l'1 de gener al 31 de desembre del mateix any. L'impost sobre societats es merita l'últim dia del període impositiu.

L'esquema de liquidació de l'impost parteix del resultat comptable de la societat, sobre el qual es calcula la base imposable a la qual s'aplica el gravamen corresponent. A partir d'aquí, s'hi apliquen les deduccions a les quals tingui dret, obtenint la quota diferencial.

El tipus de gravamen general és del 30%.

Per a les entitats de dimensió reduïda (amb una xifra de negoci inferior als 10 M€) que compleixen unes condicions determinades, el tipus de gravamen és del 25% per la part de la base imposable entre 0 i 300.000 € i del 30% per l'excés.

Pel que fa a les entitats amb una xifra de negoci inferior als 5 M€ i una plantilla de menys de 25 treballadors que tributen al tipus general amb condicions, el tipus de gravamen és del 20% si base imposable és inferior a 300.000 € i del 25% per la resta.

#### 8.3. IRPF

L'impost sobre la renda de les persones físiques és un tribut de caràcter personal i directe que grava, segons els principis d'igualtat, generalitat i progressivitat, la renda de les persones físiques d'acord amb la seva naturalesa i les seves circumstàncies personals i familiars.

#### 8.4. IVA

L’**impost sobre el valor afegit** (IVA) és un impost indirecte que grava el consum i que recau sobre el lliurament de béns i prestacions de serveis que fan empresaris o professionals, com també sobre les importacions i adquisicions intracomunitàries de béns.

## Referències

*   [Elección de la forma jurídica](http://www.ipyme.org/es-ES/DecisionEmprender/FormasJuridicas/Paginas/FormasJuridicas.aspx)
*   [Barcelona Activa Emprenedoria](https://emprenedoria.barcelonactiva.cat/emprenedoria/cat/emprenedoria/)
*   [Barcelona Activa: Formes jurídiques i tràmits](https://emprenedoria.barcelonactiva.cat/emprenedoria/cat/categories/Formes_juridiques_i_tramits.jsp)
*   [Barcelona Activa: Activitats](https://emprenedoria.barcelonactiva.cat/emprenedoria/cat/emprenedoria/activitats/index.jsp)
*   [Canal Empresa: Constitució i tràmits](https://canalempresa.gencat.cat/ca/01_que_voleu_fer/02_comencar_un_negoci/crear-empresa-constitucio-tramits/)
*   [PAE Virtual](https://paeelectronico.es/es-es/CreaEmpresa/Paginas/CreacionEmpresas.aspx)
*   [Estimación objetiva (módulos) 2020](https://www.boe.es/buscar/doc.php?id=BOE-A-2019-17252)
*   [Simulador de préstec](https://app.bde.es/asb_www/es/cuota.html#/principalCuota)