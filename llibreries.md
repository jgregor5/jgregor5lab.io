---
layout: page
title: Llibreries POO
permalink: /llibreries/
has_children: true
nav_order: 2
---
Resultats d'aprenentatge:

1.  Escriu programes que manipulin informació seleccionant i utilitzant els tipus avançats de dades facilitats pel llenguatge.
1.  Gestiona els errors que poden aparèixer en els programes, utilitzant el control d’excepcions facilitat pel llenguatge.
1.  Desenvolupa interfícies gràfiques d’usuari simples, utilitzant les llibreries de classes adequades.
1.  Realitza operacions bàsiques d’entrada/sortida d’informació, sobre consola i fitxers, utilitzant les llibreries de classes adequades.