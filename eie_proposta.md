---
layout: page_toc
title: Proposta de valor
permalink: /eie_proposta/
parent: Empresa
nav_order: 4
---

## Punt de partida

**Autoocupació**: activitat professional o empresarial generada per una persona, i que l'exerceix de forma directa pel seu compte i risc.

Qualitats de les persones emprenedores:

*   Creativitat
*   Iniciativa
*   Responsabilitat
*   Autonomia
*   Assumpció de riscos
*   Competències socials
*   Competències personals

**Intraemprenedoria**: són treballadors que des del seu lloc de treball a una empresa on no són propietaris desenvolupen i posen en pràctica les seves qualitats emprenedores en benefici de l'empresa per a la que treballen.

## La idea de negoci

La **idea** és el **producte o servei que es pretén oferir al mercat**.

Cal avaluar-la i veure la seva viabilitat:

*   és útil
*   es diferencia de la competència, creant valor afegit
*   genera innovació: és nou, millora un producte/servei existent o el seu procés de fabricació
*   és rendible

### Creativitat i innovació

**Origen de la idea innovadora:**

*   necessitat no satisfeta
*   factor diferenciador
*   innovació en tecnologia
*   aprofitar la pròpia formació o experiència
*   repetir experiències alienes
*   cercar referències en internet

**Podem validar la idea amb preguntes clau:**

*   quin valor aporta la nostra idea o producte?
*   què necessiten els meus clients potencials?
*   com és el sector on vull emprendre?
*   quin segment de clientes és rellevant per a nosaltres?
*   com puc testar la validesa de la meva idea?
*   quin tipus de comunicació volem establir amb el nostre entorn?
*   què esperem aportar a la societat?
*   quins canals de distribució i promoció són rellevants?

### Innovació i desenvolupament econòmic

Evolució del factor generador de riquesa i desenvolupament a la història:

*   la terra dedicada a l'agricultura
*   amb la revolució industrial, els recursos energètics
*   actualment, el coneixement i la innovació

Factors importants:

*   I+D+I: investigació, desenvolupament i innovació.
*   el desenvolupament integrat: econòmic, social i sostenible.

### Com podem innovar

Podem innovar:

*   en el producte (total o evolució)
*   en el procés (despeses de producció i distribució, millora de la qualitat)
*   en màrqueting (disseny, envasat, posicionament, promoció)
*   en l'organització de l'empresa (canvis en les pràctiques i procediments o al lloc de treball)

### Valoració de la idea de negoci

Un possible esquema per validar seria:

*   fer un **estudi inicial**: possibilitats que ofereix el mercat, forats que hi ha, cercar fonts, imaginació
*   fase de **consulta**: cercar recolzament d'experts, de possibles proveïdors i clients, normativa vigent
*   anàlisi de l'**acollida**: en funció de com de positiva hagi estat, veure si cal reconsiderar la idea o descartar-la
*   presa de **decisions**: decidir què farem del projecte
*   **posada en pràctica**: implementar-la

## Generació d'idees

**Brainstorming**: la Tempesta d'Idees.

**Scamper**: sobre un element que es desitja millorar, buscar idees tenint en compte les preguntes derivades:

*   Substituir, Combinar, Adaptar, Modificar, Buscar altres usos, Eliminar, Canviar la forma.

**Sinèctica**: Generació d'idees per analogia.

**Pensament lateral**: No sempre hem de pensar de forma lògica. Podem fer un enfocament indirecte i creatiu.

**Els sis barrets** (Six thinkings hats): Analitzar el problema fent ús de sis tipus de pensament diferents:

*   objectiu (fets, números, verificació)
*   intuïtiu (emocions, sentiments estètics)
*   creatiu (alternatives, atzar, extrems)
*   negatiu (riscos, perills, imperfeccions)
*   positiu (optimisme, futur, somnis)
*   control (síntesi, organització, conclusions)

## Proposta de valor

La **proposta de valor** és el conjunt de productes i serveis que creen valor per a un segments de mercat específics. L’**objectiu** és **solucionar** els problemes dels clients i **satisfer** les seves necessitats mitjançant **propostes de valor**. Quin problema ajudem a solucionar? Quin valor oferim als nostres clients? Cal plantejar-ho des de la perspectiva de “què vol comprar el nostre client” versus “què venem”.

Quan es plantegem el model de negoci, identifiquem tres preguntes:

*   **Com**? Activitats relacionades amb la producció.
*   **Què**? La nostra oferta. Aquesta és la que respon la proposta de valor.
*   **Qui**? Activitats relacionades amb la venda.

Per a arribar al **Què**, podem fer-nos les següents preguntes:

1.  **Què** és el que desitjo oferir als clients?
2.  Quines necessitats dels potencials clients cobriré amb aquest producte o servei?
3.  Què li proporciono al client que no s’estigui oferint per una altra empresa del mercat?

Donant resposta amb aquestes qüestions podrem establir la nostra proposta de valor. D’aquesta forma podrem establir els criteris de model de negoci, següents:

*   Seleccionar els clients potencials als que dirigirem l’oferta
*   Crear utilitat per als potencials clients.
*   Diferenciar-nos de la competència.
*   Aconseguir i conservar als clients
*   Com se seleccionaran els clients

## Canvas del model de negoci

Per saber com funciona el **model de negoci canvas** has de saber que és un llenç format per una sèrie d'**elements que connecten** les diferents parts de l'estructura d'un pla de negoci. És una eina útil i un format cada vegada més sol·licitat. El model canvas per emprendre està compost per 9 fases descrites a continuació:

*   **Segments de clients**: Respon a la pregunta a qui es dirigeix ​​el nostre producte o servei. Descriu el públic objectiu i les seves característiques.
*   **Proposta de valor**: En aquest apartat es tracta d'enfocar els beneficis del teu servei o producte, quina diferència teu pla de negoci al d'altres, quin és el teu punt diferenciador davant la competència.
*   **Canals de distribució**: Vies a través de les quals anem a comunicar la nostra proposta de valor. Els canals que proposa el model de negoci de canvas són: canals propis o externs, directes o indirectes. Aquest segment inclou la descripció de l'efectivitat que generen aquests canals: la notorietat, avaluació, comunicació, distribució i venda.
*   **Fonts d'ingressos**: Com generem els beneficis perquè funcioni el pla de negoci. Aquí s'ha de diferenciar d'ingressos i guanys per no obtenir errors de pressupost.
*   **Recursos clau**: enumera els actius més importants perquè el pla de negoci funcioni. Són els recursos físics, financers, humans o immaterials com les patents o coneixements.
*   **Relació amb clients**: La relació podrà ser personal o automatitzada. Es tracta de tenir en compte en el model de negoci la fidelització i captació de clients i l'estimulació de les vendes.
*   **Activitats clau**: processos claus per al funcionament de l'activitat que es va a exercir. Segons el model canvas les activitats clau d'una negoci són tres: producció, solució de problemes i plataforma.
*   **Socis clau**: aquesta part del pla de negocis amb el model canvas remarca els partners i proveïdors necessaris perquè la idea de negoci funcioni.
*   **Estructura de costos**: segons el model canvas són les despeses en què s'incorre durant el procés de generar valor, és a dir, els costos que genera el negoci. El model de negoci canvas els divideix en: costos fixos i variables, economies d'escala i economies de camp.

### El canvas social

*   **Segments de clients**: hauríem de plantejar-nos com generem valor social en el segment de clients i com millorem l'entorn social, per exemple: Que el nostre projecte estigui adreçat a algun col·lectiu en risc d'exclusió social.
*   **Proposta de valor**: hem de plantejar-nos com la nostra proposta fa una producte al servei de les persones, or exemple, podem fer que el nostre producte o servei sigui accessible a persones amb diversitat funcional).
*   **Canals de distribució**: estudiar com el canal de distribució pot ser positiu per a la societat, per exemple si faig lliurament a domicili, la faig amb bicicleta per no contaminar.
*   **Fonts d'ingressos** **(situació econòmica a l'inici)**: és important tenir present que el consum responsable és una màxima per millorar el món en què vivim, ia més pot ser un benefici per al nostre començament, això no vol dir baixar la qualitat, ni el valor que volem aportar, només es tracta d'ajustar bé els números. Per exemple, si volem que el nostre producte sigui ecològic, segurament haurem d'invertir una mica més, però no obstant això el cost social que aportem a la societat també té un gran valor que no és econòmic.
*   **Recursos clau**: serà clau si ajustem molt bé les necessitats, tant des d'un punt de vista d'eficiència, com des d'un punt de vista de consum responsable. També ens ajudarà pensar que si estigueu-vos recursos els trobem al mercat local i de proximitat estem potenciant l'economia dels meus veïns, als que conec i als que tinc confiança. O per exemple en l'aspecte dels recursos humans, el nostre empleats poden formar part d'algun col·lectiu d'inserció, o simplement podem tenir una organització interna democràtica com és el cas de moltes cooperatives.
*   **Relació amb clients**: podem fer que la nostra estratègia de comunicació tingui un impacte social positiu, per exemple si anem a fer la comunicació en paper, podem usar paper reciclat.
*   **Activitats clau**: podem fer que la nostra proposta de valor i per tant la nostra activitat principal tingui un enfocament ecològic o de compromís amb l'entorn local, per exemple que els productes que venc siguin biològics, o en el sector serveis que el meu servei pugui arribar també a un sector de la població que no té recursos econòmics.
*   **Socis clau (col·laboradors)**: es planteja que la relació amb l'entorn proper és essencial, potencia l'economia local ens afavorirà i crearà sinergies diferents de consum al nostre voltant. Quan coneixem els canals de distribució, quan els nostres clients coneixen de prop als nostres proveïdors i als nostres col·laboradors, la confiança és un element clau per al consum responsable.
*   **Estructura de costos** **(situació econòmica durant el projecte)**: és important plantejar-se l'enfocament no lucratiu, que ens ve a dir que el projecte cobreix els sous dignes i coherents de les persones treballadores, i que els beneficis es reinverteixen en benefici del projecte o es destinen a alguna obra social o ambiental. També podem plantejar-nos en aquest punt, on guardo els diners mentre no el faig servir i assegurar-me que està en un banca ètica, i no és invertit en caps amb els quals no estic d'acord.

[Descarregar](http://ecosfron.org/wp-content/uploads/CANVAS-SOCIAL-3.pptx)

## Pla estratègic

### Introducció

Molts cops la mateixa dinàmica de la companyia i l’entorn orienta a l’empresa cap a unes **estratègies** determinades sense necessitats de fer **cap pla**. Aquestes són les estratègies emergents que, en general, serveix per anar seguint el ritme del sector. En canvi, les estratègies deliberades són les que obtenim del nostre pla estratègic, i són aquestes les que ens porten a canvis importants amb estratègies ofensives.

Algunes consideracions a tenir en compte al definir l’estratègia, són:

*   Estan **en línia de la identitat de l’organització**
*   S’enfoquen a **mitjà** o **llarg termini**, a partir de 3 anys vista
*   Impliquen la **posada en marxa** una quantitat significativa de **recursos**

Una bona forma de començar la introducció és definir la situació de l’empresa en el moment d’elaborar el document.

Cal també concretar quin termini cobreix el Pla estratègic. Aquest ha de ser a llarg termini sabent que:

*   Pressupostos – s’elaboren a 1 any vista
*   Planificació – és la definició de l’estratègia
*   Estratègia – és la gestió a llarg termini

Es considera acceptable un Pla Estratègic a més de 3 anys, ja que no pot coincidir amb els pressupostos.

La diferència d’on som i on volem arribar és el **Gap estratègic**. Per tant cal gestionar el gap per poder evolucionar d’on estem avui. Cal aspirar a un demà ambiciós perquè normalment arribarem un punt més avall.

### Visió, missió i valor

La **missió**, la **visió** i els **valors** han d’estar clarament redactats al Pla estratègic, ja que ha de donar sentit al treball diari que realitzi l’empresa. Han de definir un **marc** prou **ampli** com per poder ser **vàlids al llarg de la vida de la empresa**, tot i que poden es poden revisar però no ser subjectes de grans i continuades modificacions. En aquest cas significaria que no estan ben redactats.

#### Visió

Associada al somni. **Què volem ser, on volem arribar**.

Respon a les preguntes:

*   Què volem aconseguir com a organització?
*   Com s’enfrontarà l’empresa al canvi?
*   Com es diferenciarà de la resta?
*   Com s’aconseguirà ser competitiva?

És possible que en els seus inicis l’empresa passi per dificultats però tingui una visió molt ambiciosa a la que arribar al llarg de la seva trajectòria. La visió dóna a l’empresa una **fita** per assolir que aporta sentit als esforços que desenvolupen les persones que la integren.

Algunes recomanacions a l’hora de redactar-ho:

*   Ha de incloure **dos components**: una **meta ambiciosa** a complir en 10 – 30 anys però també una **descripció palpable** del futur.
*   Hem de pensar que ha de ser un **punt d’orientació**. Ha d’apel·lar tant a la **intel·ligència** com a les **emocions** dels treballadors.

**Validació:**

*   Defineix un futur desitjable?
*   Motiva?
*   Es clara?
*   Es concisa?
*   Es memorable?

_Walmark: ser líder mundial del retail._

#### Missió

És **la raó d’existir de l'organització**. Habitualment és útil definir quines línies no volem adoptar per poder obrir ventall al què sí.

Respon a les preguntes:

*   Què fem?
*   Per a qui ho fem?
*   Quines necessitats satisfem?
*   Què valoren els nostres clients?
*   Com ens diferenciem de la resta?

_Walmark: ajudar a estalviar perquè vivim millor (lideratge en costos: economia d’escala, distribució tecnificada, integració tecnològica amb els socis, coneixement dels clients, cultura)._

#### Valors

Els valors ens indiquen quines són les **nostres formes particulars** per seguir el camí que ens porta a la visió.

Proporcionen un criteri en el moment de prendre decisions incertes, que en alguns casos podrien ser una solució fàcil però que portarien a trair els nostres valors.

## Referències

*   [La cadena de valor de Michael Porter (vídeo 6 min)](https://www.youtube.com/watch?v=fsYvfsy3ek8)
*   [Business Model Canvas Paso a Paso + 2 Ejemplos (vídeo 15 min)](https://www.youtube.com/watch?v=-ppJly4W8LI&t=7s)
*   [El canvas social](https://ecosfron.org/el-canvas-social/)
*   [Qué es una Startup y cómo funciona éste nuevo modelo de negocio](https://www.universia.net/es/actualidad/orientacion-academica/que-startup-como-funciona-este-nuevo-modelo-negocio-1121188.html)
*   [Lista Emprendedores: las 50 startups con más futuro](https://www.emprendedores.es/startups/mejores-startups-casos-ejemplos-empresas-exito-emprendedores/)
*   [17 ejemplos inspiradores de misión, visión y valores de empresas](https://blog.hubspot.es/marketing/mision-vision-valores-ejemplos)

Vídeos:

*   [Salvados. La Fageda, cuando negocio y ética van de la mano](https://youtu.be/cpJ-lmlImKs)
*   [Business Model Canvas Paso a Paso + 2 Ejemplos](https://youtu.be/-ppJly4W8LI)
*   [Elevator pitch. Tienes 20 segundos - eduCaixa](https://youtu.be/2b3xG_YjgvI)
*   [Aprende a hacer la visión, misión y valores en menos de 5 minutos](https://youtu.be/4I9_I5dRgFg)
*   [Triodos Bank en Buenafuente (La Sexta). Entrevista a Joan Antoni Melé](https://youtu.be/4S4uEkpFvsQ)
*   [El comercio justo en 6 pasos](https://youtu.be/U2JlIrrspnA)
