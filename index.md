---
layout: page
title: Inici
permalink: /
nav_order: 1
---

## Desenvolupament d'Aplicacions Multiplataforma

### M3: Programació

*   UF5: [Llibreries POO](/llibreries)
*   UF6: [Persistència POO](/persistencia)

### M7: Desenvolupament d'interfícies

*   UF1: [Disseny i implementació](/ifaces_disseny) i [Usabilitat i informes](/ifaces_usable)
*   UF2: [Preparació i distribució](/ifaces_distrib)

### M9: Serveis i processos

*   UF2: [Processos i fils](/processos_fils)
*   UF3: [Sòcols i serveis](/socols_serveis)
*   UF1: [Criptografia](/criptografia) i [Seguretat](/seguretat)


[Apunts antics](https://sites.google.com/xtec.cat/jgregor5)
