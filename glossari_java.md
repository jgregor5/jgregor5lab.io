---
layout: page_toc
title: Glossari Java
permalink: /glossari_java/
parent: Diversos
nav_order: 5
---
### Array

Una col·lecció de dades del mateix tipus amb una posició designada per un sencer. Un array és un objecte del tipus Object.

### Classe

Una plantilla que defineix la implementació d’un tipus d’objectes. Té membres: dades (variables d’instància) i funcionalitat (mètodes d’instància).

### Classe abstracta

És un tipus de classe on certs detalls de la seva implementació es posposen per ser completats més tard. Poden ser classes que utilitzen el mot "abstract" o bé interfícies (mot "interface").

### Classe embolcall (wrapper class)

Són les classes que Java defineix per embolicar els tipus primitius. Són immutables.

### Constructor

Un pseudo-mètode d’instància que crea un objecte. Tenen el nom de la classe, i s’invoquen amb “new”.

### Fil (thread)

La unitat bàsica d'execució del programa. Un procés pot tenir diversos fils que funcionen simultàniament, cadascun realitzant un treball diferent, com ara esperar esdeveniments o realitzar un treball que requereix temps que el programa no necessita completar abans de continuar.

### Interfície fluïda (fluent interface)

És una forma de dissenyar API orientades a objectes basada en l'encadenament de mètodes. L'objectiu és fer que la llegibilitat del codi font sigui més propera a la d'un text escrit.

### Mètode

Una funció definida a una classe. Hi ha mètodes d’instància i de classe.

### Mètode d'instància

Un mètode que s’invoca respecte a la instància d’una classe.

### Mètode de classe

Un mètode que s’invoca sense referir-se a cap objecte particular. També es diu “mètode estàtic”.

### Multifil

Descriu un programa que s'ha designat per tenir parts del codi que s'executen concurrentment.

### Objecte

És una instància d’una classe, una concreció singular. També es diu “instància” segons el context.

### Objecte immutable

Un objecte que NO pot ser modificat després de ser creat.

### Objecte mutable

Un objecte que pot ser modificat després de ser creat. Habitualment tenen setters/getters.

### Procés

Un espai d'adreces virtual que conté un o més fils.

### Referència

Un tipus de variable on el seu valor és l'adreça d’un objecte.

### Secció crítica

Un segment de codi en què un fil utilitza recursos (com certes variables d'instància) que altres fils poden utilitzar, però no ho poden fer alhora.

### Sobrecàrrega (mètodes: Overloading)

Definició de dos mètodes amb el mateix nom i classe però diferents implementacions.

### Sobreescriptura (mètodes: Overriding)

Quan una subclasse ofereix una implementació específica que ja s'ha oferit a una superclasse.

### Subclasse

Classe que hereta. També classe fill.

### Superclasse

Classe heretada. També classe pare.

### Synchronized

Una paraula clau en el llenguatge de programació Java que, quan s'aplica a un mètode o bloc de codi, garanteix que, com a molt, un fil alhora executi aquest codi.

### Tipus genèric

Un tipus genèric és una classe o interfície amb una secció de tipus de paràmetres envoltada per l'operador diamant <>.

### Tipus primitiu

Un tipus de variable on el seu valor té una mida i format en concordança amb el seu tipus.

### Tipus cru (raw)

Un tipus cru és una classe o interfície genèrica utilitzada sense paràmetres de tipus. No es recomana el seu ús.

### Variable

Una dada amb un identificador. Té un tipus i un àmbit. Hi ha variables de classe, d’instància i locals. Poden ser de tipus primitiu o referència.

### Variable d'instància

Una variable associada amb un objecte particular. També es diu “camp”.

### Variable de classe

Una variable associada amb una classe, però amb cap instància particular. També es diu “camp estàtic”.

### Variable local

Una variable coneguda dins d’un bloc, com per exemple dins d’un mètode.