---
layout: page_toc
title: Workflow git
permalink: /workflow_git/
parent: Diversos
nav_order: 11
---

Aquesta entrada descriu un flux de treball simplificat per treballar en equips de desenvolupament petits, des de la línia de comanda i sense crear branques.

## Conceptes

### En local

git és una eina que permet treballar amb repositoris de codi locals i remots.

Els canvis sobre els arxius d’un repositori s’agrupen en _**commits**_. Un _commit_ és l’acte d’emmagatzemar un conjunt de canvis al repositori.

En l’àmbit local, tenim tres espais:

*   El _**working directory**_ és el lloc on tens el teu codi. A l’arrel del teu _working directory_ tindràs sempre una carpeta anomenada .git on es guarden els altres dos espais.
*   El _**staging area**_ és una capsa on pots ficar i treure arxius. Un _commit_ estarà format per tots els arxius ficats en aquesta àrea, i s’identifica amb un _hash_ o resum. Quan es fa el commit, es buida.
*   El _**repositori**_ és el lloc on s’emmagatzemen els commits d’arxius provenents del _stagging area_. Podem revisar i recuperar qualsevol arxiu de qualsevol commit del passat. El _commit_ actual d’un repositori es diu _**HEAD**_.

Un repositori pot tenir branques (branches). Les branques permeten divergir de la línia principal de desenvolupament i fer feina sense afectar-la. En aquest document no les utilitzarem, per tal de tenir un flux de treball més senzill. Però cal saber que _**master**_ és el nom de la branca que git crea per defecte quan es crea un repositori.

{% plantuml %}
participant WorkingDirectory
participant StagingArea
participant Repository

WorkingDirectory -> StagingArea: git add

StagingArea -> Repository: git commit

Repository -> WorkingDirectory: git checkout
{% endplantuml %}

### En remot

Opcionalment, podem tenir repositoris remots, i comunicar-nos per pujar o baixar coses. Un repositori remot és com un de local, però no té working directory. Se’n diu “bare”.

Ens interessa tenir-ne de remots per poder tenir un lloc on compartim el codi amb la resta de membres del grup. El flux de treball serà treballar en local i compartir en remot la feina, un cop la tenim enllestida.

A un repositori local podem emparellar un de remot. El nom que git dona al principal repositori remot és _**origin**_. Un cop els hem emparellat, el codi NO se sincronitza automàticament. Tenim disponibles una sèrie d’operacions:

*   _**fetch**_: guarda en local els canvis remots (sense integrar-los).
*   _**merge**_: barreja els canvis remots que tenim en local amb els locals.
*   _**pull**_: és el mateix que fer un _fetch_ i després un _merge_.
*   _**push**_: puja tots els canvis locals al repositori remot.

{% plantuml %}
participant WorkingDirectory
participant StagingArea
participant LocalRepository
participant RemoteRepository

RemoteRepository -> LocalRepository: git fetch

LocalRepository -> WorkingDirectory: git merge

LocalRepository -> RemoteRepository: git push
{% endplantuml %}

## Escenari base

Reproduirem l’escenari base, amb dos usuaris i un repositori remot compartit. Els dos usuaris fan canvis en local i els sincronitzen amb el repositori remot.

{% plantuml %}
box "developer 1"
participant WorkingDirectory1
participant StagingArea1
participant LocalRepository1
endbox
participant RemoteRepository
box "developer 2"
participant LocalRepository2
participant StagingArea2
participant WorkingDirectory2
endbox

WorkingDirectory1 -> StagingArea1: **git add** arxiu.txt
note left: creo\narxiu.txt
StagingArea1 -> LocalRepository1: **git commit**
LocalRepository1 -> RemoteRepository: **git push**

RemoteRepository -> LocalRepository2: **git clone** (pas 1/2)
LocalRepository2 -> WorkingDirectory2: arxiu.txt (pas 2/2)
note right: rebo\narxiu.txt

WorkingDirectory2 -> StagingArea2: **git add** arxiu.txt
note right: modifico\narxiu.txt
StagingArea2 -> LocalRepository2: **git commit**
LocalRepository2 -> RemoteRepository: **git push**

RemoteRepository -> LocalRepository1: **git fetch**
LocalRepository1 -> WorkingDirectory1: **git merge** 
note left: rebo\narxiu.txt\nmodificat
{% endplantuml %}

### Eina git

[Instal·la la teva eina git](https://git-scm.com/download/) de línia de comanda al teu sistema operatiu.

Intenta executar-la: “git –version”:

```shell
$ git --version
```

### Crear el repositori

Primer, has de crear un repositori buit a github o a gitlab.

Quan l’hagis creat, pots obtenir un URL del tipus:

`https://github.com/usuari/repositori.git` o bé `https://gitlab.com/usuari/repositori.git`.

### Configuració

Les següents tres comandes són interessants per treballar: les dues primeres, calen per indicar el teu usuari i correu que es guarda a l’activitat del repositori. El tercer serveix per guardar les credencials el primer cop que s’introdueixen. Compte: es guarden en text pla a `$HOME/.git-credentials`.

```shell
$ git config --global user.email "elteu@correu.com"
$ git config --global user.name "elteunom"
$ git config --global credential.helper store
```
El flag --global indica que els canvis apliquen a tots els repositoris. Si no s'indica, només aplica al repositori en què ens trobem.

També es pot configurar credential.helper per utilitzar una cache (900 segons per defecte):
```shell
$ git config --global credential.helper cache
```

Si es volen ignorar els canvis fets al mode dels arxius, es pot fer:
```shell
$ git config --global core.filemode false
```

La comanda per esborrar una entrada és:
```shell
$ git config --global --unset <key>
```

### Clonar el repositori

A partir d’ara es parla de github, però les comandes són exactament les mateixes canviant l’URL pel de gitlab.

Clonarem el repositori buit que hem creat a github:

```shell
1$ git clone https://github.com/usuari/repositori.git
```

Això crea una carpeta “repositori” amb el working directori i la carpeta .git a dins.

Per mostrar l’estat:

```shell
1$ git status
On branch master
No commits yet
nothing to commit (create/copy files and use "git add" to track)
```

També pots mirar l’aparellament amb el repositori remot:

```shell
1$ git remote -v
origin  https://github.com/usuari/repositori.git (fetch)
origin https://github.com/usuari/repositori.git (push)
```    

Pots veure les branques locals i remotes així:

```shell
1$ git branch
* master
1$ git branch -r
  origin/HEAD -> origin/master
  origin/master
```

### Afegir contingut

Per afegir contingut, cal preparar el commit. Primer, crea o copia al working directory tot el contingut que vulguis.

Imaginem que afegim un arxiu així:

```shell
$ echo "Hola, món!" > arxiu.txt
```

Si mostres l’estat:

```shell
1$ git status
No commits yet
Untracked files:
  (use "git add <file>..." to include in what will be committed)
  arxiu.txt
nothing added to commit but untracked files present (use "git add" to track)
```

Els missatges expliquen que tenim un arxiu fora del control del repositori (untracked). Per afegir-lo:

```shell
1$ git add arxiu.txt
1$ git status
On branch master
No commits yet
Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
  new file:   arxiu.txt
```

Ara explica que tenim un nou arxiu (new file) al staging area. Per afegir-lo al commit:

```shell
1$ git commit -m "primer commit"
[master (root-commit) 17466a8] primer commit
 1 file changed, 1 insertion(+)
 create mode 100644 arxiu.txt
1$ git status
On branch master
Your branch is based on 'origin/master', but the upstream is gone.
  (use "git branch --unset-upstream" to fixup)
nothing to commit, working tree clean
```

També pots mirar el log, el lloc on es guarden els canvis del repositori:

```shell
1$ git log
commit 17466a86c10203150c8502e3aaedb8066c9d9b67 (HEAD -> master)
Author: elteunom <elteu@correu.com>
Date:   Sun Apr 26 19:39:54 2020 +0200
    primer commit
```

També hi ha un format en una línia d’aquesta comanda:

```shell
1$ git log --graph --oneline
* 17466a8 (HEAD -> master) primer commit
```

Està dient que tenim un commit “17466a8” i que és l’actual.

### Pujar contingut

Cal fer un push:

```shell
1$ git push
Enumerating objects: 3, done.
Counting objects: 100% (3/3), done.
Writing objects: 100% (3/3), 216 bytes | 216.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
 * [new branch]      master -> master
```
Nou estat:

```shell
1$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
```

Ara ens diu que estem sincronitzats amb origin/master. Nou log:

```shell
1$ git log --graph --oneline
* 17466a8 (HEAD -> master, origin/master) primer commit
```

Ara ens apareix origin/master.

### Treballar amb un segon usuari

Simularem que tenim un segon usuari amb un altre repositori. Per simplificar, els dos usuaris poden compartir credencials de github. Alternativament (recomanable), crea tants usuaris com calgui, i fes que siguin col.laboradors del projecte. Això es pot fer tant a github com a gitlab:

*   **github**: cal afegir un col·laborador des del projecte > Settings > Manage access > Invite a collaborator.
*   **gitlab**: cal afegir un membre des del projecte > Settings > Members > Invite member. Selecciona “mantainer” com a perfil.

Creem un segon workspace directory. Per distingir els dos, tindrem dos prompts diferents: 1$ i 2$.

```shell
2$ git clone https://github.com/usuari/repositori.git
Cloning into 'repositori'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Total 3 (delta 0), reused 3 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
```

Ara, modificarem l’arxiu i mirem l’estat:

```shell
2$ echo "Com ba tot?" >> arxiu.txt
2$ git status
On branch master
Your branch is up to date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
  modified:   arxiu.txt
no changes added to commit (use "git add" and/or "git commit -a")
```

Ens diu que hi ha un arxiu modificat (modified), però no està al staging area.

Ens hem equivocat! Volíem escriure “Com va tot?”. Podríem editar l’arxiu un altre cop i esborrar la nova línia, però aprofitarem per recuperar l’arxiu abans de fer la modificació. Com que no hem fet el commit, es pot recuperar així:

```shell
2$ git reset --hard
HEAD is now at 17466a8 primer commit
2$ echo "Com va tot?" >> arxiu.txt
```

Ara, afegim l’arxiu al staging area i fem el commit:

```shell
2$ git add arxiu.txt 
2$ git status
On branch master
Your branch is up to date with 'origin/master'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified:   arxiu.txt
2$ git commit -m "afegim pregunta"
[master b475802] afegim pregunta
 1 file changed, 1 insertion(+)
2$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
```

Ara, afegim els canvis al repositori remot:

```shell
2$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Writing objects: 100% (3/3), 263 bytes | 263.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
   17466a8..b475802  master -> master
2$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
```

Ja tenim tots els canvis en remot. Mirem el log:

```shell
2$ git log --graph --oneline
* b475802 (HEAD -> master, origin/master, origin/HEAD) afegim pregunta
* 17466a8 primer commit
```

Com es veu, l’últim commit (b475802: “afegim pregunta”) es mostra com l’actual.

### Rebre els canvis al primer usuari

Ara retornem al primer usuari (1$). Si mirem l’estat i el log:

```shell
1$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
1$ git log --graph --oneline
* 17466a8 (HEAD -> master, origin/master) primer commit
```

Com es pot veure, l’estat diu que està actualitzat amb origin/master (repositori remot), i al log no hi ha el nou commit que s’ha pujat al repositori remot (“primera pregunta”).

Per poder veure’l, cal baixar-se els canvis del remot. Això es pot fer amb un fetch:

```shell
1$ git fetch
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Total 3 (delta 0), reused 3 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
From https://github.com/usuari/repositori
   17466a8..b475802  master     -> origin/master
```

Si es mira l’estat i el log:

```shell
1$ git status
On branch master
Your branch is behind 'origin/master' by 1 commit, and can be fast-forwarded.
  (use "git pull" to update your local branch)
nothing to commit, working tree clean
1$ git log --graph --oneline
* 17466a8 (HEAD -> master) primer commit
```

El log no ha canviat, perquè fetch no integra els canvis al repositori, però l’estat sí: ara ens diu que estem per darrere d’origin/master, i que hauríem de fer un git pull. Com que un pull és un fetch + merge, farem només el merge.

El merge intentarà barrejar automàticament el contingut remot recuperat i el que tenim al working directory.

```shell
1$ git merge
Updating 17466a8..b475802
Fast-forward
 arxiu.txt | 1 +
 1 file changed, 1 insertion(+)
```

El merge ha funcionat: ha afegit una nova línia. Com es veu, aquesta operació és immediata: no necessita anar al repositori remot. L’arxiu s’ha actualitzat, i l’estat i el log estan igualats amb els de l’usuari 2:

```shell
1$ cat arxiu.txt
Hola, món!
Com va tot?
1$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
1$ git log --graph --oneline
* b475802 (HEAD -> master, origin/master) afegim pregunta
* 17466a8 primer commit
```

### Etiquetes

Les etiquetes (o tags) és una forma senzilla d’identificar un cert commit o estat dins del repositori. Es poden posar locals i pujar-les en remot. A github, quan es pugen en remot, s’associen a una release que permet descarregar un arxiu empaquetat. A gitlab també es pot fer, però la secció es diu “tags”.

Per afegir un tag al commit actual i mostrar-lo:

```shell
1$ git tag v1.0
1$ git tag
v1.0
```

Per pujar una etiqueta:

```shell
1$ git push origin v1.0
Total 0 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
 * [new tag]         v1.0 -> v1.0
```

Si volem veure les etiquetes des de l’altre repositori:

```shell
2$ git fetch
From https://github.com/usuari/repositori
 * [new tag]         v1.0       -> v1.0
2$ git tag
v1.0
```

### Checkout i altres

El checkout ens permet recuperar qualsevol working directori per a un commit. És la veritable raó de ser dels repositoris: poder viatjar en el temps.

Per exemple, podem recuperar un arxiu concret d’un commit. De l’últim ( — significa que no indiquem el commit), o d’un concret:

```shell
1$ git checkout -- arxiu.txt
1$ git checkout 17466a8 arxiu.txt
```

Podem recuperar tot un commit, per exemple, el “primer commit”:

```shell
1$ git checkout 17466a8
Note: checking out '17466a8'.
You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.
If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:
  git checkout -b <new-branch-name>
HEAD is now at 17466a8 primer commit
1$ git status
HEAD detached at 17466a8
nothing to commit, working tree clean
1$ cat arxiu.txt
Hola, món!
```

Com es veu, tornem al contingut de l’arxiu abans del segon commit.

El problema d’aquesta comanda és que estem en estat “detached HEAD”: no tenim commit actual i no podríem treballar amb el staging area.

Sempre podem retornar al master:

```shell
1$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
```

I si volem recuperar cert commit al master, per exemple, el “primer commit”:

```shell
1$ git reset --hard 17466a8
HEAD is now at 17466a8 primer commit
```

També podem fer referència a una etiqueta:

```shell
1$ get reset --hard v1.0
```

Si volem que el reset quedi al repositori remot:

```shell
1$ git push

To https://github.com/usuari/repositori.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'https://github.com/usuari/repositori.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Això falla perquè no es pot fer un push d’un commit antic: git sempre comprova que siguin commits més nous. Podem ometre aquesta comprovació amb el paràmetre –force:

```shell
1$ git push --force
Total 0 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
 + b475802...17466a8 master -> master (forced update)
```

Després de fer això, si anem a l’altre repositori i fem fetch:

```shell
2$ git fetch
From https://github.com/usuari/repositori
 + b475802...17466a8 master     -> origin/master  (forced update)
2$ git log --graph --oneline
* b475802 (HEAD -> master, tag: v1.0) afegim pregunta
* 17466a8 (origin/master) primer commit
2$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)
  modified:   arxiu.txt
no changes added to commit (use "git add" and/or "git commit -a")
```

Veiem que el repositori remot està al primer commit, però el local està al segon: ens diu “your branch is ahead”.

Això es pot resoldre canviant al commit remot en local:

```shell
2$ git reset --hard origin/master
2$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
2$ git log --graph --oneline
* 17466a8 (HEAD -> master, origin/master, origin/HEAD) primer commit
```

Una altra eina molt útil és git diff. Ens permet comparar dos commits locals o remots.Per exemple, per comparar el HEAD amb el tag v1.0 cal utilitzar un paràmetre amb el commit:

```shell
$ git diff v1.0
diff --git a/arxiu.txt b/arxiu.txt
index 0027e65..b4b62f7 100644
--- a/arxiu.txt
+++ b/arxiu.txt
@@ -1,2 +1,2 @@
 Hola, món!
-Com va tot?
+segona línia 13
```

En aquest cas, ens diu que el canvi del tag v1.0 al HEAD és que s’ha esborrat una línia (“Com va tot?”) i s’ha afegit una altra (“segona línia 13”).

Si volem comparar dos commits, afegirem dos paràmetres. Per exemple, si hem fet prèviament un git fetch, podem utilitzar aquesta comanda per veure si el master local i el remot estan sincronitzats:

```shell
$ git diff master origin/master
```

Una altra comanda útil és la d’esborrar arxius o carpetes. Les següents comandes permeten esborrar un arxiu o una carpeta. L’opció –cached permet esborrar només del repositori, no del workind directory. L’opció -r permet, per carpetes, fer la feina de forma recursiva.

```shell
$ git rm arxiu.txt
$ git rm --cached arxiu1.txt
$ git rm -r carpeta
```

Un cop fet això, cal afegir-ho a un commit amb `git add nomdarxiu` o bé el contingut de la carpeta i totes les subcarpetes amb `git add .`.

### Gitignore

Alguns tipus de fitxers no haurien de ser part del repositori de codi, i [es poden indicar afegint un patró](https://www.atlassian.com/git/tutorials/saving-changes/gitignore) a l’arxiu .gitignore que hi ha a les carpetes. En general, seria millor no afegir certs tipus d’arxius:

*   cachés de dependències, com els continguts de `/node_modules` o `/packages`
*   codi compilat, com `.o`, `.pyc`, i `.class`
*   carpetes de sortida de compilació, com  `/bin`, `/out`, o `/target`
*   arxius generats en temps d’execució com `.log`, `.lock`, o `.tmp`
*   arxius amagats del sistema, com `.DS_Store` o `Thumbs.db`
*   arxius de configuració personal dels IDE, com `.idea/workspace.xml`

## Escenari amb conflicte

{% plantuml %}
box "developer 1"
participant WorkingDirectory1
participant StagingArea1
participant LocalRepository1
endbox
participant RemoteRepository
box "developer 2"
participant LocalRepository2
participant StagingArea2
participant WorkingDirectory2
endbox

WorkingDirectory1 -> StagingArea1: **git add** arxiu.txt
note left: modifico\narxiu.txt
StagingArea1 -> LocalRepository1: **git commit**
LocalRepository1 -> RemoteRepository: **git push**

WorkingDirectory2 -> StagingArea2: **git add** arxiu.txt
note right: modifico\narxiu.txt
StagingArea2 -> LocalRepository2: **git commit**
LocalRepository2 ->X RemoteRepository: **git push**

RemoteRepository -> LocalRepository2: **git fetch**
LocalRepository2 ->X WorkingDirectory2: **git merge**
note right: conflictes\na arxiu.txt

WorkingDirectory2 -> StagingArea2: **git add** arxiu.txt
note right: resolc\narxiu.txt
StagingArea2 -> LocalRepository2: **git commit**
LocalRepository2 -> RemoteRepository: **git push**

RemoteRepository -> LocalRepository1: **git fetch**
LocalRepository1 -> WorkingDirectory1: **git merge**
note left: rebo\narxiu.txt
{% endplantuml %}

El conflicte més típic entre dos usuaris és que modifiquin la mateixa línia d’un arxiu. Quan els dos vulguin fer un push, el segon no podrà fer-lo amb un missatge d’error.

El primer fa:

```shell
1$ echo "segona línia 1" >> arxiu.txt 
1$ git add arxiu.txt 
1$ git commit -m "segona 1"
[master 8bf099d] segona 1
 1 file changed, 1 insertion(+)
1$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Writing objects: 100% (3/3), 262 bytes | 262.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
   17466a8..8bf099d  master -> master
1$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
```

I el segon:

```shell
2$ echo "segona línia 2" >> arxiu.txt
2$ git add arxiu.txt 
2$ git commit -m "segona 2"
[master eacb48e] segona 2
 1 file changed, 1 insertion(+)
2$ git push
To https://github.com/usuari/repositori.git
 ! [rejected]        master -> master (fetch first)
error: failed to push some refs to 'https://github.com/usuari/repositori.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

Com es veu, es diu que cal fer primer pull, ja que no pots fer push si no has integrat els canvis remots al teu repositori.

Provem de fer-ho. Primer el fetch:

```shell
2$ git fetch
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Total 3 (delta 0), reused 3 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
From https://github.com/usuari/repositori
   17466a8..8bf099d  master     -> origin/master
2$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)
nothing to commit, working tree clean
```

Ens demana el pull, farem el merge (ja hem fet el fetch). És important fer sempre el merge en un repositori on no hi hagi canvis pendents del commit.

```shell
2$ git merge
Auto-merging arxiu.txt
CONFLICT (content): Merge conflict in arxiu.txt
Automatic merge failed; fix conflicts and then commit the result.
$ git status
On branch master
Your branch and 'origin/master' have diverged,
and have 1 and 1 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)
Unmerged paths:
  (use "git add <file>..." to mark resolution)
  both modified:   arxiu.txt
no changes added to commit (use "git add" and/or "git commit -a")
```

Ja tenim el conflicte a arxiu.txt. Això es tradueix en el fet que git modifica l’arxiu del conflicte per a reflectir les dues versions, afegint tres delimitadors:

*   \<\<\<\<\<\< HEAD
*   La versió local
*   ======
*   La versió remota
*   \>\>\>\>\>\> nom_de_la_branca

En el nostre cas:

```shell
2$ cat arxiu.txt
Hola, món!
<<<<<< HEAD
segona línia 2
======
segona línia 1
>>>>>> refs/remotes/origin/master
```

En aquest punt, ens podríem fer enrere (no ho farem) fins a l’estat anterior del merge fent `git merge --abort`.

Ens diu que teníem “segona línia 2” (HEAD) i que al remot tenim “segona línia 1” (refs/remotes/origin/master). Per resoldre el conflicte manualment, hem d’editar aquest arxiu i decidir què fem, esborrant les línies delimitadores (<,=,>) i tot el que no ens interessi.

En el nostre cas, decidim que ni una línia ni l’altra: “segona línia 12”. Editem l'arxiu:

```shell
2$ cat arxiu.txt
Hola, món!
segona línia 12
```

Després d'editar-lo, cal fer `git add` per marcar el conflicte com a resolt i ja podem fer commit i push:

```shell
2$ git add arxiu.txt
2$ git commit -m "resolt!"
[master 6fada39] resolt!
2$ git status
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)
nothing to commit, working tree clean
2$ git push
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 8 threads
Compressing objects: 100% (2/2), done.
Writing objects: 100% (6/6), 523 bytes | 523.00 KiB/s, done.
Total 6 (delta 0), reused 0 (delta 0)
To https://github.com/usuari/repositori.git
   8bf099d..6fada39  master -> master
2$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
2$ git log --graph --oneline
*   6fada39 (HEAD -> master, origin/master) resolt!
|\  
| * 8bf099d segona 1
* | eacb48e segona 2
|/  
* 17466a8 primer commit
```

Es poden veure els dos commits en paral·lel, i com finalment hi ha un commit (6fada39) que resol el problema.

Ara tornem al repositori 1:

```shell
1$ git fetch
remote: Enumerating objects: 10, done.
remote: Counting objects: 100% (10/10), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 6 (delta 0), reused 6 (delta 0), pack-reused 0
Unpacking objects: 100% (6/6), done.
From https://github.com/usuari/repositori
   8bf099d..6fada39  master     -> origin/master
1$ git status
On branch master
Your branch is behind 'origin/master' by 2 commits, and can be fast-forwarded.
  (use "git pull" to update your local branch)
nothing to commit, working tree clean
1$ git log --graph --oneline
* 8bf099d (HEAD -> master) segona 1
* 17466a8 primer commit
1$ git merge
Updating 8bf099d..6fada39
Fast-forward
 arxiu.txt | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)
1$ git status
On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
1$ git log --graph --oneline
*   6fada39 (HEAD -> master, origin/master, origin/HEAD) resolt!
|\  
| * 8bf099d segona 1
* | eacb48e segona 2
|/  
* 17466a8 primer commit
```

I ja tenim els dos repositoris sincronitzats després del conflicte.

## Flux

Aquest és el flux de treball proposat:

1.  Obtenir canvis remots, en dos passos:
    *   Obtenir-los amb **git fetch**
    *   Barrejar-los amb **git merge**
2.  Si el merge genera conflicte:
    *   Editar arxius conflictius 
    *   Fer **git add** de les solucions
    *   Fer **git commit**
3.  Fer canvis en local:
    *   Modificar els arxius del working directory
    *   Afegir-los al staging area (**git add**)
    *   Fer commit (**git commit**)
4.  Pujar canvis locals:
    *   Fer **git push**

{% plantuml %}
start

:$ git fetch
$ git merge;
if (conflictes?) then (sí)
	:editar-los (noms)
	$ git add (noms) 
	$ git commit
	$ git push;
else (no)
endif

repeat
	:editar arxius locals
	$ git commit;	
repeat while (finalitzat?) is (no)
-> sí;

:$ git push;

stop
{% endplantuml %}

## FAQ

### Alternatives per crear un repositori

Suposem que l'usuari **myuser** ha creat un nou repositori buit a gitlab anomenat **myproj**.

#### Crear un nou repositori

```shell
$ git clone https://gitlab.com/myuser/myproj.git
$ cd myproj
$ git switch -c main
# afegir arxius i .gitignore i fer commit d'ells
$ git push -u origin main
```

#### Pujar una carpeta existent

```shell
$ cd myproj
$ git init
$ git symbolic-ref HEAD refs/heads/main # millor main que master
$ git remote add origin https://gitlab.com/myuser/myproj.git
# afegir .gitignore preferit
$ git add .
$ git commit -m "commit inicial"
$ git push -u origin main
```

## Referències

*   [git/github guide](https://kbroman.org/github_tutorial/)
