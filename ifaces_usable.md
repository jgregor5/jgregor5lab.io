---
layout: page_toc
title: Usabilitat i informes
permalink: /ifaces_usable/
parent: Interfícies
nav_order: 2
---
Resultats d'aprenentatge:

1.  Dissenya interfícies gràfiques identificant i aplicant criteris d’usabilitat.
2.  Crea informes avaluant i utilitzant eines gràfiques.

## User Experience (UX)

La UX, o **User eXperience**, és com percep l'usuari final l'ús del teu producte, sistema o servei. Inclou emocions, creences, preferències, percepcions, respostes físiques i psicològiques. I tot això, tant abans, durant com després de l'ús.

Una de les teories que intenten explicar la UX és la dels tres cercles de l'arquitectura de la informació. Segons aquesta, els tres components a tenir en compte per a dissenyar un producte digital són:
*   El **context**: el negoci o la missió.
*   Els **usuaris**: les necessitats.
*   El **contingut**: la solució tècnica.

Els elements de la UX són (segons Garrett), de més abstracte a més concret:
*   Els objectius del generals del servei: si és de negoci, creatiu, social, etc.
*   Les necessitats de l'usuari, relacionat amb el seu origen i una segmentació.
*   Les especificacions funcionals per a complir les necessitats de l'usuari.
*   El disseny de la interacció: ha de facilitar les tasques, i defineix com interactua l'usuari amb les funcionalitats.
*   El disseny de la informació: com es presenta per a facilitar la seva comprensió.
*   El disseny de la interfície que facilita les interaccions.
*   El disseny visual (look-and-feel).

Les facetes de la UX que ha de reflectir el nostre producte o servei (segons Morville) són:
*   **Útil**: ha d'omplir una necessitat dels usuaris.
*   **Usable**: ha de ser simple i fàcil d'utilitzar, trobar-ho familiar, amb poc aprenentatge.
*   **Desitjable**: ha de provocar apreciació i emocions favorables amb la imatge, identitat i marca.
*   **Localitzable**: ha de ser fàcil trobar tot allò que es busca.
*   **Accessible**: hem de proporcionar accessibilitat a persones amb discapacitats.
*   **Confiable**: el disseny també influeix la credibilitat i confiança dels usuaris.
*   **Valuable**: si és amb ànim de lucre, ha de contribuir a l'objectiu i millorar la satisfacció de client. Si no ho és, ha de avançar la seva missió.

Les tasques a realitzar associades a l'UX són variades, i generen diferents perfils de dissenyadors:
*   Un dissenyador UX ha de fer recerca, identificar necessitats i crear fluxes de tasques. 
*   El dissenyador d'UI s'encarrega de la part cosmètica: la tipografia, els colors, l'espaiat, les icones, etc.

## Usabilitat

La **usabilitat** és la capacitat d'un sistema de proporcionar les condicions perquè els seus usuaris puguin realitzar les seves tasques de forma segura, efectiva i eficient, i que ho facin gaudint l'experiència. COm hem vist, es tracta d'una de les facetes de la UX.

Aquests són **10 principis heurístics** per a dissenyar interfícies usables (segons Nielsen).

1.  **Visibilitat de l'estat** del sistema. Cal compartir l'estat, i no actuar sense informar, donant feedback tan aviat com sigui possible.
2.  **Reflex del món real**. No hi ha d'haver vocabulari desconegut, cal reflectir la terminologia més familiar.
3.  **Control i llibertat** de l'usuari. Permetre desfer i refer, saber com cancel.lar i sortir.
4.  **Consistència i estàndards**. Interna, al producte i a una família, i externa, amb els estàndards de la indústria.
5.  **Prevenció d'errors**. Evitar els errors en situacions proclius, sempre comprovar-los i informar si cal alguna acció.
6.  **Reconèixer millor que recordar**. Reduir la quantitat d'informació que cal recordar, ajudar contextualment millor que fer tutorials.
7.  **Flexibilitat i eficiència**. Proporcionar dreceres, personalització i customització per a permetre diferents formes de portar a terme accions.
8.  **Disseny minimalista i estètic**. Centrar-se en els qüestions essencials, traient aspectes innecessaris i prioritzant els objectius principals.
9.  **Identificació d'errors**. Els errors s'han d'expressar en un llenguatge planer que indiqui el problema de forma precisa i ofereixi una solució constructiva.
10. **Ajuda i documentació**. Ajuda fàcil de buscar, en el context i amb passes concretes a realitzar.

### Visibilitat de l'estat

Només podem canviar l'estat del sistema si sabem quin és aquest estat.
Un cop tenim un objectiu, hi ha dos processos problemàtics o bretxes: 
*   l'avaluació: quin és l'estat actual del sistema?
*   l'execució: com puc utilitzar el sistema?

Alguns consells a seguir:
*   Dissenyar el sistema per a informar de l'estat per a poder actuar sobre ell, i veure com les accions modifiquen l'estat (interdependència). A més, el disseny ha de ser familiar per als usuaris. Per exemple: el comptaquilòmetres i l'accelerador. 
*   Donar una realimentació apropiada un cop es produeix la interacció, si la operació està en progrés i si ha acabat.
*   L'estat i les possibles interaccions ha de mostrar-se de forma senzilla, convidant els usuaris a actuar. 

Aquests són les opcions comunicatives que tenim:
*   **Indicadors**: fem destacar un element per a informar l'usuari que se l'ha d'atendre. Es pot fer amb icones, variacions tipogràfiques, canvis de mida o animacions. Són contextuals, apareixen condicionalment i són passius (no cal actuar).
*   **Validacions**: són missatges d'error sobre un problema a una entrada per part de l'usuari, que haurà d'actuar. Es pot utilitzar una icona.
*   **Notificacions**: alerten l'usuari sobre esdeveniments generals al sistema no relacionats amb accions immediates. Poden ser contextuals o globals, i poden requerir o no accions.

### Reflex del món real

Cal tenir en compte que nosaltres (dissenyadors) no som el mateix que els usuaris. Els termes que utilitzem li han de resultar familiars sense caldre consultar un diccionari.

La solució seria utilitzar objectes i activitats del món real. Si el model mental (teoria de com funciona el sistema) de l'usuari sobre el sistema s'assembla al de la realitat, li facilitem la comprensió i li diem que el coneixem i ens importa.

### Control i llibertat

Alguns aspectes a considerar:
*   Sempre permetre anar enrere.
*   Que anar enrere sigui el que espera l'usuari.
*   Que els enllaços de tancar, sortir o cancel.lar siguin visibles i estiguin on cal.
*   Que es pugui cancel.lar una acció en qualsevol punt intermedi i, si cal, distingir-ho de tancar.
*   Poder desfer una acció fàcilment.
*   Que desfer sigui visible, i ho sigui mentre es pugui dur a terme.

### Consistència i estàndards

Hi ha unes convencions que caldria respectar. Tenim internes i externes:
*   Les internes dins el nostre sistema o sistemes es pot guiar mitjançant un manual d'identitat de marca. 
*   Les externes poden basar-se en les convencions i estàndards establerts a la indústria. Per exemple, com funciona la navegació, quin aspecte tenen les pàgines d'inici, l'aspecte dels enllaços o dels botons, com s'introdueix la informació als formularis, com funcionen les cistelles de la compra, etc.

Alguns nivells als quals podem actuar:
*   El visual. Per exemple, l'hamburguesa del menú per a llocs mòbils.
*   La pàgina i la distribució de botons (ordre i colors).
*   La introducció de dades de forma guiada segons el seu tipus (telèfons, dates, etc.).
*   El contingut, mantenint un to consistent de comunicació.

### Prevenció d'errors

Tenim dos tipus d'errors:
*   **Relliscada**: és inconscient, per un descuit, l'usuari volia fer una acció però acaba fent una altra, de forma accidental.
*   **Errors**: és conscient, perquè el model mental de l'usuari no encaixa amb el disseny. L'acció acaba sent inapropiada per a la tasca que volem completar, perquè no hem entès com funciona el sistema.

Per a prevenir **relliscades**:
*   Incloure limitacions a les entrades de dades.
*   Oferir suggeriments i bons valors per defecte (tasques repetitives). Es poden utilitzar valors més freqüents o representatius.
*   Permetre formats alternatius per a introduir certs tipus de dades.

Per a prevenir **errors**:
*   Fes una bona visualització de l'estat.
*   Recull dades dels usuaris per veure on fallen.
*   Segueix les convencions de disseny.
*   Permet fer una previsualització del resultat.

Per a prevenir els dos tipus d'errors:
*   No obligar a recordar moltes coses.
*   Confirmar abans d'accions destructives.
*   Permetre el desfer.
*   Avisar d'errors abans de que passin.

### Reconèixer millor que recordar

Cal fer visibles i fàcilment accessibles:
*   La informació necessària per a aconseguir un objectiu.
*   Les funcions de la interfície, com botons, navegació i altres elements.

Per exemple:
*   Tenir un historial o contingut visitat prèviament.
*   La revelació progressiva amaga les funcions avançades o por habituals a espais secundaris.
*   Ajuda contextual i consells en lloc de tutorials.

### Flexibilitat i eficiència

No tots els usuaris tenen les mateixes necessitats. Cal:
*   Tenir mètodes diferents per a acomplir la mateixa tasca, segons les preferències.
*   Permetre l'usuari customitzar la interfície segons les necessitats, però deixant uns bons valors per defecte.
*   També pot ser que la customització la faci el sistema, llavors es diu personalització.
*   Tenir acceleradors per a usuaris avançats, que no afecten als primerencs. Per exemple, gestos o macros.

### Disseny minimalista i estètic

Els aspectes estètics són importants. Les primeres impressions són importants, com també ho són la percepció visual per sobre de l'experiència. A més, reforcen la identitat de marca.

Per altra banda, el disseny minimalista demana que hi hagi tots els elements per a suportar les tasques de l'usuari, però cap altra més. Tenir elements de més pot amagar els elements necessaris. Podem reduir el soroll del disseny:
*   Aprofitant patrons universals de disseny amb connotacions positives. Per exemple, certs tipus d'imatges, com paisatges.
*   Acceptar que la bellesa està a l'ull de l'espectador, i que hem de considerar les persones.

Els cinc **principis de disseny visual** poden ajudar a assolir aquest disseny:
1.  Escala: utilitzar mides relatives a la importància i prioritat dins de la composició dels elements.
2.  Jerarquia visual: guiar l'ull dins de la pàgina perquè atengui els elements en ordre d'importància.
3.  Balanç: passa quan hi ha una càrrega de senyals visuals distribuïda de forma igualitària als dos costats d'un eix imaginari.
4.  Contrast: la juxtaposició d'elements no iguals per a mostrar elements diferents.
5.  Principis gestalt: la tendència a percebre diversos elements individuals com a un tot, ja que ens resulta una forma més estable.

### Identificació d'errors

Segueix aquesta guia per a escriure els teus missatges d'error:
*   Cal que sigui explícit, indicant que alguna cosa concreta ha fallat.
*   Ha de ser fàcil de llegir per a qualsevol persona, sense codis ni abreviacions.
*   Ha de ser respectuós.
*   Ha de ser precís, sense vaguetats genèriques.
*   Ha d'aconsellar constructivament com resoldre el problema.

A més, els errors s'han de mostrar amb tractaments visuals que permetin identificar-los i reconèixer-los. Utilitza els visuals de les convencions, com text vermell o negreta.

### Ajuda i documentació

Tenim dos tipus d'ajuda: proactiva i reactiva.

L'ajuda **proactiva**, abans de que es produeixi un error. L'onboarding (primer cop que veiem la interfície) i els consells contextuals són d'aquest tipus. S'orienta a familiaritzar l'usuari amb l'interfície. Pot estar fora del flux de treball i s'ignoren (push) o contextuals i són útils (pull).
Guia:
*   Mantenir-la curta i en el punt on som per no distreure l'usuari.
*   Afavorir la pull sobre la push.
*   Les revelacions push haurien de ser fàcils d'ignorar.
*   Ha de ser accessible des de qualsevol lloc (fins i tot quan s'han ignorat).

L'ajuda **reactiva**, amb materials de consulta. S'orienta a respondre preguntes, consultar problemes o ajudar a usuaris que volen ser experts. De vegades s'orienten amb un FAQ. 
Guia:
*   Assegurar-se que la documentació és comprensiva i detallada.
*   La informació més important s'ha de presentar abans.
*   Considerar l'ús de gràfics i vídeos com a font secundària.
*   Permetre la cerca.
*   Organitzar per categories.
*   Subratllar el contingut visitat freqüentment.

## Accessibilitat Web

A continuació es mostren una sèrie de consells en funció de l'activitat a què es refereixen: el disseny, la creació de continguts i el desenvolupament.

### Disseny accessible

*   Proporcioneu un contrast suficient entre el primer pla i el fons.
*   No utilitzeu només el color per transmetre informació.
*   Assegureu-vos que els elements interactius siguin fàcils d'identificar: botons i enllaços.
*   Proporcioneu opcions de navegació clares i coherents: capçaleres, molles de pa, mapes.
*   Assegureu-vos que els elements del formulari inclouen etiquetes associades clarament.
*   Proporcioneu feedback fàcilment identificable: icones, colors, etc.
*   Utilitzeu els encapçalaments i l'espaiat per agrupar contingut relacionat: faciliteu la comprensió.
*   Creeu dissenys per a diferents mides de visualització: responsivitat al mòbil.
*   Incloeu alternatives d'imatge i mitjans al vostre disseny: textos, captions, etc.
*   Proporcioneu controls per al contingut que s'inicia automàticament: als carrousels, sliders, vídeos, so de fons, etc.

### Contingut accessible

*   Proporcioneu títols de pàgines informatius i únics.
*   Utilitzar els encapçalaments (i subencapçalaments) per transmetre significat i estructura.
*   Fes que el text de l'enllaç tingui significat: evita el "clica aquí".
*   Escriu alternatives de text significatives per a imatges que tinguin una funció (no per a les decoratives).
*   Crea transcripcions i subtítols per a multimèdia.
*   Proporcioneu instruccions clares: a les instruccions, guies i als missatges d'error. Sense utilitzar llenguatge tècnic.
*   Mantingueu el contingut clar i concís.

### Desenvolupament accessible

*   Associa una etiqueta a cada control de formulari (label for).
*   Incloeu text alternatiu per a les imatges (alt).
*   Identificar l'idioma de la pàgina i els canvis d'idioma (html lang).
*   Utilitzeu les marques per transmetre significat i estructura: HTML semàntic com section, article, aside, ul, etc.
*   Ajudeu els usuaris a evitar i corregir errors: indicar on hi ha el problema, explicar-ho i suggerir correccions.
*   Reflecteix l'ordre de lectura en l'ordre del codi: evita que el CSS sigui qui marqui aquest ordre.
*   Escriu codi que s'adapti a la tecnologia de l'usuari: responsivitat.
*   Donar significat als elements interactius no estàndard: utilitza [WAI-ARIA](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/WAI-ARIA_basics).
*   Assegureu-vos que tots els elements interactius siguin accessibles amb el teclat.
*   Eviteu CAPTCHA sempre que sigui possible.

## Referències
*   [10 Usability Heuristics for User Interface Design](https://www.nngroup.com/articles/ten-usability-heuristics/)
*   [The Beginner’s Guide to Information Architecture in UX](https://xd.adobe.com/ideas/process/information-architecture/information-ux-architect/)
*   [Designing for Web Accessibility](https://www.w3.org/WAI/tips/designing/)
