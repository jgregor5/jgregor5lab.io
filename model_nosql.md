---
layout: page_toc
title: Model NoSQL
permalink: /model_nosql/
parent: Diversos
nav_order: 10
nav_exclude: true
---

## Model NoSQL

El principal problema d'un model relacional és la distribució d'una BBDD en diferents servidors per raó de mida i rendiment (clústers), i que pugui ser accedida com una sola.

El funcionament en clústers utilitza dues idees:

-   La **replicació**: repliquem les dades en master-slave o peer-to-peer.
-   El **sharding**: situem diferents parts de les dades en diferents servidors.

Les BBDD relacionals no estan dissenyades per a funcionar eficientment en clústers, aquesta és la raó principal per a migrar a NoSQL. La segona és que es veu com una una forma de millorar la productivitat en el desenvolupament d'aplicacions, ja que la interacció amb les dades és més còmoda.

Els tipus principals de BBDD NoSQL són els models agregats (key-value, document i column) i els de graf. 

Un **model agregat** és una col·lecció de dades amb les quals interactuem com a una unitat. Els models agregats permeten treballar més fàcilment amb clústers, agafant l'agregació com a unitat de replicació i sharding. A més, l'agregació també facilita la feina dels desenvolupadors, perquè la manipulació de dades es produeix molt sovint a nivell de agregat. 

## Referències

*   [NoSQL Distilled](https://martinfowler.com/books/nosql.html)
