---
layout: page_toc
title: Empresa i societat
permalink: /eie_societat/
parent: Empresa
nav_order: 2
---
## Entorn empresarial

L'activitat d'una empresa requereix d'una sèrie d'àrees funcionals. Algunes, relacionades directament amb el mercat o competències clau:

-   Direcció i control
-   Compres a proveidors i emmagatzametge
-   Producció
-   Comercial o vendes

D'altres, són de suport a les primeres:

-   Recursos humans
-   Àrea financera (obtenció de recursos econòmics)
-   Comptabilitat
-   Administració

L'entorn d'una empresa inclou proveïdors, clients, competidors, entitats financeres, administracions públiques, mercat laboral i comunitat.

Cal mencionar el concepte de **stakeholder** o grups d'interès, persones i grups a què afecta l'activitat de la nostra empresa i que poden influir en el seu funcionament. Per tant, cal tenir-los en compte en les decisions de l'empresa. Principalment inclouen els accionistes i els treballadors, però també proveïdors, clients, consumidors, entitats reguladores i competència. 

La empresa responsable ha de dirigir-se estratègicament no només a satisfer als **shareholders** o accionistes, que busquen el profit de les seves inversions, sinó també dels stakeholders, que componen el teixit social a que afecta l'activitat econòmica.

## Impacte social i ambiental

L'activitat empresarial té impactes econòmics, socials i ambientals. Pel fet d'integrar-se a la societat i interactuar amb ella, els comportaments associats poden produir impactes positius i negatius.

**Impactes positius**:

-   Suministre de productes o serveis útils.
-   Desenvolupament humà: creen ocupació, formen persones.
-   Creació de riquesa, que distribueix entre treballadors, administració (impostos), accionistes i proveidors.
-   Les institucions que ordenen l'activitat econòmica busquen el bé comú, i per tant les empreses ho fan subsidiàriament.

**Impactes negatius**:

-   Enfermetats professionals i accidents laborals.
-   Reconversió o desaparició de tipus de feines.
-   Per als treballadors, productes insegurs o insans.
-   Per als consumidors, productes adulterats, insegurs, tòxics, caducats, abusos amb la privacitat de les dades.
-   Deteriorament ecològic per contaminació, consum de recursos no renovables, sobreexplotació de recursos renovables, amenaces a la biodiversitat.

A les **resposabilitats d'una empresa** es poden distingir tres nivells:

-   **Legal**, en referència al cumpliment de la legislació civil, administrativa i penal.
-   **Ètica**, segons la deontologia professional (professió) o cultura corporativa (empresa).
-   **Moral**, en relació al conjunt de regles o principis de comportament d'una persona o col.lectivitat pròpies d'una cultura.

Si el comportament empresarial té una regla clara, n'hi ha prou amb aplicar-la. Si no, podem aplicar el següent enfoc: el correcte és el que produeix més beneficis per al major nombre de persones, i que no viola els drets individuals.

Podem qualificar els **comportaments** empresarials com a:

-   **Inmorals**: quan només busquem si la acció té beneficis o èxit. Les lleis són obstacles a superar, i no hi ha conducta ètica.
-   **Amorals**: busquem els beneficis o èxit dins de les regles del mercat i la llei. S'interpreta que l'ètica no és un tema empresarial, o no es considera rellevant l'efecte de les accions.
-   **Morals**: es busca l'èxit però només dins dels preceptes de conducta acceptats per la societat. L'empresa té objectius ètics, i interpreta l'esperit de la llei.

Per a l'impacte ambiental, hi ha una tendència a afegir legislació per part dels organismes públics que obliga a les empreses afectades. La difusió de la informació de l'impacte social d'una empresa és voluntària per a una empresa convencional. Aquesta informació permetria als individus prendre decisions de consum i a les empreses i l'administració, decisions de contractació.

## Responsabilitat social

La **responsabilitat social** corporativa o empresarial (RSC/RSE) és el compromís que de forma voluntària assumeixen les empreses i organitzacions per fer-se responsables dels seus impactes, fomentar un desenvolupament sostenible i crear valor econòmic i social.

Els àmbits on calen accions transformadores són els següents:

-   **Bon govern**
    1.  Gestionar amb ètica i transparència.
    1.  Contribuir al benestar de la societat i al bé comú.
    1.  Mesurar la gestió dels àmbits per poder prendre decisions.
    1.  Informar públicament dels avenços.
    1.  Fomentar espais de diàleg amb els grups d'interés.
-   **Econòmic**
    1.  Evitar l'evasió i elusió fiscal.
    1.  Apostar per la compra de proximitat i els recursos locals.
    1.  Afegir criteris no econòmics a les compres.
    1.  Promoure relacions basades en el benefici mutu a la cadena de proveïment.
    1.  Innovar en processos, productes i serveis i invertir en R+D+I.
    1.  Realitzar accions d'inversió socialment responsable.
-   **Laboral**
    1.  Generar ocupació de qualitat: estabilitat i condicions de treball dignes.
    1.  Gestionar les persones amb la seva participació quan els incumbeix.
    1.  Assegurar entorns de treball saludables.
    1.  Desenvolupar les competències i habilitats dels treballadors.
    1.  Gestionar positivament la diversitat.
    1.  Fomentar la reforma horària i la conciliació.
-   **Ambiental**
    1.  Reduir els consums de recursos naturals i energètics.
    1.  Promoure una mobilitat sostenible.
    1.  Reduir i reaprofitar residus.
    1.  Utilitzar productes i serveis respectuosos amb el medi.
    1.  Sumar esforços destinats a la mitigació i l'adaptació al canvi climàtic.
    1.  Apostar per l'economia circular.
-   **Social**
    1.  Impulsar la cohesió social i el compromís amb la comunitat.
    1.  Promoure el consum responsable.
    1.  Integrar la responsabilitat social en l'educació, la formació i la recerca.
    1.  Respectar i protegir els drets humans en tota la cadena de valor.
    1.  Intercanviar experiències i bones pràctiques.

Hi ha una sèrie d'[eines de gestió de la responsabilitat social per a PIMES](https://treball.gencat.cat/ca/detalls/article/La-responsabilitat-social-a-la-PIME) associades a certificacions que permeten avaluar una organització i obtenir un segell reconeixible socialment. En aquesta llista destaquen el [Balanç Social](https://mercatsocial.xes.cat/ca/eines/balancsocial/) o el [Pacte Mundial](https://www.pactomundial.org/).

Pel fet que cada cop és més important aparèixer social com a empresa responsable socialment, s'està produint el fenòmen del **"rentat d'imatge verd"** (greenwashing): és l'acció d'una empresa, un govern o un organisme d'usar el màrqueting per a promoure la percepció que els seus productes, objectius o polítiques són respectuosos amb el medi ambient, quan en realitat funciona de manera oposada.

## Objectius de Desenvolupament Sostenible (ODS)

Els ODS és una iniciativa de les Nacions Unides amb una agenda fins al 2030. Són una crida universal a l'acció per posar fi a la pobresa, protegir el planeta i millorar les vides i les perspectives de les persones a tot el món. 

Estan dirigits a la societat, de forma general. Aquesta és l'aproximació empresarial que fan les Nacions Unides als ODS per a cada objectiu:

1.  **Fi de la pobresa**. Donar oportunitats laborables per a grups vulnerables, amb condicions dignes i impactant positivament a les comunitats locals.
2.  **Fam zero**. Investigar la tecnologia agrícola, pràctiques sostenibles a la cadena de subministrament i accés a aliments sans i suficients.
3.  **Salut i benestar**. Plans de seguretat i salut laboral per als treballadors i cadenes de valor, evitant impacte negatiu de les seves operacions i contribuir positivament sobre el benestar.
4.  **Educació de qualitat**. Formació dels empleats i grups d'interés, inversió en educació per a millorar les oportunitats laborals i salaris.
5.  **Igualtat de gènere**. Garantir els mateixos drets i oportunitats laborals a la dona, programes d'empoderament econòmic.
6.  **Aigua neta i sanejament**. Gestió sostenible de recursos hídrics en la elaboració de productes i serveis, foment de la millora de la gestió sostenible en la cadena de valor.
7.  **Energia asequible i no contaminant**. Inversió en fonts d'energia neta, tecnologies que redueixen el consum elèctric, projectes per electrificar comunitats desfavorides.
8.  **Feina decent i creciment econòmic** sostenible. Garantir les condicions dignes de treball, foment de la ma d'obra vulnerable.
9.  **Industria, innovació i infrastructura**. Innovació per a la sostenibilitat, processos industrials sense impacte sobre el medi, infrastructures sostenibles i resilients, accés TIC a tots els treballadors, tecnologies eficients i sostenibles.
10. **Reducció de les desigualtats**. Condicions laborals dignes, redistribució igualitària dels salaris, mecanismes per evita l'evasió fiscal, projectes de cooperació al desenvolupament.
11. **Ciutats i comunitats sostenibles**. Innovació per al desenvolupament de ciutats sostenibles i intel·ligents, mobilitat sostenible, reducció de consum energètic i aigua.
12. **Producció i consum responsables**. Ús eficient de recursos a la cadena de valor, retirar productes i serveis amb consum excessiu, impuls d'energies renovables, reutilització hídrica, reduir la contaminació, formació en pràctiques de producció i consum sostenible, combatre el desperdici alimentari, ecoetiquetat.
13. **Acció per al clima**. Reduir emissions de gasos, impulsar energies renovables, innovació al medi.
14. **Vida submarina**. Reduir la contaminació de mars i oceans, promoure la pesca sostenible, ajustar-se al dret internacional.
15. **Vida d'econsistemes terrestres**. Evitar l'impacte sobre els econsistemes i hàbitats en les operacions de l'empresa, respectar la normativa corresponent, integrar la conservació de la diversitat biològica.
16. **Pau, justícia i institucions sòlides**. Incorporar el respecte als DDHH i transparència en l'organització, evitar quasevol tipus de violència sobre menos i grups volnerables, impulsar l'estat de dret.
17. **Aliances per a assolir els objectius**. Aliar-se amb el sector públic, la societat civil, universitats i altres empreses per a realitzar projectes en pro dels ODS.

## Les cooperatives

La organització amb responsabilitat social per antonomasia té la forma jurídica de **cooperativa**. Entre els principis que identifiquen la cooperativa cal destacar:

-   La democràcia empresarial que defineix la seva gestió.
-   La participació econòmica del socis.
-   L'interès per proporcionar formació i informació als socis.
-   La millora de la situació econòmica i social, tant dels components com de l'entorn comunitari.

Les cooperatives poden ser de diversos tipus. Les més addients per a produir béns o serveis són les **cooperatives de treball associat**. També hi ha les **cooperatives de serveis** per a professionals per compte propi. Finalment, hi ha dues condicions que poden tenir les cooperatives: ser d'iniciativa social o ser sense ànim de lucre. En funció d'aquestes característiques, poden tenir beneficis concrets.

## Referències

*   [Economia social](https://treball.gencat.cat/ca/ambits/economia_social/)
*   [La responsabilidad social de la empresa](https://dialnet.unirioja.es/servlet/articulo?codigo=565243)
*   [Responsabilitat social](https://treball.gencat.cat/ca/rscat/serveis/eines-autoavaluacio/responsabilitat-social)
*   [Recursos formatius de responsabilitat social](https://treball.gencat.cat/ca/consell_relacions_laborals/espais_dialeg/responsabilitat_social/recursos_formatius/)
*   [El sector privado ante los ODS](https://www.pactomundial.org/wp-content/uploads/2016/09/Guia_ODS_online.pdf)
*   [17 objectius per a les persones i el planeta](https://www.youtube.com/watch?v=MCKH5xk8X-g)
*   [Carlos Taibo: "El planeta se nos va y es necesario frenar de inmediato la locomotora del crecimiento"](https://www.climatica.lamarea.com/carlos-taibo-frenar-crecimiento/)
*   [¿Es el decrecimiento económico una alternativa real?](https://www.ideasimprescindibles.es/decrecimiento-economico-alternativa-real/)
*   [Los fallos del PIB y sus alternativas](https://elordenmundial.com/pib-alternativas-producto-interior-bruto/)
