---
layout: page_toc
title: Píndoles POO
permalink: /pindoles_poo/
parent: Diversos
nav_order: 3
---

## Conceptes de POO

### Abstracció

L'abstracció amaga la complexitat a l'usuari i només mostra la informació rellevant. Permet centrar-nos en el "què" en lloc del "com". Els detalls d'un mètode abstracte són implementats de forma separa per cada classe.

**Objectius**: reusabilitat de codi, flexibilitat d'implementació, herència múltiple.

**Java**: classes abstractes (abstracció parcial) i interfícies (abstracció total).

**Sintaxi**: `abstract` (classes abstractes) i `interface`, `implements` (interfícies).

### Encapsulació

L'encapsulació lliga les dades i els mètodes relacionats dins d'una classe. També protegeix les dades (estat) fent els camps privats i donant accés a ells només mitjançant els mètodes relacionats que implementen les regles de negoci i invariants.

**Objectius**: protecció de dades (estat), llegibilitat del codi.

**Java**: camps privats i mètodes públics (només els exposats).

**Sintaxi**: `private, setX(), getX()`.

### Herència

Amb l'objectiu de reusar codi, l'herència permet que una classe fill hereti les característiques (camps i mètodes) d'una altra classe pare.

**Objectius**: reusabilitat de codi, llegibilitat del codi.

**Java**: Classe pare i class fill. Alternativa vàlida: patrons de composició i delegació.

**Sintaxi**: `extends`.

### Polimorfisme

El polimorfisme permet substituir i estendre la funcionalitat d'objectes que tenen una mateixa interfície per uns altres durant el temps d'execució (dynamic binding).

**Objectius**: llegibilitat del codi, flexibilitat del codi.

**Java**: sobreescriptura de mètode (dinàmic), menys rellevant: sobrecàrrega de mètode (estàtic).

**Sintaxi**: `myMethod() myMethod(int x) myMethod(int x, String y)` (estàtic) i `ParentClass.myMethod() ChildClass.myMethod()` (dinàmic).

## Quan fer herència

L'herència només s'ha d'utilitzar quan:

1.  Les dues classes es troben en el mateix domini lògic
2.  La subclasse és un subtipus adequat de la superclasse
3.  La superclasse és un contracte (interfície o classe abstracta)
4.  La implementació de la superclasse és necessària o apropiada per a la subclasse
5.  Les millores realitzades per la subclasse són principalment additives.

## Generalització, associació, agregació i composició

{% plantuml %}
hide circle
skinparam linetype ortho

interface Comparable {
    compareTo()
}

class Double {
    isInfinite()
    isNaN()
}

class Number {
}

Number <|-- Double
Comparable <|.. Double

class Parent1 {
  Integer age
  teach()
  takecare()
  earn()
}
class Child1 {
  Integer age
  learn()
}

Parent1 -- Child1

class Parent2 {
  Integer age
  teach()
  takecare()
  earn()
}
class Child2 {
  Integer age
  learn()
}

Parent2 <-- Child2

class Car {
  move()
}
class Wheel {
  rotate()
}

Car o-- Wheel

class Human {
  breath()
}
class Heart {
  pumpBlood()
}

Human *-- Heart

class Object1
class Object2

Object1 ..> Object2

{% endplantuml %}

1.  Herència de Number i implementació de Comparable
1.  Associació entre Child1 i Parent1
1.  Associació navegable des de Child2 cap a Parent2
1.  Agregació: Wheel pot existir sense Car
1.  Composició: Heart no pot existir sense Human
1.  Dependència: Object1 usa l'Object2

## Patrons de disseny

Un **patró de disseny** és una solució general a un problema comú i recurrent en el disseny de programari. Un patró de disseny no és un disseny acabat que es pot transformar directament en codi; és una descripció o plantilla per resoldre un problema que es pot utilitzar en moltes situacions diferents.

Tenim algunes categories generals:

*   **De comportament**: identifiquen patrons de comunicació entre objectes.
*   **Estructurals**: faciliten el disseny quan s'han d'establir relacions entre entitats.
*   **Creacionals**: relacionats amb mecanismes de creació d'objectes de la forma més adient per cada cas.
*   **Concurrència**: tracten el paradigma de programació multifil.

A continuació es mostren alguns patrons importants.

### Patrons de comportament

#### Command

El patró command encapsula una sol·licitud com a objecte, de manera que us permetrà parametrizar altres objectes amb diferents peticions, cues o peticions de registre i donar suport a operacions reversibles.

{% plantuml %}
class Client

class Invoker {
}

class Receiver {
    + action()
}

class Command {
    + execute()
}

class ConcreteCommand {
    + execute()
}

hide empty members

Client --> Receiver
Invoker *-- Command
Client ..> ConcreteCommand : creates
Receiver <- ConcreteCommand
ConcreteCommand -|> Command
{% endplantuml %}

Tenim dos actors principals: el Client crea el ConcreteCommand i assigna el seu Receiver. L'Invoker té Commands que pot executar.

#### Iterator

El patró iterator proporciona una manera d'accedir als elements d'un objecte agregat seqüencialment sense exposar la seva representació subjacent.

{% plantuml %}
class Client

interface Aggregate {
    + createIterator()
}

class ConcreteAggregate {
    + createIterator() : Context
}

interface Iterator {
    + next() : Context
}

class ConcreteIterator {
    + next() : Context
}

hide empty members

Client --> Aggregate
Client --> Iterator
Aggregate <|-- ConcreteAggregate
Iterator <|-- ConcreteIterator
{% endplantuml %}

Exemples: [java.util.Iterator](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Iterator.html) i [java.util.Enumeration](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/Enumeration.html)

#### Observer

El patró observer defineix una dependència entre molts objectes de manera que quan un objecte canvia d'estat, tots els seus dependents són notificats i actualitzats automàticament.

{% plantuml %}
interface Subject {
    + attach(in o : Observer)
    + detach(in o : Observer)
    + notify()
}

class ConcreteSubject {
    - subjectState
}

interface Observer {
    + update()
}

class ConcreteObserver {
    - observerState
    + update()
}

hide empty members

Subject -> Observer : notifies
Subject <|-- ConcreteSubject
Observer <|-- ConcreteObserver
ConcreteSubject <- ConcreteObserver : observes
{% endplantuml %}

Exemples: [java.util.EventListener](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/EventListener.html)

#### State

El patró state permet a un objecte alterar el seu comportament quan canvia el seu estat intern. L'objecte semblarà canviar de classe.

{% plantuml %}
class Context {
    + request()
}

interface State {
    + handle()
}

class ConcreteState1 {
    + handle()
}

class ConcreteState2 {
    + handle()
}

hide empty members

Context *-- State
State <|-- ConcreteState1
State <|-- ConcreteState2
{% endplantuml %}

El Context pot tenir un nombre de States. Quan cridem request(), el que fem es cridar el handle corresponent del State actual. Un Client no coneix el funcionament intern dels States.

#### Strategy

El patró strategy s’utilitza quan tenim diversos algorismes per a una tasca específica i el client decideix que s’utilitzi la implementació real en temps d’execució.

{% plantuml %}
class Context

interface Strategy {
    + execute()
}

class ConcreteStrategyA {
    + execute()
}

class ConcreteStrategyB {
    + execute()
}

hide empty members

Context *-- Strategy
Strategy <|-- ConcreteStrategyA
Strategy <|-- ConcreteStrategyB
{% endplantuml %}

És molt similar a State, però és el Client qui habitualment escull la Strategy.
Exemples: Collections.sort() amb el paràmetre Comparator

#### Dependency Injection

El patró de Dependency Injection permet a un objecte rebre les instàncies d'altres objectes de que depèn. Això permet que el client no s'hagi de referir a instàncies concretes, seguint el principi d'inversió de dependència. S'utilitza per implementar el principi de la inversió de control (IoC) mitjançant un contenidor.

Hi ha tres tipus d'injecció: Constructor, Setter i Interface.
*   Constructor: li diem al contenidor quina implementació s'utilitza per als paràmetres del constructor, que poden ser interfícies.
*   Setter: li diem al contenidor quines propietats (amb setters) cal instanciar d'un cert objecte.
*   Interface: li diem al contenidor que el nostre client rebrà les dependències mitjançant un mètode (del tipus setService).

Per utilitzar-los, cal configurar el contenidor. Es pot fer programàticament o mitjançant un arxiu de configuració.

{% plantuml %}
@startuml
skinparam linetype ortho

class Client
interface ServiceA
interface ServiceB
class Injector
class ConcreteServiceA
class ConcreteServiceB

Injector -u.> Client : inject
Client -r-> ServiceA
Client -r-> ServiceB
Injector -u.> ConcreteServiceA : create
Injector -u.> ConcreteServiceB : create
ConcreteServiceA -u.|> ServiceA
ConcreteServiceB -u.|> ServiceB
@enduml
{% endplantuml %}

L'objecte que injecta les dependències es diu injector, i els objectes de que depèn solen ser serveis.

#### Service Locator

El Service Locator és una alternativa al Dependency Injector, amb la particularitat que el codi client té una dependència del contenidor, que utilitza per localitzar tots els serveis que necessita. És a dir, no es produeix la injecció automàtica. 

No es tracta d'una Factory, ja que no crea els serveis cada cop, només si cal. Es tracta més bé d'un registre.

{% plantuml %}
class Client
interface ServiceA
interface ServiceB
class Locator {
    + getService()
}

Client -> Locator : uses
Locator -d-> ServiceA : locates
Locator -d-> ServiceB : locates
{% endplantuml %}

#### Template method

El patró template method defineix l'esquelet d'un algorisme d'un mètode, diferint alguns passos a subclasses. Aquest patró permet que les subclasses redefineixin certs passos d'un algorisme sense canviar l’estructura de l’algoritme.

{% plantuml %}
class AbstractClass {
    + templateMethod()
    # subMethod()
}

class ConcreteClass {
    + subMethod()
}

hide empty members

AbstractClass <|-- ConcreteClass
{% endplantuml %}

El templateMethod fa ús del subMethod.
Exemple: [construcció d'una casa de fusta o de vidre](https://www.journaldev.com/1763/template-method-design-pattern-in-java)

### Patrons estructurals

#### Adapter

El patró adapter converteix la interfície d'una classe en una altra interfície que els clients esperen. L'adaptador permet que les classes treballin conjuntament, tot i tenir interfícies incompatibles.

{% plantuml %}
interface Adapter {
    + operation()
}

class Client

class ConcreteAdapter {
    - adaptee
    + operation()
}

class Adaptee {
    + adaptedOperation()
}

hide empty members

Adapter <- Client
Adapter <|-- ConcreteAdapter
ConcreteAdapter -> Adaptee
{% endplantuml %}

El ConcreteAdapter està composat amb l'Adaptee, que li permet una operació addicional: adaptedOperation.
Exemple: [un capità que només pot utilitzar barques de rem i no pot navegar](https://java-design-patterns.com/patterns/adapter/)

#### Composite

El patró composite permet compondre objectes en estructures d’arbre per representar jerarquies parcials. Composite permet als clients tractar objectes individuals i composicions d'objectes de manera uniforme.

{% plantuml %}
interface Component {
    + operation()
    + add(in c : Component)()
    + remove(in c : Component)
    + getChild(in i : int)
}

class Leaf {
    + operation()
}

class Composite {
    + operation()
    + add(in c : Component)()
    + remove(in c : Component)
    + getChild(in i : int)
}

Component <|-- Leaf
Component <|-- Composite
Component --* Composite
{% endplantuml %}

Tant els objectes individuals com els composats poden ser tractats igual amb operation.

#### Decorator

El patró decorator atribueix dinàmicament responsabilitats addicionals a un objecte. Els decorators proporcionen una alternativa flexible a la subclasse per ampliar la funcionalitat.

{% plantuml %}
interface Component {
    + operation()
}

class ConcreteComponent {
    + operation()
}

class Decorator {
    + operation()
}

class ConcreteDecorator {
    - Component wrappedObj    
    - Object newState
    + operation()
    + addedBehaviour()
}

Component <|-- ConcreteComponent
Component <|-- Decorator
Component --* Decorator
Decorator <|- ConcreteDecorator
{% endplantuml %}

Podem afegir un comportament al ConcreteDecorator.
Exemple: [trol decorat amb una porra](https://java-design-patterns.com/patterns/decorator/)

#### Facade

El patró facade proporciona una interfície unificada i més senzilla a un conjunt d’interfícies d’un subsistema. La façana defineix una interfície de més alt nivell que facilita la utilització del subsistema, seguint el principi del "Least knowledge".

{% plantuml %}
class Facade

package "Complex system" <<Node>> {
    class Dummy1
    class Dummy2
    class Dummy3
    class Dummy4 
}

hide empty members

Facade --> Dummy1
Facade --> Dummy2
Facade --> Dummy3
Facade --> Dummy4
{% endplantuml %}

Exemple: [treballadors de la mina d'or](https://java-design-patterns.com/patterns/facade/)

#### Proxy

El patró de proxy proporciona un substitut per a un altre objecte per controlar-ne l'accés.

{% plantuml %}
class Client

interface Subject {
    + request()
}

class RealSubject {
    + request()
}

class Proxy {
    + request()
}

hide empty members

Subject <|-- RealSubject
Subject <|-- Proxy
RealSubject <- Proxy : represents
Client --> Subject
{% endplantuml %}

Qualsevol Client pot tractar el Proxy com el RealSubject, ja que implementen Subject.
Exemple: [els tres mags que entren a la torre](https://java-design-patterns.com/patterns/proxy/)

### Patrons creacionals

Separa la construcció d'un objecte complex de la seva representació, de forma que el mateix procés de construcció pot generar diferents representacions.

#### Builder

{% plantuml %}
class Director {
    + construct()
}

interface Builder {
    buildPart()
}

class ConcreteBuilder {
    + buildPart()
    + getResult()
}

class Product

Director *- Builder
Builder <|-- ConcreteBuilder
ConcreteBuilder -> Product : creates

{% endplantuml %}

#### Factory method

Factory method defineix una interfície per crear un objecte, però permet que les subclasses decideixin quina classe s'inicia. Permet diferir la instància de classe a subclasses.

{% plantuml %}
class Creator {
    + factoryMethod()
    + anOperation()
}

class ConcreteCreator {
    + factoryMethod()
}

interface Product

class ConcreteProduct

hide empty members

Product <|-- ConcreteProduct
Creator <|-- ConcreteCreator
ConcreteProduct <. ConcreteCreator
{% endplantuml %}

El mètode factoryMethod és abstracte, i s'encarrega de crear Products. Permet seguir el principi de "Dependency inversion": evitar dependències de tipus concrets.
Exemple: [el regne que necessita tres objectes temàtics](https://java-design-patterns.com/patterns/abstract-factory/)

#### Singleton

El patró de Singleton assegura que una classe només té una instància i li proporciona un punt d'accés global.

{% plantuml %}
' skinparam classAttributeIconSize 0
class Singleton {
    - static uniqueInstance
    - singletonData
    + static instance()
    + singletonOperation()
}
{% endplantuml %}

Exemple: [només pot haver una torre d'ivori](https://java-design-patterns.com/patterns/singleton/)

## Referències

*   [OOPs concepts in Java, Infographic](https://raygun.com/blog/images/oop-concepts-java/oops-concepts-infographic.png)
*   [Comparison of programming paradigms](https://en.wikipedia.org/wiki/Comparison_of_programming_paradigms)
*   [Composition vs. Inheritance: How to Choose?](https://www.thoughtworks.com/insights/blog/composition-vs-inheritance-how-choose)
*   [Java Design Patterns](https://java-design-patterns.com/)
*   [Design Patterns (Java, JournalDev)](https://www.journaldev.com/java/design-patterns)
*   [Difference between Association, Aggregation and Composition in UML, Java and Object Oriented Programming](http://opensourceforgeeks.blogspot.com/2014/11/difference-between-association.html)
*   [PlantUML Design Patterns](https://github.com/RafaelKuebler/PlantUMLDesignPatterns)
