---
layout: page_toc
title: Estudi de mercat
permalink: /eie_estudi/
parent: Empresa
nav_order: 5
---

## Segmentació

### 1\. Segmentació, Beneficis i Característiques de la segmentació

**Segmentació de mercat.**

Un mercat està format per **empreses** i **consumidors**. En els mercats de consum convé **segmentar** els consumidors en grups de consumidors que tenen les mateixes característiques, és a dir, en **grups homogenis**.

L’objectiu de la **segmentació** és **aplicar una estratègia comercial diferenciada** a cada segment, fet que aportarà més efectivitat a les nostres accions.

La segmentació de mercats permet diferenciar el producte segons les necessitats de cada grup de consumidors. Per exemple: una empresa fabricant de cotxes ofereix: cotxes familiars, cotxes esportius, cotxes compactes urbans, etc. un per cada tipus de consumidor.

**Beneficis de la segmentació:**

Segmentant coneixem millor els nostres consumidors i disposem de més informació per prendre decisions de màrqueting. Beneficis:

*   Permet **identificar** els segments de mercat **més atractius**: sigui per creixement de mercat o perquè ens permet identificar segments que estan insatisfets. Després el que caldrà fer és establir prioritats per decidir quins segments satisfer primer.
*   Facilita l’**anàlisi de la competència**. Si la competència ha segmentat i té diferents tipus de productes podem saber què cobreix i identificar millor les seves accions i trobar forats de mercat.
*   Permet **adaptar el producte a les necessitats** del consumidor i satisfer-lo millor: nou producte, nou disseny, reposicionament...

**Característiques d’un segment:**

Un segment ha de ser:

*   **Mesurable**: Hem d’identificar la mida del mercat (volum de vendes potencials) i el poder de compra dels seus consumidors: renta disponible)
*   **Accessible**: Cal determinar on podem vendre i publicitar el producte.
*   **Substancial**: Cal que tingui una mida mínima per tal que sigui rendible.
*   **Estable** al llarg del temps: Hem de poder rendibilitzar la inversió.
*   Cada segment ha de ser **diferent dels altres** (Ex. pel tipus d’ús del producte, pel comportament de compra...).

### 2\. Criteris de segmentació

#### Criteris de segmentació

Quan segmentem un mercat podem dividir-lo segons uns criteris:

*   **Generals**: Que no tenen relació amb el producte.
*   **Específics**: Que tenen relació amb el producte.
*   **Objectius**: fàcils de quantificar.
*   **Subjectius**: No tan fàcil de quantificar.

#### Criteris objectius

*   **Generals**
    *   Demografia: Unitats familiars, població urbana/rural, edat, sexe.
    *   Socioeconòmics: Nivell econòmic.
*   **Específics**
    *   Ús del producte:
        *   Continu/1 sol cop. Ex. lentilles, càmeres.
        *   Familiar /individual (menjar precuinat).
        *   Ús freqüent o esporàdic. Ex. crema solar o crema diària per la pell.
    *   Lloc de consum o de compra: cosmètics de viatge o de casa, ex. consumidors de llibres en màquines del metro o consumidors de llibres en llibreries, Cola-Cao en sobres per als bars i Cola-Cao en pot per a les famílies).
    *   Fidelitat a la marca: programa de punts frequent flyer per aquells clients que són fidels.
    *   Categoria d’usuaris: nou client /antic/ regular... ex. promoció per a nous clients d’un banc o de telefònica o per a subscriptors d’un diari.

#### Criteris subjectius

*   **Generals**
    *   Personalitat: Extravertit / introvertit, prudent /arriscat, rata /generós, confiat / desconfiat, perfeccionista / indiferent...
    *   Estil de vida: regularitats que s’observen en la conducta de les persones en diferents situacions canviants de la seva vida. Forma de viure. Com ho calculem: Segons les opinions de la gent, els interessos (salut / oci) o la cultura (família, sexualitat, treball...) Veiem com gasten els diners i el temps en activitats, treball, compres... Ex. estils de vida: JASP.
*   **Específics**
    *   Actituds, percepcions, preferències dels consumidors: Ex. grau de risc que accepten: cotxes segurs... Percepcions sobre l'obesitat (per això fan productes light), percepcions sobre el medi ambient (productes reciclables).
    *   Beneficis o avantatges que busquem en el producte... el motiu de la compra. Ex. diferents segments en funció del que esperen per la compra d’una minicadena hi-fi... un de més tècnic es preocupa pel so, un altre voldrà un disseny especial, un altre valorarà que sigui econòmic...

### 3\. Estratègies de segmentació

Hem vist que podem segmentar el mercat per **criteris generals** (que no tenen a veure amb el producte, com p. ex. la demografia) i **específics** (relacionats amb el producte: com el seu ús: ampolla familiar o d’ús individual) i a la vegada per criteris **subjectius** (difícils de mesurar, com per exemple la personalitat) i **objectius** (com per exemple la categoria d’usuari).

#### Estratègia indiferenciada

Consisteix a aplicar la **mateixa estratègia comercial** a tots els segments que hem determinat. Busca cobrir les necessitats comunes més que trobar les diferències.

Considera que les diferències existents entre cadascun dels segments no són suficientment importants com per fer estratègies diferents.

La **distribució i la publicitat** són generalment **massives**.

Els **costos** de l’estratègia **són menors** que d’altres, ja que estalviem en termes de producció (productes únics, menys colors, formes...), distribució (menys temps negociant amb diferents canals) i comunicació (no cal adaptar-la a cada segment).

L’inconvenient d’aquesta estratègia és que **no és optima en els mercats on hi ha molta competència** o quan existeixen **segments molt heterogenis**. I avui en dia la majoria de mercats ja estan molt segmentats i costa trobar un producte que s’adapti a grups heterogenis. És difícil competir amb altres empreses que sí que fan la diferenciació i satisfan millor als clients.

Per exemple, en un primer moment Coca-cola només oferia una versió del seu producte, esperant que fos del gust de tothom, més endavant va anar adaptant el producte a diferents segments: light, sense cafeïna, sense cafeïna i light...

A més a més, l’aparició de **nous mitjans de comunicació** i **canals de distribució** ha ocasionat que aparegui la **possibilitat de segmentar més el mercat** i que sigui més difícil aplicar una estratègia comercial única. No farem igual una campanya per internet que en un gran hipermercat o en una botiga detallista.

#### Estratègia de segment diferenciada

L’empresa analitza el mercat i estableix diferents segments als quals aplicarà una estratègia concreta i diferenciada una de l’altra per cobrir millor les necessitats dels clients.

Aquest és el cas de les agències de viatges que tenen en compte el Cicle de vida de les famílies (el cicle de vida familiar significa que hi ha diferents etapes en la vida familiar normal):

1.  Etapa de solters: persones joves sense vincles matrimonials.
2.  Parelles casades joves sense fills.
3.  Niu ple. Parelles casades joves amb fills.
4.  Nius plens. Parelles casades, de major edat, amb fills encara dependents.
5.  Niu buit: parelles casades de major edat sense fills dependents.
6.  Persones de major edat que viuen soles. Encara treballant o ja jubilades.

Podem adaptar una estratègia comercial diferent per a cadascun d’aquests segments.

**Avantatges**: l’empresa esdevé **més eficient** perquè es concentra esforços en satisfer al client adaptant el producte, escollint el canal i realitzant una comunicació específica per al client. Quan el mercat està molt segmentat **no trobem tants competidors** perquè adaptar-se als segments implica un cost elevat, guanyem doncs, **quota de mercat i fidelitat envers la marca.** Si som forts en cada un dels segments, podem aconseguir ser més eficients que dirigint-nos a la totalitat del mercat amb un sol producte, ex. Procter&Gamble amb el sabó de la roba.

**Inconvenients**: Cost ja que gastem més en adaptar la producció, en controlar els diferents canals i en imaginar diferents estratègies de comunicació. Una excessiva segmentació ens pot portar a **confondre** al consumidor, **canibalisme** entre productes i una **disminució de la rendibilitat** (no hi ha economies d’escala).

#### Estratègia de segmentació concentrada: nínxols

L’empresa decideix **atendre a un segment** o a un subsegment del mercat **però no a tots** perquè no té la capacitat interna suficient o perquè les condicions del mercat no són favorables. Un subsegment pot ser per exemple, dins de la categoria de cotxes utilitaris, el subsegment pick-up i el subsegment utilitari esportiu.

Els segments són generalment grans i els subsegments o nínxols són més petits i **normalment atrauen menys competidors**.

Pex. Bentley es dirigeix a un subsegment: cotxe de luxe, alta qualitat, bon servei i status.

L’empresa es dirigeix al segment on té un avantatge competitiu que d’altres no tenen on que no volen utilitzar perquè no tenen prou experiència o no consideren que sigui un mercat en creixement.

La producció, distribució i comunicació són molt específiques per al segment.

**Avantatge**: Aconsegueix una bona quota de mercat perquè s’especialitza.

**Inconvenient**: Estratègia molt sensible als canvis de les preferències del consumidor i a l’aparició de nous competidors.

### 4\. Màrqueting mix, Distribució o Comunicació

A partir de la concreció de les variables del màrqueting mix (producte, preu, distribució i comunicació) podem segmentar segons els criteris corresponents.

#### Per producte

Podem oferir diferents models d’un mateix producte diferenciats per:

*   Hàbit d’ús: envàs familiar o individual: aigua
*   Lloc d’ús: a casa, de viatge: raspall de dents
*   Marca: creem una segona marca com per exemple Sony i Aiwa.
*   Complexitat del producte: mòbils normals o avançats.

#### Per preu

Podem oferir un preu diferent a cada segment, sigui per una oferta temporal o per una categoria d’usuaris especial: ex. Els subscriptors tenen un 20% de descompte en entrades al teatre o, si contractes un pac de telefonia abans d’una data X, tens una bonificació. Podem fer una compra en tres mesos de temps i tenim un dte.

Dia de l'espectador, dias azules de RENFE...

#### Per distribució

Escollim acuradament el canal de distribució que volem fer servir per fer arribar el nostre producte. No trobarem certs perfums a perfumeries de barri o certes marques de roba a qualsevol botiga.

Canal **exclusiu**: Només fem arribar el producte per un sol canal de distribució ben seleccionat.

Canal **selectiu**: Escollim els canals que compleixin amb certes característiques (per exemple Mango només selecciona locals que estiguin al centre de la ciutat en les avingudes més importants).

**Distribució intensiva**: maximitzar la presencia en tota mena de canals: ex. cacauets que trobem tant a supermercats, màquines de vending, quioscs, bars...

#### Per comunicació

Escollirem el mitjà de comunicació que millor satisfaci les nostres necessitats de comunicació i a la vegada, detallarem les característiques, p.ex. televisió, a una franja horària X, premsa de menors de 25 anys, a la pàgina del mig...

Certes marques de roba fan publicitat esponsoritzant campionats de golf o de polo, concentrant-se en sectors de la població minoritaris però amb alt poder adquisitiu.

## Estudi de mercat

L’anàlisi de l’entorn i l’estudi de mercat són aspectes molt importants en tot projecte empresarial. Es tracta de **detallar el mercat en el qual l’empresa mantindrà l’activitat principal**, així com els **clients potencials** i la **competència**. Una empresa ven productes i serveis i, per tant, necessita clients disposats a comprar-los. És per això que l’estudi de mercat ens ha de permetre analitzar qui són aquests clients; les necessitats, els desitjos, les demandes i les expectatives que poden tenir; com es comporten a l’hora de comprar i de quina manera haurem de respondre a tot això.

### 1\. Característiques del sector

Qualsevol emprenedor ha de **conèixer el sector on es desenvoluparà l’activitat**, és a dir, ha de conèixer els **clients**, els **proveïdors**, la **competència**, l'amenaça de **nous competidors**, les **possibles aliances** (col·laboradors), les **barreres d’entrada** existents i els **productes o serveis substitutius**. També és habitual analitzar la concentració o dispersió de les empreses, l’evolució i les perspectives futures, el volum de facturació, les regulacions del sector i els permisos necessaris per actuar-hi, entre altres.

### 2\. Anàlisi del mercat

Es tracta d’**analitzar** el mercat en què l’empresa desenvoluparà l’activitat i **identificar les forces competitives** que el configuren.

### 3\. Àmbit, evolució i tendències

*   **Zones geogràfiques** on es preveu comercialitzar el producte o servei (barri, municipi, comarca, entre altres). Cal diferenciar entre el mercat real, aquell que actualment compra o consumeix el producte o rep el servei, i el mercat potencial, aquell que pot comprar o consumir el producte o servei de l’empresa independentment que ja ho faci o no.
*   **Tendència** i **evolució** del **mercat**: s’ha de conèixer si aquest mercat pateix una evolució a l’alça o a la baixa, i en quina proporció respecte a anys anteriors, o bé si efectua una desviació cap a productes o serveis semblants.
*   **Volum del mercat**: calculat en unitats, en euros, en quilos, amb la màxima segmentació possible (en àrees geogràfiques, per canals de distribució i d’altres).
*   **Possibles canvis en la demanda**.
*   **Quota de participació estimada** de l’empresa: part del mercat que compra o consumeix el producte o servei de l’empresa en relació amb el total de compradors o consumidors del producte genèric.

### 4\. Segmentació del mercat

Segmentar el mercat és **agrupar els clients en grups similars** en funció de les seves necessitats i dels seus hàbits, que solen estar vinculats a criteris demogràfics, geogràfics, socioeconòmics, i altres. Amb aquesta segmentació podrem **establir plans específics per a cadascun** d’aquests segments homogenis i pensar en les raons per les quals el producte pot satisfer-ne les necessitats.

### 5\. Anàlisi dels clients

Es tracta d’**aprofundir en el coneixement dels clients** i arribar a comprendre’n el comportament. Caldrà, doncs, determinar quins seran els clients potencials de l’empresa. Aquests clients poden ser **particulars** (consumidors finals), dels quals hauríem de definir-ne el perfil (sexe, edat, estat civil, poder adquisitiu, nivell cultural, localització geogràfica, hàbits de consum, entre altres), però també poden ser **empreses**, **administracions públiques** o **associacions**, **fundacions**, i d’altres.

En qualsevol cas, siguin del grup que siguin, és important determinar qui són, on són, què necessiten i què demanen, i quines millores desitjarien respecte als productes que ara ofereix la competència i en què basen les seves decisions de compra.

Respondrem les següents preguntes sobre els clients:

*   **Qui** compra? Característiques personals.
*   **Per què** compra? Motivacions.
*   **Què** compra? Productes i marques.
*   **Com** compra? Busca el producte o compra el que se li ofereix.
*   **Quant** compra? Quantitats.
*   **On** compra? Establiments, context, distància.

### 6\. Anàlisi de la competència

En aquest apartat s’ha d’**analitzar la competència més directa**, és a dir, les empreses que **ofereixen** els **mateixos** (o similars) **productes o serveis** i que s’**adrecen al mateix públic**. És important no limitar-se a fer una llista d’aquests competidors, ja que cal conèixer els aspectes més importants que els caracteritzen:

*   Identificar **quins competidors** hi ha.
*   **On són**, en quines zones operen i quina és la seva **quota de mercat**.
*   **A qui venen** i **quina és la imatge** que té d’ells el client potencial? Tenen prestigi?
*   **Quins** productes o serveis ofereixen i amb **quines garanties**? Són **innovadors**? Tenen **qualitat**?
*   Quina és la seva **política de preus**, descomptes i condicions de pagament?
*   **Inverteixen** part del seu pressupost en **promoció i publicitat**?
*   **Quina estratègia competitiva** utilitzen? Quins **avantatges** tenen, quines són les seves **mancances**, i **per què tenen èxit** o **per què no**?

### 7\. Anàlisi dels intermediaris

En alguns sectors, si l’**empresa no ven directament** al client, és important conèixer els **intermediaris** (**distribuïdors**, **detallistes**, entre altres), perquè incideixen en la qualitat i la imatge que es dóna. Cal saber **qui i quants són**, **com treballen** i **com poden agregar valor** a l’empresa.

### 8\. Anàlisi dels proveïdors

Els proveïdors **influeixen de manera directa en la qualitat dels productes o serveis** d’una empresa. S’ha de conèixer els possibles proveïdors i identificar **els que ofereixin avantatges competitius** als productes o serveis que ens disposem a desenvolupar. En general, **cal escollir els proveïdors que ens ofereixin una qualitat acceptable a un preu raonable**, tenint en compte també els terminis de pagament i els descomptes o ràpels, però sense oblidar els terminis de lliurament, ja que poden ser crítics en alguns processos productius.

## Referències

*   [Guía para realizar un estudio de mercado](https://www.infoautonomos.com/estudio-de-mercado/breve-guia-para-estudio-de-mercado/)
*   [Qué es un análisis de mercado y cómo se hace](https://cl.oberlo.com/blog/analisis-de-mercado)
